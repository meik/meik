<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
/**
 * This is the SQL Class for MEIK.
 * Goal is to provide a low-level backend on which
 * MEIK can rely on.
 * 
 * @param string $host     The MySQL Server to connect to.
 * @param string $user     The MySQL user connect with.
 * @param string $pass     The MySQL user's pass.
 * @param string $db       The MySQL database to use.
 * @param string $sqlcount The number of MySQL requests that were executed.
 * @param string $error    The error message, when it occurs. Null if none.
 * @param string $sqlconn  The MySQL ressource.
 * 
 * @author Lissyx
 * @copyright Copyright &copy; 2005, Lissyx
 * 
 */

class SQL {
	public $host = "localhost";
	public $user = "";
	public $pass = "";
	public $db   = "";
	public $sqlcount = 0;
	public $error    = null;
	public $sqlconn; // Associated ressource to the SQL connection
	public $hiderr = true;
	
	/* MySQL Backend stuff */
	
	/**
	 * SQL Class constructor, we initialize with a false
	 * connection ressource, to avoid problems.
	 * */
	function __construct()
	{
		$this->sqlconn = false;
		return true;
	}
	
	/**
	 * SQL Class destructor, we force the disconnect from
	 * server.
	 * */
	function __destructor()
	{
		$this->disconnect();
	}
	
	/**
	 * Try to connect to the serveur $this->host, using
	 * user $this->user, password $this->pass and database
	 * $this->db.
	 * 
	 * If connection is successfull, then put the connection
	 * resource within $this->sqlconn. Otherwise, send an error.
	 * 
	 */
	function connect() {
		if(!$this->host || !$this->user || !$this->pass || !$this->db) {
			$this->error = _("Cannot connect to MySQL server. Please check settings.")."<br /> ";
		} else {
			$this->sqlconn = @mysql_connect($this->host, $this->user, $this->pass);
			if($this->sqlconn) {
				if(!@mysql_select_db($this->db, $this->sqlconn)) {
					$this->error = _("Cannot select a database.")." "._("Error was :")." ".mysql_error($this->sqlconn);
				}
			} else {
				$this->error = _("Cannot connect to MySQL server, maybe it has gone away ?")."<br /> "._("Error was :")." ".mysql_error($this->sqlconn);
			}
		}
	}
	
	/**
	 * This is used to manage SQL queries.
	 * 
	 * @param $sql The MySQL request to executes.
	 * 
	 * @return An associating array if we got something interesting.
	 */
	// Executes a MySQL query 
	function query($sql)
	{
		// echo("Executing : $sql<br />");
		if($this->checkMySQL()) { // MySQL is alive ?
			$q = mysql_query($sql, $this->sqlconn); // do query.
			if($q) { // Works !
				$this->sqlcount++;
				if(is_resource($q) || is_object($q)) { // it's a ressource/object result !
					$r = null;
					while ($res = @mysql_fetch_assoc($q)) {
						$r[] = $res; // create array !
					}
					return $r;
				} else {
					return $q;
				}
			} else {
				if(!$this->hiderr) { // So bad ...
					$this->mysqlerr();
				}
				return false;
			}
		} else {
			if(!$this->hiderr) { // Waou !
				$this->mysqlerr();
				warning("Connection to MySQL server isn't established.");
			}
			return false;
		}
	}
	
	/**
	 * Just a little wrapper for playing smoothly with MySQL errors ...
	 */
	function mysqlerr($sql)
	{
		error(_("An error occured while executing this query :")." $sql<br /> "._("Please check this error :")." ".mysql_error($rsql->sqlconn));
	}
	
	/**
	 * This checks that the MySQL connection has been established.
	 * 
	 */		
	function checkMySQL()
	{
		return $this->sqlconn;
	}
	
	/**
	 * Displays a &lt;p&gt;&lt;/p&gt; tag that includes informations about the
	 * connection, such as number of successfully executed requests,
	 * execution time ...
	 */
	function totalcount()
	{
		$time_end = microtime(true);
		$time = round($time_end - $GLOBALS["time_start"], 4);
		return '
	<p id="sql-end">
	<span class="sql-end">'. $this->sqlcount . ' '._("SQL requests were successfully executed.") . ' ' ._("Execution time : ").$time.' secs.</span></p>
';
	}
	
	/**
	 * This displays the MySQL informations return by $this->totalcount()
	 * then, check the MySQL connection is still alive, and if, disconnect
	 * from the server.
	 */
	function disconnect()
	{
		echo $this->totalcount();
		if($this->checkMySQL()) {
			mysql_close($this->sqlconn);
		}
	}
	
	/* Enf of MySQL backend stuff */
}
?>