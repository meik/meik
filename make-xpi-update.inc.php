<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
$userid = $session->getsessvar("id");

$rank = $meik->getuserrank($userid);

if($rank == 1) {
	$meik->debug = false;
	
	echo '
<div class="update-exts">';
	debug("Listing XPI directory : ". $meik->dir);
	
	$values = $meik->listPackages();
	
	if(sizeof($values) < 1) {
		echo '<br />' . _("Hmm, it looks like there was no extension to add or update.") . '<br />
</div>';
	} else {
		foreach($values as $ext_infos) {
			$meik->add_ext($ext_infos);
		}
		$meik->mail_updates();
		
		echo '<br />' . _("Now, all extensions should be up to date. Check above for any error message.") . '<br />
</div>';
	}

} else {
	echo '<p class="error">You are not privileged enough.</p>';
}
?>