<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
 /**
  * We just want to extract internationalization code from the whole MEIK's lib.
  */

// Load languages.
include_once("locale/langs.php"); 

class Lang {
 	/**
	 * We'll select the best language.
	 */
	
	/* Prepare for internationalization */
	function intl() {
		$lang = $this->chooselang();
		$this->setlang($lang);
	}
	
	/**
	 * Checks that language exists.
	 * 
	 * @param $lang The language to test.
	 * 
	 * @return Was the file locale/$lang/LC_MESSAGES/messages.mo found ? (true|false)
	 */
	function langexists($lang)
	{
		return is_file(dirname(__FILE__) . "/locale/".$lang."/LC_MESSAGES/messages.mo");
	}
	
	/**
	 * We select the best language, regarding what's available and
	 * what's the user accepts.
	 * 
	 * If the user send :
	 * Accept-Language: sk-SK,fr-FR,en-US
	 * 
	 * And, if we only have fr-FR and en-US locale, we'll send him
	 * the French page.
	 * 
	 * Furthermore, if user accepts fr-CA,fr-FR,en-US and we only have
	 * fr-FR, as fr-CA is more close to fr-FR than to en-US, we'll send 
	 * the French page also.
	 * 
	 * @param $languages An array of accepted languages, prepares by
	 * $this->getuserlang();
	 * 
	 * @return The selected language. e.g. "fr-FR"
	 */
	function select_lang($languages)
	{
		// First try with xx_YY
		foreach($languages as $lang) {
			if($this->langexists($lang)) { // Good ....
				return $lang;
			}
		}
		
		// still no language found ? heh, try with xx
		foreach($languages as $lang) {
			if(strlen($lang) == 2) { // it's a 2 bytes string, such as fr, en ...
				foreach($GLOBALS["avail_lang"] as $avail => $desc) {
					if(strstr($avail, $lang)) {
						if($this->langexists($avail)) { /// YEAH !
							return $avail;
						}
					}
				}
			}
		}
	}
	
	/**
	 * We'll get charset from $GLOBALS["avail_lang"], which is took
	 * from locale/langs.php and we force output with this charset.
	 * 
	 * @return Defined charset for the lang, or "us-ascii" if none.
	 */
	function charsetselect()
	{
		/*
		 * _("") outputs :
		 * Project-Id-Version: MEIK pre-1.0
		 * Report-Msgid-Bugs-To: 
		 * POT-Creation-Date: 2005-08-27 21:36+0200
		 * PO-Revision-Date: 2005-09-04 01:53+0100
		 * Last-Translator: blah <root@localhost>
		 * Language-Team: Meik Developpement Team <lissyx@infos-du-net.com>
		 * MIME-Version: 1.0
		 * Content-Type: text/plain; charset=iso-8859-15
		 * Content-Transfer-Encoding: 8bit
		 * X-Poedit-Language: French
		 * X-Poedit-Country: FRANCE
		 * X-Poedit-Basepath: E:\ALEX\PHP-MySQL\meik
		 * X-Poedit-SearchPath-0: E:\ALEX\PHP-MySQL\meik
		 */
		$avail_lang = $GLOBALS["avail_lang"]; // load all languages.
		if(is_array($avail_lang)) { // check if it's valid.
			$dispos = null;
			$selected = $this->chooselang(); // choose the language
			$charset = $avail_lang[$selected][1]; // retrieve charset
			return $charset;
		} else {
			return "us-ascii";
		}
	}
	
	/**
	 * We'll select the language. First, we try to read if there's
	 * already a cookie, or not.
	 * 
	 * If a cookie is found, then we read it and return it's value.
	 * 
	 * If no cookie is found, we try to select a language from what's
	 * the user sent and what's available.
	 * 
	 * If still none language was found, we return "en-US".
	 * 
	 * @return Language string (e.g. fr-FR)
	 */
	function chooselang()
	{
		$cookie_lang = $GLOBALS["session"]->getlang(); // read cookie
		if(false !== $cookie_lang) { // a language cookie was found !
			$l10n = $cookie_lang;
		} else {
			$l10n = $this->getuserlang(); // try to read from user's data
		}
		if(empty($l10n)) {
			$l10n = "en_US"; // still nothing, sorry.
		}
				
		return $l10n;
	}
	
	/**
	 * We read from HTTP headers to know what's can be tried.
	 * 
	 * @return If a language was found.
	 * If no Accept-Language header is present, 
	 */
	function getuserlang()
	{
		if(array_key_exists("HTTP_ACCEPT_LANGUAGE", $_SERVER)) { // check for HTTP header
			$lang = split(";", $_SERVER["HTTP_ACCEPT_LANGUAGE"]); // Prepare data, get languages
			$lang = split(",", $lang[0]); // Prepare data, making the array of languages.
			$lang = $this->prepare_lang($lang);
			return $this->select_lang($lang); // then select the language.
		} else {
			return false;
		}
	}
	
	function prepare_lang($array)
	{
		if(is_array($array)) {
			$narray = array();			
			foreach($array as $entry) {
				if(strlen($entry) == 5) {
					$narray[] = strtolower($entry[0].$entry[1]) . "_"
							   .strtoupper($entry[3].$entry[4]);
				} else {
					$narray[] = strtolower($entry[0].$entry[1]);
				}
			}
			return $narray;
		} else {
			return $array;
		}
	}
	
	/**
	 * We set all environement variables to select the appropriate language.
	 * 
	 * @param $lang The language to apply for.
	 * */
	function setlang($lang)
	{
		$this->lang = $lang; // First, set class variable of language.
		setlocale(LC_ALL, "$lang"); // Set locale to our language.
		putenv("LANG=$lang"); // needed under Windows.
		bindtextdomain("messages", dirname(__FILE__) . "/locale/"); // Set where data lives
		textdomain("messages");
	}
	
	/**
	 * We want to prepare the dropdown box for language selection.
	 * 
	 * @return Some uggly HTML code ;)
	 */
	function showlangselect()
	{
		$avail_lang = $GLOBALS["avail_lang"];
		if(is_array($avail_lang)) { // Check if we have some languages.
			$dispos = null;
			foreach($avail_lang as $locale => $infos) { // Yeah, list them all !
				$selected = $this->chooselang();
				$langue = $infos[0];
				if($selected == $locale) {
					$select = ' selected="selected"'; // It's teh one selected by the user !
				} else {
					$select = '';
				}
				$dispos .= '
					<option value="'.$locale.'"'.$select.'>'.$langue.'</option>'; // Add.
			}
		
			$html = '
	<div id="lang-select">
		<form action="meik.index.php?nxt=setlang" method="post">
			<p id="form-lang-select">
				<select name="lang" id="lang" onchange="javascript:submit()">'.$dispos.'
				</select>
				<input type="submit" name="valid" value="'._("Ok").'" />
			</p>
		</form>
	</div>
';
		} else { // So bad ...
			$html = '
	<div id="lang-select">
			<p id="form-lang-select">
				'._("No language found.").'
			</p>
	</div>
';
		}
		
		return $html;
	}
}
?>