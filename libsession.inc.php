<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
class Session {
	function __construct()
	{
		session_name("meik");
		session_set_cookie_params(2592000);
		session_start();
	}
	
	function createsession($user, $id, $nom)
	{
		$_SESSION["user"]  = $user;
		$_SESSION["id"]    = $id;
		$_SESSION["nom"]   = $nom;
	}
	
	function deletesession()
	{
		$_SESSION = array();
		if(isset($_COOKIE[session_name()])) {
			@setcookie(session_name(), "", time() - 42000, "/");
		}
		@session_destroy();
	}
	
	function isconnected($user)
	{
		return in_array($user, $_SESSION);
	}
	
	function hascookie()
	{
		return $this->isconnected($this->getsessvar("user"));
	}
	
	function showdeco($user)
	{
		if($this->isconnected($user)) {
			return '<dd><a href="?nxt=unauth">' . _("Disconnect") . '</a></dd>';
		}
	}
	
	function showusername($user)
	{
		$nom = $this->getsessvar("nom");
		if($nom) {
			return $nom;
		} else {
			return $user;
		}
	}
	
	function getsessvar($varname)
	{
		if(array_key_exists($varname, $_SESSION)) {
			return $_SESSION[$varname];
		} else {
			return false;
		}
	}
	
	function showadmin()
	{
		
		/**
		 * gestion des utilisateurs
		 * gestion des extensions disponibles
		 * gestion des applications cibles 
		 */
		
		$admin = $GLOBALS["meik"]->getuserrank($this->getsessvar("id"));
		debug("Admin or not ? " . $admin);
		$code = null;
		
		if($admin) {
			$admin_menu = array (
  			"id"    => "admin", 
  			"title" => _("Administrator menu"),
  			"data"  =>
  				array (
			  		array ("admin.php?manager=users",      _("Users management")),
			  		array ("admin.php?manager=exts",       _("Extensions management")),
					array ("admin.php?manager=aims",       _("Target management")),
					array ("admin.php?manager=update",     _("Reindex available extensions")),
					array ("admin.php?manager=add",        _("Add or update one or more extension")),
					array ("admin.php?manager=auto-update",_("Check available update using updateURL")),
					array ("admin.php?manager=valid-sub",  _("Validate new extensions")),
					array ("admin.php?manager=valid-not",  _("Validate update notification")),
					array ("admin.php?manager=auth-infos", _("Authentication backend")),
					array ("admin.php?manager=xpi-manage", _("List non-added XPI packages"))
			  	)
			);
	
		$options = array (
  			"id"    => "options-admin", 
  			"title" => _("Options"),
  			"data"  =>
  				array (
			  		array ("javascript:plouf('msg-info')",    _("Toggle information messages")),
			  		array ("javascript:plouf('msg-warning')", _("Toggle warning messages")),
					array ("javascript:plouf('msg-debug')",   _("Toggle debug messages")),
					array ("javascript:plouf('msg-err')",     _("Toggle error messages")),
			  	)
			);
			$code = makemenu($admin_menu) . makemenu($options);
		}
		
		return $code;
	}
	
	function showuser()
	{
		$menu = array (
			"id"    => "user", 
			"title" => _("User menu"),
			"data"  =>
				array (
			  		array ("meik.index.php", _("Profiles management")),
			  		array ("?todo=addp",     _("Add a profile")),
			  		array ("?todo=compte",   _("Account management")),
			  		array ("?todo=upform",   _("Submit one or more extension")),
			  		array ("?todo=notif",    _("Notify an extension's update")),
			  		array ("?nxt=unauth",    _("Disconnect"))
			  	)
			);
		
		$code = makemenu($menu);
		
		return $code;
	}
	
	function showunauth()
	{
		$menu = array (
			"id"    => "anon", 
			"title" => _("Not connected"),
			"data"  =>
				array (
			  		array ("meik.index.php", _("Login page")),
			  		array ("?nxt=inscr",     _("Register for an account"))
			  	)
			);
		
		$code = makemenu($menu);
		
		return $code;
	}
	
	function showmessages()
	{	
		$admin = $GLOBALS["meik"]->getuserrank($this->getsessvar("id"));
		
		if($admin) {
			$code = '<span class="messages" id="messages"></span>';
		} else {
			$code = '';
		}
		
		return $code;
	}
	
	
	function savelang($lang)
	{
		setcookie("meik-l10n", $lang, time() + 2592000);
	}
	
	function getlang()
	{
		if(array_key_exists("meik-l10n", $_COOKIE)) {
			$lang = $_COOKIE["meik-l10n"];
			if(isset($lang)) {
				return $lang;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
?>