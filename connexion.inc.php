<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

echo 
$session->showunauth() .
'
<div class="contenu">
	<p class="logo"><img src="images/meik-logo.png" alt="MEIK Logo" /></p>
	<p class="info-reg">
		'._("Welcome. MEIK will help you to easily install many Firefox, Mozilla Suite, and SeaMonkey (depending on extension's compatibility) extensions. So, you first need to register (an easy and fast step), then, after getting logged in, just create a new profil where you'll put extension you want to install at the same time. You are also able to create as many profile as you want with each account in order to, e.g., having one profile for each computer. MEIK also let you the way of submit new extension that are not already present, to make the choice growing, and to notice administrators when one or more extension isn't up to date.").'
	<br /><br />
		'._("Not registered ?").' <a href="?nxt=inscr">'._("Get an account !").'</a>'.'
	</p>
';

$msg = getvar("msg");
$mysql = $rsql->CheckMySQL();
switch($msg) {
	case "1":
		$m = _("Cannot authenticate your account. Please check your login and password.") . "<br />" . _("MySQL Server is"). " ".($mysql ? _("up.") : _("down. You cannot authenticate, as MySQL server is currently down.") );
		break;
	
	default:
		$m = "";
		break;
}
echo '
<p class="error">'.$m.'</p>

<form action="?nxt=auth" method="post">
	<table class="form-reg">
		<tr>
			<td>'._("Login").' :</td>
			<td><input type="text" name="login" /></td>
		</tr>
		<tr>
			<td>'._("Password").' :</td>
			<td><input type="password" name="pass" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" name="validate" value="'._("Connect").'" /></td>
		</tr>
	</table>
</form>
</div>';
?>