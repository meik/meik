/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

const MOD_ROWS = 3;
const MOD_COLS = 30;

function update_nbr()
{
	document.getElementById("count_choisies").innerHTML = "(" + document.getElementById("choisies").length + ")";
	return true;
}

function addline_dispo()
{
	modif_selectbox("dispo", "add-row", MOD_ROWS);
	return true;
}

function remline_dispo()
{
	modif_selectbox("dispo", "del-row", MOD_ROWS);
	return true;
}

function addline_choisies()
{
	modif_selectbox("choisies", "add-row", MOD_ROWS);
	return true;
}

function remline_choisies()
{
	modif_selectbox("choisies", "del-row", MOD_ROWS);
	return true;
}

function modif_selectbox(id, todo, value)
{
	// todo  : add-row, del-row, add-col, del-col
	// value : nr rows/cols to add/remove.
	var el     = document.getElementById(id);
	
	switch(todo) {
		case "add-row":
				var actual = el.size;
				var _new = actual + value;
				el.size = _new;
			break;
			
		case "del-row":
				var actual = el.size;
				var _new = actual - value;
				el.size = _new;
			break;
		
		default:
				return false;
			break;
	}
	
	return true;
}

function XPIinstall()
{
	if(checkSoftwareInstall()) {
		var i = 0; // compteur pour les ids.
		var xpilist = ""; // liste des XPI ? installler.
		var xpis = document.getElementById("xpis");
		var checkBoxes = GetCheckboxes(xpis); // Le nombre de checkbox du form.
		var max = checkBoxes.length;
		var xpiInst = new Object();
		
		for(i = 0; i < max; i++) {
			xpiInst[checkBoxes[i][0]] = checkBoxes[i][1];
		}
		
		InstallTrigger.install(xpiInst);
	}
	
	return true;
}

function GetCheckboxes(formulaire)
{
    var lastnode = formulaire.elements.length - 1;
    var i;
    var id = null;
    var k = 0;
    var correct = new Array();
	
    for(i = 0; i < lastnode; i++) {
    	id = "ext" + i;
    	if(formulaire.elements[i].type == "checkbox"
    	&& formulaire.elements[i].checked) {
			correct[k] = new Array();
			correct[k][0] = formulaire.elements[i].name;
			correct[k][1] = formulaire.elements[i].value;
			k++;
		}
    }

    return correct;
}

function checkSoftwareInstall()
{
	return load();
}

function load()
{
	var ins = InstallTrigger.enabled();
	
	if(!ins) {
		alert("Software installation is disabled. MEIK cannot run.");
	}
	
	if(document.getElementById("description")) {
		document.getElementById("description").style.display = "none";
	}
	
	return ins;
}

function addext(source, target)
{
	var dispo    = document.getElementById(source);
	var choisies = document.getElementById(target);
	var i;
	
	for(i = 0; i < dispo.options.length; i++) {
		if(dispo.options[i].selected && dispo.options[i].text
		&& dispo.options[i].value && !exists(choisies, dispo.options[i].value)) {
			var newLine = new Option(
						dispo.options[i].text,
						dispo.options[i].value
						);
						
			choisies.options[choisies.options.length] = newLine;
		}
	}
	
	update_nbr();
	
	return true;
}

function rmext(source, target)
{
	var choisies = document.getElementById(source);
	var dispo    = document.getElementById(target);
	
	while(choisies.options.selectedIndex > -1) {
		choisies.removeChild(choisies.childNodes[choisies.options.selectedIndex]);
	}
	
	update_nbr();
	
	return true;
}

function exists(cible, id)
{
	var nbr = cible.childNodes.length;
	var i;
	
	for(i = 0; i < nbr; i++) {
		if(cible.childNodes[i].value && cible.childNodes[i].value == id) {
			return true;
		}
	}
	
	return false;
}

function recap()
{
	var choisies = document.getElementById("choisies");
	var exts     = document.getElementById("exts");
	var nbr      = choisies.childNodes.length;
	var i;
	
	for(i = 0; i < nbr; i++) {
		// choisies.options[i].selected = true;
		if(choisies.childNodes[i].value) {
			exts.value += choisies.childNodes[i].value + ";";
		}
		// document.write("Element (" + i + ") :: " + choisies.options[i].value + " :: " + choisies.options[i].selected + ";<br />");
	}
}

function recap_notif()
{
	var choisies = document.getElementById("choisies");
	var exts     = document.getElementById("exts");
	var nbr      = choisies.childNodes.length;
	var i;
	
	for(i = 0; i < nbr; i++) {
		// choisies.options[i].selected = true;
		if(choisies.childNodes[i].selected) {
			exts.value += choisies.childNodes[i].value + ";";
		}
		// document.write("Element (" + i + ") :: " + choisies.options[i].value + " :: " + choisies.options[i].selected + ";<br />");
	}
}

function add_to_update_list(id, name)
{
	var cible   = document.getElementById("mass-update");
	var taille  = cible.childNodes.length;
	var present = false;
	var i;
	
	var newLine = new Option(
			name,
			id
		);
		
	for(i = 0; i < taille; i++) {
		if(cible.childNodes[i].value == id) {
			present = true;
			break;
		}
	}
	
	if(present == false)
		cible.options[cible.options.length] = newLine;
	
	// alert("Adding " + id + " (" + url + ") ");
}

function add_all_to_update_list()
{
	var liste  = document.getElementsByTagName("div");
	var taille = liste.length;
	
	var name, id, hasupdate;
	
	var i;
	
	for(i = 0; i < taille; i++) {
		if(liste[i].className == "exts-opts") {
			if(liste[i].childNodes[3]
			&& liste[i].childNodes[5]
			&& liste[i].childNodes[9] ) {
				id			= liste[i].childNodes[3].value;
				name		= liste[i].childNodes[5].value;
				hasupdate	= liste[i].childNodes[9].value;
			
				if(hasupdate == "1")
					add_to_update_list(id, name);
			}
		}
	}
}

function remove_from_update_list()
{
	var cible  = document.getElementById("mass-update");
	
	while(cible.options.selectedIndex > -1) {
		if(cible.childNodes[cible.options.selectedIndex])
			result = cible.removeChild(cible.childNodes[cible.options.selectedIndex]);
	}
}

function recap_update()
{
	var cible    = document.getElementById("mass-update");
	var exts     = document.getElementById("exts");
	var nbr      = cible.childNodes.length;
	var i;
	
	for(i = 0; i < nbr; i++) {
		// cible.options[i].selected = true;
		if(cible.childNodes[i].value)
			exts.value += cible.childNodes[i].value + ";";
		// alert("Element (" + i + ") :: " + cible.options[i].value + " :: " + cible.options[i].selected + ";<br />");
	}
}

function showdiv(classname)
{
	var elements = document.getElementsByTagName("div");
	var total    = elements.length;
	var result   = null;
	var i;
	
	for(i = 0; i < total; i++) {
		if(elements[i].className == classname) {
			elements[i].style.display = "block";
		}
	}
}

function hidediv(classname)
{
	var elements = document.getElementsByTagName("div");
	var total    = elements.length;
	var result   = null;
	var i;
	var k = 0;
	
	for(i = 0; i < total; i++) {
		if(elements[i].className == classname) {
			elements[i].style.display = "none";
			k++;
		}
	}
	
	notif_hidden(classname, k);
}

function plouf(_classname)
{
	var elements = document.getElementsByTagName("div");
	var total    = elements.length;
	var result   = null;
	var i;
	var k = 0;
	
	for(i = 0; i < total; i++) {
		if(elements[i].className == _classname) {
			if(elements[i].style.display == "none") {
				elements[i].style.display = "block";
				putdesc("messages-" + _classname, "");
			} else {
				elements[i].style.display = "none";
				k++;
			}
		}
	}
	
	notif_hidden(_classname, k);
}

function notif_hidden(_class, number)
{
	if(number >= 0) {
		putdesc("messages-" + _class, "(" + number + ") " + _class);
	}
}

function admin_toggle_load()
{
	showdiv('msg-warning');
		notif_hidden("msg-warning", 0);
	showdiv('msg-err');
		notif_hidden("msg-err", 0);
	showdiv('msg-debug');
		notif_hidden("msg-debug", 0);
	hidediv('msg-info');
	
	load();
}

function makeXML(source)
{
	document.body.style.cursor = "wait";
	setWait("Loading, please wait ...");
	var id = document.getElementById(source).value;
	sendData("xml.php", "do=getdescr&id=" + id);
}

function setWait(message)
{
	putdesc("desc-error", message);
	document.getElementById("description").style.display = "";
}

function showDesc(xmlresult)
{
	var error    = getel(xmlresult, "error");
	
	switch(error) {
		case "2":
			var nom  = getel(xmlresult, "name");
			var ver  = getel(xmlresult, "version");
			var desc = getel(xmlresult, "desc");
			
			putdesc("desc-error", null);
			putdesc("desc-name", nom);
			putdesc("desc-vers", ver);
			putdesc("desc-desc", desc);
			
			break;
			
		default:
			putdesc("desc-error", "Unknown error.");
			document.getElementById("desc-success").style.display = "none";
			break;
	}
	
	document.body.style.cursor = "default";
	document.getElementById("description").style.display = "";
	
	/* var response = xmlresult.getElementsByTagName("response")[0].firstChild.data;
	alert("response == " + response); */
}

function getel(root, elname)
{
	return root.getElementsByTagName(elname)[0].firstChild.data;
}

function putdesc(id, message)
{
	var dest = document.getElementById(id);
	dest.innerHTML = message;
}

/**
 * This function is freely inspired from openweb.eu.org
 */
function sendData(url, data)
{
	var xmlhttp = getHTTPObject();

	if (!xmlhttp) {
        return false;
    }
        
    xmlhttp.open("GET", url + "?" + data, true);
    xmlhttp.send(null);
    
    return true;
}

/**
 * This one also.
 */
function getHTTPObject()
{
  var xmlhttp = false;

  /* on essaie de cr?er l'objet si ce n'est pas d?j? fait */
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined')
  {
     try  {
        xmlhttp = new XMLHttpRequest();
     }
     catch (e)
     {
        xmlhttp = false;
     }
  }

  if (xmlhttp)
  {
     /* on d?finit ce qui doit se passer quand la page r?pondra */
     xmlhttp.onreadystatechange=function()
     {
        if (xmlhttp.readyState == 4) /* 4 : ?tat "complete" */
        {
           if (xmlhttp.status == 200) /* 200 : code HTTP pour OK */
           {
              /*
              Traitement de la r?ponse.
              Ici on affiche la r?ponse dans une bo?te de dialogue.
              */
              // alert(xmlhttp.responseText);
              showDesc(xmlhttp.responseXML.documentElement);
           }
        }
     }
  }
  
  return xmlhttp;
}