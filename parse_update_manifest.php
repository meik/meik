<?php
require_once("class_rdf_parser.php");

function parse_update_manifest( $manifestdata )
{
	$data = array();
	
	$rdf=new Rdf_parser();
	$rdf->rdf_parser_create( NULL );
	$rdf->rdf_set_user_data( $data );
	$rdf->rdf_set_statement_handler( "__update_mf_statement_handler" );
	$rdf->rdf_set_base("");
	
	if ( ! $rdf->rdf_parse( $manifestdata, strlen($manifestdata), true ) ) {
		return null;
	}
	
	$rdf->rdf_parser_free();
	
	// now set the targetApplication data for real
	$tarray = array();
		
	if(is_array($data["extensions"])) {
		foreach($data["extensions"] as $key => $ext) {
			if(is_array($ext["targetApplication"])){
				foreach($ext["targetApplication"] as $node => $ta) {
					$id 		= $data[$ta][EM_NS . "id"];
					$id         = substr($id, 1, strlen($id)-2); // strip { and }
					$minVer     = $data[$ta][EM_NS . "minVersion"];
					$maxVer     = $data[$ta][EM_NS . "maxVersion"];
					$updateLink = $data[$ta][EM_NS . "updateLink"];
					$tarray[$key][$id]["minVersion"] = $minVer;
					$tarray[$key][$id]["maxVersion"] = $maxVer;
					$tarray[$key][$id]["updateLink"] = $updateLink;
				}
			}
		}
				
		foreach($data["extensions"] as $key => $ext) {
			$s = explode(":", $key);
			$uuid = $s[0];
			$res["extensions"][$uuid][$ext["version"]] = $tarray[$key];
		}
	}
	
	print_r($res);
	
	return $res;
}

function __update_mf_statement_handler(
	&$data,
	$subject_type,
	$subject,
	$predicate,
	$ordinal,
	$object_type,
	$object,
	$xml_lang )
{
	global $singleprops, $multiprops, $l10nprops;
		
	// look for props on the install manifest itself
	if(preg_match("/^" . MF_EXTS . "/", $subject) ) { // permet de matcher les urn:mozilla:extension des updaterdf

		// we're only really interested in EM props
		$l = strlen(EM_NS);
		if( strncmp($predicate,EM_NS,$l) == 0) {
			$prop = substr($predicate,$l,strlen($predicate)-$l);
		
			$ver  = explode(":", $subject);
			$uuid = substr($ver[3], 1, strlen($ver[3])-2);

			if(count($ver) == 5) {
				$vers = $ver[4];
				
				$key = $uuid.":".$vers;
				
				if($singleprops[$prop]) {
					$data["manifest"][$prop] = $object;
				}
				elseif($multiprops[$prop]) {
					$data["manifest"][$prop][] = $object;
				}
				elseif($l10nprops[$prop]) {
					// handling these separately
					// so we can handle multiple languages
					if($xml_lang) {
						$lang = $xml_lang;
					}
					else {
						// default to en-US
						$lang = "en-US";
					}
					$data["manifest"][$prop][$lang] = $object;
				}
				
				$data["extensions"][$key] = $data["manifest"];
			}
        }
	}
	else {
		// just save it, probably a targetApplication or something
		// shouldn't ever have multiple targets, doesn't matter
		$data[$subject][$predicate] = $object;
	}
}

?>
