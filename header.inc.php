<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
header('Content-Type: ' . contenttype() . '; charset='.$language->charsetselect());
echo '<?xml version="1.0" encoding="'.$language->charsetselect().'" ?>';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>MEIK : Multi Extension Installer Kit - <?php echo MEIK_VERSION; ?></title>
	<script type="text/javascript" src="meik.js"></script>
	<link rel="stylesheet" type="text/css" media="screen" title="MEIK Default Style" href="meik.css" />
</head>

<body onload="javascript:load();">
<div id="head">
	<?php
		echo $language->showlangselect();
	?>
	<h1>MEIK : Multi Extension Installer Kit <span class="version">(v<?php echo MEIK_VERSION; ?>)</span></h1><p class="logo-mini"><img src="images/meik-logo-mini.png" alt="Mini MEIK's Logo" /></p>
</div>