<?php
/* $Id$ */
/**
 * Copyright (c) <2006> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
 /**
  * Here, we'll have an abstraction over RDF files.
  * 
  * It will be used for install.rdf and update.rdf parsing.
  */
 
 // http://frenchmozilla.sourceforge.net/cz/versions.php

class MofoRDF
{
	private $XML_Parser				= null; ///< The XML parser object of our XML file !
	private $DOM_XPath				= null; ///< The DOM XPath object of our XML stream ;)
	private $namespace_rdf			= null; ///< The XML NameSpace used for RDF.
	private $namespace_em			= null; ///< The XML NameSpace used for em.
	private $rdftype				= null; ///< Set the RDF file Type (MOFO_UPDATE_RDF, or MOFO_INSTALL_RDF)
	private $allowed_rdftype		= array(); ///< Set all the allowed RDF type constants.
	private $callback_rdftype		= array(); ///< Contains the corresponding function for each rdf_type to retrieve data.
	private $updaterdf_description_array	= array(); ///< The Array that will be used to store all UUIDs & versions of extensions. (Update RDF)
	private $updaterdf_targets_array		= array(); ///< This array will contain all version informations, such as target application, version ... (Update RDF)
	private $installrdf_description_array	= array(); ///< The Array that will be used to store all extensions informations. (Install RDF)
	private $installrdf_targets_array		= array(); ///< This array will contain all version informations, such as target application, version ... (Install RDF)
	
	const MOFO_UPDATE_RDF			= 1; ///< Defines the MOFO_UPDATE_RDF to 1.
	const MOFO_INSTALL_RDF			= 2; ///< Defines the MOFO_INSTALL_RDF to 1.
		
	/**
	 * This is constructor, checking for DOMDocument & DOMXPath, and loading data into private variables.
	 * 
	 * @param string $xmldata Contains the whole XML data we want to play with.
	 * 
	 * @return Fully working object, or an error is any.
	 */
	function __construct($xmldata)
	{
		if(!class_exists("DOMDocument") || !class_exists("DOMXPath"))
			return ERR_UNAVAILABLE_CLASS;
		
		if(empty($xmldata))
			return ERR_EMPTY_PARAMS;
			
		$this->_fill_allowed_rdf_type();
		
		/**
		 * We have something :)
		 * Hope it's some XML !
		 */
		$this->XML_Parser = new DOMDocument();
		$this->XML_Parser->loadXML($xmldata);
		$this->DOM_XPath = new DOMXPath($this->XML_Parser);
		$this->__rdf_set_namespace();
	}
	
	/**
	 * This function is used to set the $this->rdftype private variable.
	 * 
	 * @return true if $type was correctly set up (generally, meaning $type is correct and allowed.) and false either.
	 */
	public function rdf_set_type($type)
	{
		if(!in_array($type, $this->allowed_rdftype)) {
			error(_("Not allowed RDF type" . ' : ' . $type ));
			return false;
		}
		
		$this->rdftype = $type;
		return true;
	}
	
	/**
	 * Return the content of correct function depending on the $this->rdftype.
	 * 
	 * @return Good stuff :))
	 */
	public function get_array()
	{
		$func_name = $this->callback_rdftype[$this->rdftype];
		if(method_exists($this, $func_name)) {
			// debug(_("Calling") . $func_name . ' ' . _("(due to") . ' ' . $this->rdftype . ") !");
			return call_user_func(array($this, $func_name)); ///< This allows us to call $func_name which lives inside $this :)
			// call_user_method is deprecated ! => we use call_user_func
		} 
		else return false;
	}
	
	/**
	 * Get the array stored within $this->updaterdf_targets_array, which contains all data !
	 * 
	 * @return $this->updaterdf_targets_array, as an Array.
	 */
	private function updateRDF_get_array()
	{
		$this->_updaterdf_prepare_descriptions();
		$this->_updaterdf_prepare_targets();
		$arr = array("extensions" => $this->updaterdf_targets_array);
		return $arr;
	}
	
	private function installRDF_get_array()
	{
		/**
		$infos = array();
		$infos["version"] = $res["version"];
		$infos["extension"] = $res["name"];
		$infos["description"] = $res["description"];
		$infos["uuid"] = $this->extractfrombrackets($res["id"]); // Get uid from {uid}
		$infos["compat"] = $this->getcompat($res["targetApplication"]);
		$infos["time"] = filemtime($dir.$entry);
		$infos["updateURL"] = $res["updateURL"];
		 */
		$this->_installrdf_prepare_descriptions();
		$this->_installrdf_prepare_targets();
		$arr = $this->installrdf_targets_array;
				
		return $arr;
	}
	
	/**
	 * This function prepares the $this->allowed_rdftype array, adding $type to the array.
	 * 
	 * @param string $type The constant to add.
	 * @param string $callback The function to call.
	 * 
	 * @return true if something was added. false either.
	 */
	private function _set_allowed_rdf_type($type, $callback)
	{
		$return = false;
		
		if(is_array($this->allowed_rdftype)) {			
			$count_allowed  = count($this->allowed_rdftype);
			$count_callback = count($this->callback_rdftype);
			$allowed  = array_push($this->allowed_rdftype, $type);
			$this->callback_rdftype[$type] = $callback;
			$callback = count($this->callback_rdftype);
			
			return (($count_allowed+1  == $allowed)
				&&	($count_callback+1 == $callback));
		}
		
		return $return;
	}
	
	
	/**
	 * This function prepares the $this->allowed_rdftype array, using $this->_set_allowed_rdf_type()
	 *  
	 * @return true if something was added. false either. (depends on $this->_set_allowed_rdf_type() value)
	 */
	private function _fill_allowed_rdf_type()
	{
		$return = false;
		
		$return  = $this->_set_allowed_rdf_type("MOFO_UPDATE_RDF",  "updateRDF_get_array");
		$return &= $this->_set_allowed_rdf_type("MOFO_INSTALL_RDF", "installRDF_get_array");
		
		return $return;
	}
	
	/**
	 * This function sets an array of all <RDF:Description about=""> elements.
	 * 
	 * Array contains only the UUID & version of each extensions we'll talk about.
	 * 
	 * @return Nothing, puts all stuff in $this->updaterdf_description_array
	 * 
	 */
	private function _updaterdf_prepare_descriptions()
	{
		// Only match General extensions UUID, not subversions :)
		// $XPath = $this->namespace_rdf . 'Description/' . $this->namespace_em . 'updates/' . $this->namespace_rdf . 'Seq/' . $this->namespace_rdf . 'li';
		$XPath		= "/data:RDF/data:Description/info:updates/data:Seq/data:li";
		$XPath_desc	= "/data:RDF/data:Description";
		// echo "XPath == $XPath<br />\n";
		$_res = $this->DOM_XPath->query($XPath);
		$desc = $this->DOM_XPath->query($XPath_desc);

		if($_res->item(0) && $desc->item(0)) {
			$i = 0;
			// List all items found with this XPath
			while( ($item = $_res->item($i)) !== NULL) {
				$uuid_ext	= $desc->item($i)->getAttribute("about");
				$value		= $item->getAttribute("resource"); ///< and get their resource attribute.
				
				if(empty($value)) {///< check that we have a <RDF:li resource="urn:mozilla:extension:uuid:version"></RDF:li>
					// no 'resource' is here. Maybe it's a gore-RDF with <RDF:Description> insinde <RDF:li> ...
					$newPath = "data:Description";
					$newbase = $this->DOM_XPath->query($newPath, $item); ///< Check if we have some <RDF:Description> inside our <RDF:li>.
					
					if($newbase->item(0)) { ///< Yeah :)) We'll be able to find <em:version> here :)
						$versionPath	= "info:version";
						$version		= $this->DOM_XPath->query($versionPath, $newbase->item(0));
						
						if($version->item(0)) {
							$vers			= $version->item(0)->nodeValue; ///< read <em:version>
							$value 			= $uuid_ext . ':' . $vers; ///< creates the correct string so that we'll be able to play, after.
							$update_base	= $newbase; ///< set 'DocumentRoot' of update data. He,re it's in '/data:RDF/data:Description/info:updates/data:Seq/data:li/data:Description'
						}
					}
				} else {
					$update_base			= $desc;  ///< set 'DocumentRoot' of update data. /data:RDF/data:Description'
				}
				
				// Now, extract stuff from $value :))
				$uuid = $this->__rdf_extract_uuid_and_version($value); ///< extract '{UUID}:version'
				if($uuid)
					$this->updaterdf_description_array[] = array
						(
							"uuid" => $uuid, ///< and store this string within the array. This will be used by _updaterdf_prepare_targets().
							"base" => $update_base
						);
				
				$i++;
			}
		} else {
			return false;
		}
		
		// var_dump($this->rdf_description_array);
	}
	
	/**
	 * This function extract data for corresponding each entry of $this->updarerdf_description_array
	 * 
	 * @return true if all goes well, false either.
	 */
	private function _updaterdf_prepare_targets()
	{		
		// var_dump($this->rdf_description_array);
		foreach($this->updaterdf_description_array as $data) {
			// We first match the whole extensions
			$infos		= "urn:mozilla:extension:" . $data["uuid"];
			//$XPath		= '/data:RDF/data:Description[@about="' . $infos . '"]'; ///< XPath to search for all RDF:Description concerning $infos.
			// echo "XPath == $XPath<br />\n";
			// $base		= $this->DOM_XPath->query($XPath);
			$base		= $data["base"];
			$ext_uuid	= $this->__rdf_extract_uuid($infos);
			$ext_vers	= $this->__rdf_extract_vers($infos);
			
			if($base->item(0)) { ///< Right, there is at least one extension.
				if(!$this->__rdf_extract_update_data($base, $ext_uuid, $ext_vers))
					return false;
			} // end of test for item presence.
		}
		
		return true;
	}
	
	/**
	 * This function sets an array of all <RDF:Description about=""> elements.
	 * 
	 * Array contains only the UUID & version of each extensions we'll talk about.
	 * 
	 * @return Nothing, puts all stuff in $this->installrdf_description_array
	 * 
	 */
	private function _installrdf_prepare_descriptions()
	{
		// Only match General extensions UUID, not subversions :)
		$XPath_desc	= "/data:RDF/data:Description[@about='urn:mozilla:install-manifest']";
		$desc = $this->DOM_XPath->query($XPath_desc);

		if($desc->item(0)) {
			$i = 0;
			// List all items found with this XPath
			while( ($item = $desc->item($i)) !== NULL) {
				$uuid			= $this->DOM_XPath->query("info:id", $item); ///< Get {UUID}.
				$version		= $this->DOM_XPath->query("info:version", $item); ///< Get Version
				$contributor	= $this->DOM_XPath->query("info:contributor", $item); ///< Get Contributors
				$name			= $this->get_dom_value("info:name", $item); ///< Get name.
				$description	= $this->get_dom_value("info:description", $item); ///< Get description
				$creator		= $this->get_dom_value("info:creator", $item); ///< Get creator.
				$homepageURL	= $this->get_dom_value("info:homepageURL", $item); ///< Get homepageURL
				$updateURL		= $this->get_dom_value("info:updateURL", $item); ///< Get updateURL
				$iconURL		= $this->get_dom_value("info:iconURL", $item); ///< Get iconURL
				
				$str = $uuid->item(0)->nodeValue . ":" . $version->item(0)->nodeValue;
				
				$this->installrdf_targets_array["id"]			= $uuid->item(0)->nodeValue;
				$this->installrdf_targets_array["version"]		= $version->item(0)->nodeValue;
				$this->installrdf_targets_array["name"]			= $name;
				$this->installrdf_targets_array["description"]	= $description;
				$this->installrdf_targets_array["creator"]		= $creator;
				$this->installrdf_targets_array["homepageURL"]	= $homepageURL;
				$this->installrdf_targets_array["updateURL"]	= $updateURL;
				$this->installrdf_targets_array["iconURL"]		= $iconURL;
				
				
				if($contributor->item(0)){
					$j = 0;
					while( ($con = $contributor->item($j)) !== NULL) {
						$this->installrdf_targets_array["contributor"][] = $con->nodeValue;
						$j++;
					}
				}
				
				if($uuid && $version) {
					$this->installrdf_description_array[] = array
						(
							"uuid" => $str, ///< and store this string within the array. This will be used by _updaterdf_prepare_targets().
							"base" => $item
						);
				}
				
				$i++;
			}
		} else {
			return false;
		}
		return true;
	}
	
	/**
	 * This is some kind of alias for $this->DOM_XPath->query()
	 * 
	 * @param $target The XML target (e.g. 'info:name' (which maps to 'em:name')).
	 * @param $base The base DOM_XPath to start from.
	 * 
	 * @return Some string, of good, or false.
	 */
	private function get_dom_value($target, $base)
	{
		if( ($elem = $this->DOM_XPath->query($target, $base)) !== NULL) {
			if( ($val = $elem->item(0)->nodeValue) !== NULL)
				return $val;
			else
				return false;
		} else {
			return false;
		}
	}
	
	/**
	 * This function extract data for each targetApplication found.
	 * 
	 * @return true if all goes well, false either.
	 */
	private function _installrdf_prepare_targets()
	{
		foreach($this->installrdf_description_array as $data) {
			$path			= 'info:targetApplication';
			$applications	= $this->DOM_XPath->query($path, $data["base"]);
			
			if($applications->item(0)){
				$j = 0;
				
				while( ($app = $applications->item($j)) !== NULL) {
					$id			= $this->DOM_XPath->query("data:Description/info:id", $app);
					$minversion	= $this->DOM_XPath->query("data:Description/info:minVersion", $app);
					$maxversion	= $this->DOM_XPath->query("data:Description/info:maxVersion", $app);

					/**
					 * And now, we place all this data within a little array :)
					 */
					$id = $this->__rdf_strip_brackets($id->item(0)->nodeValue);
					$this->installrdf_targets_array["targetApplication"]["{" . $id . "}"] = array
						(
							"minVersion" => $minversion->item(0)->nodeValue, ///< minimum version for target.
							"maxVersion" => $maxversion->item(0)->nodeValue, ///< maximum version for target.
						);
					
					$j++;
				}
			}
		}
		
		return true;
	}
	
	private function __rdf_extract_update_data($base, $ext_uuid, $ext_vers)
	{
		if(!$base->item(0))
			return false;
		
		$i = 0;
		while( ($item = $base->item($i)) !== NULL ) { ///< loop while we find item corresponding to the above XPath.
			// Then, just have a look at version and target applications.
			$vers			= $this->DOM_XPath->query('info:version', $item); ///< get local em:version data
			$applications	= $this->DOM_XPath->query('info:targetApplication', $item); ///< prepare for loop on all applications target :)
			
			if($applications->item(0)) { ///< same as above. Prepare to loop on all applications this extensions has as a target.
				$j = 0;
				while ( ($app = $applications->item($j)) !== NULL) { ///< and now, loop.
				/**
				 * The following get all informations about the target application, such as :
				 *  - UUID
				 *  - minVersion
				 *  - maxVersion
				 *  - updateLink
				 */
					$id			= $this->DOM_XPath->query("data:Description/info:id", $app);
					$minversion	= $this->DOM_XPath->query("data:Description/info:minVersion", $app);
					$maxversion	= $this->DOM_XPath->query("data:Description/info:maxVersion", $app);
					$updatelink	= $this->DOM_XPath->query("data:Description/info:updateLink", $app);
					
					/* echo "Ok, on a l'appli " . $id->item(0)->nodeValue . ", minVer=" . $minversion->item(0)->nodeValue . ", maxVer=" . $maxversion->item(0)->nodeValue . " et lien=" . $updatelink->item(0)->nodeValue . " � <br />\n"; */
					
					/**
					 * And now, we place all this data within a little array :)
					 */
					$id = $this->__rdf_strip_brackets($id->item(0)->nodeValue);
					$this->updaterdf_targets_array[$ext_uuid][$ext_vers][$id] = array
						(
							"minVersion" => $minversion->item(0)->nodeValue, ///< minimum version for target.
							"maxVersion" => $maxversion->item(0)->nodeValue, ///< maximum version for target.
							"updateLink" => $updatelink->item(0)->nodeValue  ///< update url for target.
						);
					$j++;
				} // end of loop on all the applications.
			} // end of test for applications presence.
		$i++;
		} // end of loop on all the items.
		
		return true;
	}
	
	/**
	 * This rfunctions return the UUID contained within a string like 
	 *   'urn:mozilla:extension:{59c81df5-4b7a-477b-912d-4e0fdf64e5f2}'
	 * 
	 * @param string $string The string to analyse
	 * 
	 * @return A string containing the UUID (such as 59c81df5-4b7a-477b-912d-4e0fdf64e5f2),
	 * or false if any error occurs.
	 */
	private function __rdf_extract_uuid($string)
	{
		if(preg_match("/^urn:mozilla:extension:\{([0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12})\}/", $string, $uuid))
			return $uuid[1];
		else
			return false;
	}
	
	/**
	 * This function returns the Version contained within a string like 
	 *   'urn:mozilla:extension:{59c81df5-4b7a-477b-912d-4e0fdf64e5f2}:0.1.2.3'
	 * 
	 * @param string $string The string to analyse
	 * 
	 * @return A string containing the version (such as 0.1.2.3),
	 * or false if any error occurs.
	 */
	private function __rdf_extract_vers($string)
	{
		if(preg_match("/^urn:mozilla:extension:\{[0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12}\}:([0-9].*)/", $string, $uuid))
			return $uuid[1];
		else
			return false;
	}
	
	
	/**
	 * This function returns the UUID and version contained within a string like 
	 *   'urn:mozilla:extension:{59c81df5-4b7a-477b-912d-4e0fdf64e5f2}:0.1.2.3'
	 * 
	 * @param string $string The string to analyse
	 * 
	 * @return A string containing the UUID & version ({59c81df5-4b7a-477b-912d-4e0fdf64e5f2}:0.1.2.3),
	 * or false if any error occurs.
	 */
	private function __rdf_extract_uuid_and_version($string)
	{
		if(preg_match("/^urn:mozilla:extension:(\{[0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12}\}:[0-9].*)/", $string, $uuid))
			return $uuid[1];
		else
			return false;
	}
	
	/**
	 * This functions sets the namespace used in the XML document withion a private variable.
	 * 
	 * @return Nothing :).
	 */
	private function __rdf_set_namespace()
	{
		/**
		 * Set RDF and EM prefix within $this->namespace_rdf & $this->namespace_em.
		 * Not sure it's really needed, now.
		 */
		$this->namespace_rdf = $this->XML_Parser->lookupPrefix("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		$this->namespace_em  = $this->XML_Parser->lookupPrefix("http://www.mozilla.org/2004/em-rdf#");
		
		/**
		 * Set the data & info pseudo-namespace for using within XPath queries, with the following convention :
		 *  - data is refering to RDF namespace.
		 *  - info is refering to EM  namespace.
		 */
		$this->DOM_XPath->registerNamespace("data", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		$this->DOM_XPath->registerNamespace("info", "http://www.mozilla.org/2004/em-rdf#");
	}
	
	/**
	 * This function returns the passed string without '{' and '}'.

	 * 
	 * @param string $string The string to clean.
	 * 
	 * @return The same string, without '{' and '}', or false if no bracket is fount.
	 */
	private function __rdf_strip_brackets($string)
	{
		if(preg_match("/\{(.*)\}/", $string, $brackets))
			return $brackets[1];
		else
			return false;
	}

}
/** used for debug :) *
 * 
 */
/*
$rdf = file_get_contents("http://lissyx.dyndns.org/~lissyx/stuff/czfrinstall.xml"); 
$xml = new MofoRDF($rdf);
$xml->rdf_set_type("MOFO_INSTALL_RDF");
print_r($xml->get_array());*/

/*echo "<br /><hr /><br />\n";

$rdf = file_get_contents("http://extensions.roachfiend.com/update.rdf");
$xml = new MofoRDF($rdf);
$xml->rdf_set_type("MOFO_UPDATE_RDF");
var_dump($xml->get_array());*/

?>