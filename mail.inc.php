<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <lissyx@infos-du-net.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

class EmailCheck
{
	public $email; // the mail to check
	public $debug; // turn on or off debug. Boolean.
	
	private $serv; // the mail domain (e.g. : in email@host.domain.tld, the $serv = host.domain.tld)
	private $servers; // the MX records
	private $mxsock; // the socket connexion established, during mail testing process.
	
	function __construct()
	{
		$this->debug = false;
	}
	
	function checkemail()
	{
		$arr = explode("@", $this->email);
		$this->serv = $arr[1];
		$this->servers = $this->getMX($this->email);
		return $this->check();
	}
	
	function connectTo($server, $port)
	{
		$socket = socket_create (AF_INET, SOCK_STREAM, 0); 
		$result = @socket_connect ($socket, $server, $port);
	
		if(!$result) {
			return false;
			exit;
		}
		return $socket;
	}
	
	function sendTo($buffer)
	{
		$buffer .= "\r\n";
		if(@socket_write($this->mxsock, $buffer, strlen($buffer))) {
			return true;
		} else {
			return false;
		}
	}
	
	function read()
	{
		$out = @socket_read($this->mxsock, 2048);
		return $out;
	}

	function getSMTPcode($str)
	{
		$code = substr($str, 0, 3);
		return $code;
	}
	
	function getMX($email)
	{
		if($this->isWindows()) {
			if(!$this->Wgetmxrr($this->serv, $mxrrp)) {
				$mxrrp[] = gethostbyname($this->serv);
			}
		} else { // We're not under Windows !
			if(!getmxrr($this->serv, $mxrrp)) {
				$mxrrp[] = gthostbyname($this->serv);
			}
		}
		return $mxrrp;
	}
	
	function MXtest($MX)
	{
		$mxsock = socket_create (AF_INET, SOCK_STREAM, 0);
		if(@socket_connect($mxsock, $MX, 25)) {
			return true;
		} else {
			return false;
		}
	}
	
	function isWindows()
	{
		return (isset($_SERVER["WINDIR"]));
	}
	
	function Wgetmxrr($hostname, &$mxhosts)
	{
	   $mxhosts = array();
	   
	   exec('nslookup -type=mx '.$hostname, $result_arr);
	   foreach($result_arr as $line)
	   {
		   if (preg_match("/.*preference = (\d+), mail exchanger = (.*)/", $line, $matches)) {
			   $mxhosts[] = $matches[2];
		   }
	   }
	   return( count($mxhosts) > 0 );
	}
	
	function getSMTPAns()
	{
		return $this->getSMTPcode($this->read($this->mxsock));
	}
	
	function ask_and_check($question, $answer)
	{
		$sent = $this->sendTo($question);
		$ans  = $this->getSMTPAns();
		if($this->debug) {
			echo "Asking for : '$question'\n";
			echo "Awaiting   : '$answer'\n";
			echo "Got answer : '$ans'\n";
		}
		return ($ans == $answer);
	}
	
	function check()
	{
		$mail = $this->email;
		$servers = $this->servers;
		
		$questions = array(
			"HELO ".$_SERVER["SERVER_NAME"],
			"MAIL FROM: <$mail>",
			"RCPT TO: <$mail>",
			"QUIT"
			);
		$reponses = array(
			"250",
			"250",
			"250",
			"221"
			);
		
		foreach($servers as $server) {
			if($this->MXtest($server)) {
				if($this->mxsock = $this->connectTo($server, 25)) {
					if($this->getSMTPAns() == "220") {
						$k = count($questions);
						for($i = 0; $i < $k; $i++)
						{
							if(!$this->ask_and_check($questions[$i], $reponses[$i])) {
								return false;
							}
						}
						return true; // If we came to here, we haven't got a ask_and_check which was false :)
					} else {
						if($this->debug) {
							echo "Cannot initiate SMTP ($server).\n";
						}
						return false;
					}
				} else {
					if($this->debug) {
						echo "Cannot connect to SMTP ($server).\n";
					}
					return false;
				}
			} else {
				if($this->debug) {
					echo "Cannot find MX ($server).\n";
				}
				return false;
			}
		}
	}
}
?>