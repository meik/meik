<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

error_reporting(E_ALL);
$time_start = microtime(true);
include_once("libmeik.inc.php");
$meik = new Meik();
$meik->debug = true;
$todo = getvar("nxt");

switch($todo) {
	case "auth":
		$meik->debug = false;
		$login = getvar("login");
		$pass  = getvar("pass");
		
		$rsql->hiderr = true;
		if($meik->authuser($login, $pass)) {
			include_once("header.inc.php");
			include_once("meik.php");
		} else {
			header("Location: meik.index.php?msg=1");
		}
		break;
		
	case "unauth":
		$meik->debug = false;
		$session->deletesession();
		header("Location: meik.index.php");
		break;
		
	case "inscr":
		include_once("header.inc.php");
		$meik->debug = false;
		include_once("inscription.inc.php");
		break;
		
	case "validate":
		include_once("header.inc.php");
		$meik->debug = false;
		$login = getvar("login");
		$pass  = getvar("pass");
		$nom   = getvar("nom");
		$email = getvar("email");
		
		echo $session->showunauth();
		echo '<div class="contenu">';
		$meik->registeruser($login, $pass, $nom, $email);
		echo '</div>';
		
		break;
	
	case "setlang":
		$lang = getvar("lang");
		$session->savelang($lang);
		if(isset($_SERVER["HTTP_REFERER"])) {
			$cible = $_SERVER["HTTP_REFERER"];
		} else {
			$cible = "meik.index.php?auth";
		}
		header("Location: " . $cible);
		break;
	
	default:
		include_once("header.inc.php");
		$meik->debug = false;
		if(!$session->hascookie()) {
			include_once("connexion.inc.php");
		} else {
			include_once("meik.php");
		}
		break;
}

$time_end = microtime(true);
$time = round($time_end - $time_start, 4);

$rsql->disconnect();

include_once("footer.inc.php");
?>