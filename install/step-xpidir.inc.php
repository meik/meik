<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

$steptitle    = _("XPI Directory Creation");
$stepfinished = _("XPI directory has been successfully created.");
$steploaded   = '
<p>
' . _("We are now going to create some needed directory. Ensure your web user has enough right for this, otherwise it will fail.") . '
</p>
';

function process_step()
{
	$xpidir = dirname(dirname(__FILE__)) . ( (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/' ) . "xpis" ;
	if(!@mkdir($xpidir)) {
		return array("1", "1", _("Cannot create XPI directory."));
	} else {
		return array("0");
	}
}

?>
