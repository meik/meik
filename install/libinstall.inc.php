<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

/**
 * This lib contains all functions needed for the installation process,
 * such as HTML template rendering, or database creation.
 */

$steps = array (
	"pres",
	"php",
	"db-create",
	"db-config",
	"web-config",
	"windows",
	"xpidir",
	"admin",
	"authbackend"
	);

// we'll need some functions from MEIK code.
include_once("../functions.inc.php");
include_once("../defines.inc.php");

// We need session also ...
include_once("../libsession.inc.php");
$session = new Session();

// We starts MySQL ...
include_once("../libsql.inc.php");
$rsql = new SQL();

// We play with languages
include_once("../liblang.inc.php");
$language = new Lang();
$language->part = "messages";
$language->intl();

// We need some part of Administration codaz.
include_once("../libmeik.inc.php");
$meik = new Meik();
include_once("../libadmin.inc.php");
$admin = new Admin();

/**
 * This is used to have a central 
 */
function htmlrender($code)
{
	$base = '<?xml version="1.0" encoding="'.$GLOBALS["language"]->charsetselect().'" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>MEIK : Multi Extension Installer Kit - ' . MEIK_VERSION . '</title>
	<script type="text/javascript" src="../meik.js"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="meik.install.css" />
</head>

<body onload="javascript:load();">
<div id="head">
	'. $GLOBALS["language"]->showlangselect() .'
	<h1>MEIK : Multi Extension Installer Kit <span class="version">(v' . MEIK_VERSION . ')</span></h1><p class="logo-mini"><img src="../images/meik-logo-mini.png" alt="Mini MEIK\'s Logo" /></p>
</div>
 	' . $code . '
	
	' . $GLOBALS["rsql"]->totalcount() . '

	<p id="credits" class="credits">
		'._("Contains parts of").' <a href="https://addons.mozilla.org">UMO</a> - 
		'._("This page might be ") . ' <a href="http://validator.w3.org/check?uri=referer">XHTML 1.0 Strict</a> ' . _("valid.") . ' - 
		'._("CSS also should be ").' <a href="http://jigsaw.w3.org/css-validator/check/referer">'._("valid.").'</a>
	</p>
</body>
</html>
';
	return $base;
}
?>
