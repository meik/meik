<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

$steptitle    = _("Administrator Account");
$stepfinished = _("Adminitsrator Account has been correctly created.");
$steploaded   = '
<p>' . _("Now, we need your help. You have to choose a user name and a password, that will be setup as administrator accoutn for MEIK. This mean that you will be able to do all you want using this account.") . '
</p>
	<table>
		<tbody>
			<tr>
				<td class="align-right">' . _("Username"). ' :</td>
				<td><input type="text" name="admin_user" value="root" /></td>
			</tr>
			<tr>
				<td class="align-right">' . _("Password"). ' :</td>
				<td><input type="password" name="admin_password" value="" /></td>
			</tr>
			<tr>
				<td class="align-right">' . _("Name"). ' :</td>
				<td><input type="text" name="admin_name" value="" /></td>
			</tr>
			<tr>
				<td class="align-right">' . _("Email"). ' :</td>
				<td><input type="text" name="admin_email" value="" /></td>
			</tr>
		</tbody>
	</table>
';


function process_step()
{
	$db = $_SESSION["db"];
	$user = getvar("admin_user");
	$pass = getvar("admin_password");
	$name = getvar("admin_name");
	$mail = getvar("admin_email");
	
	if(!@mysql_connect($db[0], $db[2], $db[3])) {
		return array("1", "1", _("Cannot connect to MySQL server") . " " . $db[2] . ":" . $db[3] . "@" . $db[0] . " " . _("MySQL Error") . ' : ' . mysql_error());
	}
	
	if(!@mysql_select_db($db[1])) {
		return array("1", "2", _("Cannot select MySQL database") .  " " . $db[2] . ":" . $db[3] . "@" . $db[0] . " " . _("MySQL Error") . ' : ' . mysql_error());
	}
	
	$ajout = "INSERT INTO `meik_users` VALUES (1, 0, '$user', MD5('$pass'), '$name', '$mail', 1, 1, 0);";
	
	if(!@mysql_query($ajout)) {
		return array("1", "2", _("Cannot add MEIK administrator to MySQL database") .  " " . $db[2] . ":" . $db[3] . "@" . $db[0] . " " . _("MySQL Error") . ' : ' . mysql_error());
	}
	
	unset($_SESSION["db"]);
	return array("0");
}
?>
