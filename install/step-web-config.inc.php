<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

$steptitle    = _("Web Server Configuration");
$stepfinished = _("Web Server Configuration has been successfully written.");
if(!rewrite()) {
	$msg = '
<div class="msg-error">
	' . _("We can't find the rewrite module in your Apache configuration, please check your server configuration.") . '
</div>
';
}
$steploaded = '
	<p>
	' . _("Please check that those information are good, it's critical for MEIK to be accessible.") . '
	</p>
	' . $msg . '
	<table>
		<tbody>
			<tr>
				<td class="align-right">' . _("Public path for MEIK"). ' :</td>
				<td><input type="text" name="path" value="' . dirname(dirname($_SERVER["REQUEST_URI"])) . '/' . '" size="64" /></td>
			</tr>
		</tbody>
	</table>
';

function rewrite()
{
	return in_array("mod_rewrite", apache_get_modules());
}

function process_step()
{
	$path = getvar("path");
	if(!rewrite()) {
		// we can't use mod_rewrite ... uggly hack : index.php -> meik.index.php
		$data = '<?php header("Location: meik.index.php"); ?>';
		if($handle = @fopen(dirname(dirname(__FILE__)) . ( (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/' ) . "index.php", "w")) {
			if(false ===  @fwrite($handle, $data)) {
				return array("1", "1", _("Cannot write into file") . ' index.php');
			}
		} else {
			return array("1", "2", _("Cannot open file") . ' index.php ' . _("for writing."));
		}
	} else {
		// We can use mod_rewrite !
		$data = 'RewriteEngine On
RewriteBase ' . $path . '
RewriteRule ^$ meik.index.php [L]';
		if($handle = @fopen(dirname(dirname(__FILE__)) . ( (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/' ) . ".htaccess", "w")) {
			if(false ===  @fwrite($handle, $data)) {
				return array("1", "3", _("Cannot write into file") . ' .htaccess');
			}
		} else {
			return array("1", "4", _("Cannot open file") . ' .htaccess ' . _("for writing."));
		}
	}
}
?>
