<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

$steptitle    = _("Windows Detection");
$stepfinished = _("Windows workaround correctly applied.");
$steploaded   = '
<p>
' . _("We are going to check if your webserver is using a Windows-based OS, and we'll apply workaround for MEIK if needed."). '
</p>
';

function process_step()
{
	$filename = dirname(dirname(__FILE__)) . ( (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/' ) . "liblang.inc.php";
	if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		// Windows OS detected
		$liblang  = file($filename);
		if(!in_array("putenv", $liblang)) {
			// no trace of `` putenv("LANG=$lang"); // needed under Windows. ``
			$newarr = array();
			foreach($liblang as $ligne)
			{
				if(strpos($ligne, "setlocale")) {
					$newarr[] = '		putenv("LANG=$lang"); // needed under Windows.
';
					$newarr[] = $ligne;
				} else {
					$newarr[] = $ligne;
				}
			}
			$data = implode("", $newarr);
			if($handle = @fopen($filename, "w")) {
				if(false ===  @fwrite($handle, $data)) {
					return array("1", "1", _("Cannot write into file") . ' liblang.inc.php');
				}
			} else {
				return array("1", "2", _("Cannot open file") . ' liblang.inc.php ' . _("for writing."));
			}
		}
	} else {
		return array("0");
	}
}
?>
