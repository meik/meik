<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
$time_start = microtime(true);
include_once("libinstall.inc.php");

$stepid = getvar("step");
$valid  = getvar("validstep");
$array = array (
  "id"    => "install",
  "title" => _("Installations steps"),
  "data"  => array (
  		array ("?step=0",      _("Presentation"),             ($stepid == 0), ($stepid > 0)),
  		array ("?step=1",      _("PHP Checks"),               ($stepid == 1), ($stepid > 1)),
  		array ("?step=2",      _("Database Creation"),        ($stepid == 2), ($stepid > 2)),
  		array ("?step=3",      _("Database Configuration"),   ($stepid == 3), ($stepid > 3)),
  		array ("?step=4",      _("Web Server Configuration"), ($stepid == 4), ($stepid > 4)),
  		array ("?step=5",      _("Windows Detection"),        ($stepid == 5), ($stepid > 5)),
  		array ("?step=6",      _("XPI Directory Creation"),   ($stepid == 6), ($stepid > 6)),
  		array ("?step=7",      _("Administrator Account"),    ($stepid == 7), ($stepid > 7))
  		)
	);

$last_step = count($array["data"])-1;

if(empty($stepid) || ($stepid < 0 || $stepid > ($last_step+1))) {
	$stepid = 0;
}

if($stepid != $last_step && !is_readable('step-' . $steps[$stepid] . '.inc.php')) {
	$stepid = 0;
	error(_("Please check your install/ directory, some files are not readable or missing !"));
}

if(is_numeric($valid))
	$stepid = $valid;

if(!is_numeric($valid)) {
	if($stepid < $last_step) {
		// we have to load one of the steps ...
		@require_once("step-" . $steps[$stepid] . ".inc.php");
		// it's a step !
		$step = '
<div id="welcome">
	<h2>' . _("Welcome to MEIK installation wizard.") . '</h2>
</div>
	' . makemenu($array) . '
<div id="content" class="contenu">
	<h3>' . $steptitle . '</h3>
	<form action="?validstep=' . ($stepid). '" method="post">
	<p>' . $steploaded . '
		<br />
		<input type="submit" value="' . _("Validate") . '" />
	</p>
	</form>
</div>';
	} elseif($stepid == $last_step) {
		// we have to load one of the steps ...
		@require_once("step-end.inc.php");
		// it'sthe END
		$step = '
<div id="welcome">
	<h2>' . _("Welcome to MEIK installation wizard.") . '</h2>
</div>
	' . makemenu($array) . '
<div id="content" class="contenu">
	<h3>' . $steptitle . '</h3>
	<p>
		' . $steploaded . '
	</p>
</div>';
	}
} else {
	@require_once("step-" . $steps[$valid] . ".inc.php");
	// we need to process the step
	// step id is $valid
	/**
	 * $result is returned by process_step() to show us what occurred.
	 * 
	 * $result = Array
	 * (
	 * 	[0] => Step status (0 : all done correctly, 1 : error occurred)
	 * 	[1] => error number (0 : all done correctly, > 0 : error number)
	 * 	[2] => error description & additionnal messages.
	 * )
	 */
	$result = process_step();
	if($result[0] == 0) {
		$step = '
<div id="welcome">
	<h2>' . _("Welcome in MEIK installation wizard.") . '</h2>
</div>
' . makemenu($array) . '
<div id="content" class="contenu">
	<h3>' . $steptitle . '</h3>
	<form action="?step=' . ($valid+1). '" method="post">
	<p>
		<div class="msg-info">
			' . $stepfinished . '
		</div>
		<br />
		<input type="submit" value="' . _("Continue") . '" />
	</p>
	</form>
</div>
';
	} else {
		$step = '
<div id="welcome">
	<h2>' . _("Welcome in MEIK installation wizard.") . '</h2>
</div>
' . makemenu($array) . '
<div id="content" class="contenu">
	<h3>' . $steptitle . '</h3>
	<form action="?validstep=' . ($valid). '" method="post">
	<p>
		<div class="msg-error">
			' . _("An error occurred. Error") . ' #' . $result[1] . ' : ' . $result[2] . '
		</div>
		' . $steploaded .'
		<br />
		<input type="submit" value="' . _("Retry") . '" />
	</p>
	</form>
</div>
';
	}	
}

echo htmlrender($step);

?>
