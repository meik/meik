<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

$steptitle    = _("Database Creation");
$stepfinished = _("Database creation has been successfully done.");
$steploaded   = '
	<table>
		<tbody>
			<tr>
				<td class="align-right">' . _("Server Hostname"). ' :</td>
				<td><input type="text" name="serv_hostname" value="localhost" /></td>
			</tr>
			<tr>
				<td class="align-right">' . _("Database name"). ' :</td>
				<td><input type="text" name="db_name" value="meik" /></td>
			</tr>
			<tr>
				<td class="align-right">' . _("Check if you have already created a database and user for MEIK"). ' :</td>
				<td><input type="checkbox" name="ready" /></td>
			</tr>
			<tr class="grey">
				<td class="align-right">' . _("Database administrator"). ' :</td>
				<td><input type="text" name="db_admin_name" value="root" /> (' . _("Only if you haven't checked the checkbox above"). ')</td>
			</tr>
			<tr class="grey">
				<td class="align-right">' . _("Database administrator password"). ' :</td>
				<td><input type="password" name="db_admin_pass" value="" /> (' . _("Only if you haven't checked the checkbox above"). ')</td>
			</tr>
			<tr>
				<td class="align-right">' . _("Database user"). ' :</td>
				<td><input type="text" name="db_user_name" value="meik" /></td>
			</tr>
			<tr>
				<td class="align-right">' . _("Database password"). ' :</td>
				<td><input type="password" name="db_user_pass" value="" /></td>
			</tr>
		</tbody>
	</table>
';

function process_step()
{
	$server        = getvar("serv_hostname");
	$ready         = getvar("ready");
	$db_name       = getvar("db_name");
	$db_admin_name = getvar("db_admin_name");
	$db_admin_pass = getvar("db_admin_pass");
	$db_user_name  = getvar("db_user_name");
	$db_user_pass  = getvar("db_user_pass");
	
	if(!$ready) {
		$user = $db_admin_name;
		$pass = $db_admin_pass;
	} else {
		$user = $db_user_name;
		$pass = $db_user_pass;
	}
	
	if(!@mysql_connect($server, $user, $pass)) {
		return array("1", "1", _("Cannot connect to MySQL database with user") . ' ' . $user);
	} else {
		if(!$ready) {
			$ajout1 = "GRANT USAGE ON * . * TO '$db_user_name'@'localhost' IDENTIFIED BY '$db_user_pass' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 ;";
			$ajout2 = "CREATE DATABASE `$db_name` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;";
			$ajout3 = "GRANT ALL PRIVILEGES ON `$db_name` . * TO '$db_user_name'@'localhost' WITH GRANT OPTION ;";
			$ajout4 = "FLUSH PRIVILEGES;";
			
			if(!@mysql_query($ajout1))
				return array("1", "2", _("Cannot grant user") . ' ' . $db_user_name);
				
			if(!@mysql_query($ajout2))
				return array("1", "3", _("Cannot create database") . ' ' . $db_name);
				
			if(!@mysql_query($ajout3))
				return array("1", "4", _("Cannot grant privileges for database") . ' ' . $db_name . ' ' . _("to user") . ' ' . $db_user_name);
				
			if(!@mysql_query($ajout4))
				return array("1", "5", _("Cannot flush privileges"));
		} else {
			
		}
		@mysql_select_db($db_name);
		
		$ajout_exts = "
CREATE TABLE IF NOT EXISTS `meik_exts` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `meik_notifs_ext_id` int(10) unsigned NOT NULL default '0',
  `meik_profiles_data_id` int(10) unsigned NOT NULL default '0',
  `meik_exts_vers_id` int(10) unsigned NOT NULL default '0',
  `meik_target_id` int(10) unsigned NOT NULL default '0',
  `uuid` varchar(36) character set latin1 default NULL,
  `timestamp` int(10) unsigned default NULL,
  `xpi` varchar(255) character set latin1 default NULL,
  `name` varchar(255) character set latin1 default NULL,
  `version` varchar(16) character set latin1 default NULL,
  `description` tinytext,
  `notif` tinyint(1) default NULL,
  `updateurl` varchar(255) character set latin1 default NULL,
  PRIMARY KEY  (`id`),
  KEY `meik_exts_FKIndex1` (`meik_target_id`),
  KEY `meik_exts_FKIndex2` (`meik_exts_vers_id`),
  KEY `meik_exts_FKIndex3` (`meik_profiles_data_id`),
  KEY `meik_exts_FKIndex4` (`meik_notifs_ext_id`)
);";

		$ajout_exts_vers = "
CREATE TABLE IF NOT EXISTS `meik_exts_vers` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `uuid_ext` varchar(36) character set latin1 default NULL,
  `uuid_appli` varchar(36) character set latin1 default NULL,
  `minver` varchar(16) character set latin1 default NULL,
  `maxver` varchar(16) character set latin1 default NULL,
  PRIMARY KEY  (`id`)
);";

		$ajout_notifs = "
CREATE TABLE IF NOT EXISTS `meik_notifs` (
  `ext_id` int(10) unsigned NOT NULL auto_increment,
  `url` varchar(255) character set latin1 default NULL,
  PRIMARY KEY  (`ext_id`)
);";

		$ajout_profiles = "
CREATE TABLE IF NOT EXISTS `meik_profiles` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `meik_users_id` int(10) unsigned NOT NULL default '0',
  `meik_users_meik_profiles_data_id` int(10) unsigned NOT NULL default '0',
  `meik_exts_meik_notifs_ext_id` int(10) unsigned NOT NULL default '0',
  `meik_profiles_data_id` int(10) unsigned NOT NULL default '0',
  `meik_exts_id` int(10) unsigned NOT NULL default '0',
  `user_id` int(10) unsigned default NULL,
  `name` varchar(255) character set latin1 default NULL,
  PRIMARY KEY  (`id`,`meik_users_id`),
  KEY `meik_profiles_FKIndex1` (`meik_users_id`),
  KEY `meik_profiles_FKIndex3` (`meik_users_id`,`meik_users_meik_profiles_data_id`),
  KEY `meik_profiles_FKIndex4` (`meik_profiles_data_id`),
  KEY `meik_profiles_FKIndex2` (`meik_exts_id`,`meik_exts_meik_notifs_ext_id`)
);";

		$ajout_profiles_data = "
CREATE TABLE IF NOT EXISTS `meik_profiles_data` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `meik_users_id` int(10) unsigned default NULL,
  `meik_profile_id` int(10) unsigned default NULL,
  `meik_ext_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
);";

		$ajout_submit = "
CREATE TABLE IF NOT EXISTS `meik_submit` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `url` varchar(255) character set latin1 default NULL,
  PRIMARY KEY  (`id`)
);";

		$ajout_target = "
CREATE TABLE IF NOT EXISTS `meik_target` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `meik_exts_vers_id` int(10) unsigned NOT NULL default '0',
  `uuid` varchar(36) character set latin1 default NULL,
  `nom` varchar(255) character set latin1 default NULL,
  PRIMARY KEY  (`id`),
  KEY `meik_target_FKIndex1` (`meik_exts_vers_id`)
);";

		$ajout_users = "
CREATE TABLE IF NOT EXISTS `meik_users` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `meik_profiles_data_id` int(10) unsigned NOT NULL default '0',
  `login` varchar(255) character set latin1 default NULL,
  `pass` varchar(32) character set latin1 default NULL,
  `nom` varchar(255) character set latin1 default NULL,
  `email` varchar(255) character set latin1 default NULL,
  `statut` int(11) default NULL,
  `notifs` tinyint(1) default NULL,
  `nnative` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `meik_users_FKIndex1` (`meik_profiles_data_id`)
);";
		
		$pop_target = "
INSERT INTO `meik_target` (`id`, `meik_exts_vers_id`, `uuid`, `nom`) VALUES
(1, 0, '86c18b42-e466-45a9-ae7a-9b95ba6f5640', 'Mozilla Suite'),
(2, 0, '3550f703-e582-4d05-9a08-453d09bdfdc6', 'Mozilla Thunderbird'),
(3, 0, '3db10fab-e461-4c80-8b97-957ad5f8ea47', 'Netscape Browser'),
(4, 0, 'ec8030f7-c20a-464f-9b0e-13a3a9e97384', 'Mozilla Firefox'),
(15, 0, '136c295a-4a5a-41cf-bf24-5cee526720d5', 'Nvu'),
(16, 0, '92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a', 'SeaMonkey'),
(17, 0, '718e30fb-e89b-41dd-9da7-e25a45638b28', 'Sunbird');
";

		if(!@mysql_query($ajout_exts))
			return array("1", "6", _("Cannot add table meik_exts"));
			
		if(!@mysql_query($ajout_exts_vers))
			return array("1", "7", _("Cannot add table meik_exts_vers"));
		
		if(!@mysql_query($ajout_notifs))
			return array("1", "8", _("Cannot add table meik_notifs"));
			
		if(!@mysql_query($ajout_profiles))
			return array("1", "9", _("Cannot add table meik_profiles"));
		
		if(!@mysql_query($ajout_profiles_data))
			return array("1", "10", _("Cannot add table meik_profiles_data"));
			
		if(!@mysql_query($ajout_submit))
			return array("1", "11", _("Cannot add table meik_submit"));
		
		if(!@mysql_query($ajout_target))
			return array("1", "12", _("Cannot add table meik_target"));
			
		if(!@mysql_query($ajout_users))
			return array("1", "13", _("Cannot add table meik_users"));
		
		if(!@mysql_query($pop_target))
			return array("1", "14", _("Cannot populate table meik_target"));
		
		/**
		 * Store MySQL authentication data for later.
		 */
		$_SESSION["db"] = array($server, $db_name, $db_user_name, $db_user_pass);
		
		return array("0");
	}
}

?>
