<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

$db = $_SESSION["db"];
$steptitle    = _("Database Configuration");
$stepfinished = _("Database configuration has been successfully written.");

$steploaded   = '
	<p>
	' . _("This is the information needed to connect to MySQL database. You just need to validate this.") . '
	</p>
	<table>
		<tbody>
			<tr>
				<td class="align-right">' . _("Server Hostname"). ' :</td>
				<td><input type="text" name="hostname" value="' . $db[0] . '" /></td>
			</tr>
			<tr>
				<td class="align-right">' . _("Database name"). ' :</td>
				<td><input type="text" name="name" value="' . $db[1] . '" /></td>
			</tr>
			<tr>
				<td class="align-right">' . _("Database user"). ' :</td>
				<td><input type="text" name="user" value="' . $db[2] . '" /></td>
			</tr>
			<tr>
				<td class="align-right">' . _("Database password"). ' :</td>
				<td><input type="password" name="pass" value="' . $db[3] . '" /></td>
			</tr>
		</tbody>
	</table>
';


function process_step()
{
	$hostname = getvar("hostname");
	$name     = getvar("name");
	$user     = getvar("user");
	$pass     = getvar("pass");
	
	if(!@mysql_connect($hostname, $user, $pass)) {
		return array("1", "1", _("Cannot connect to MySQL server") . " $user:$pass@$hostname " . _("MySQL Error") . ' : ' . mysql_error());
	}
	
	if(!@mysql_select_db($name)) {
		return array("1", "2", _("Cannot select MySQL database") . " $user:$pass@$hostname " . _("MySQL Error") . ' : ' . mysql_error());
	}
	
	$data = '<?php
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
$rsql->host = "'.$hostname.'";
$rsql->user = "'.$user.'";
$rsql->pass = "'.$pass.'";
$rsql->db   = "'.$name.'";
$rsql->connect();
?>';
	
	if($handle = @fopen(dirname(dirname(__FILE__)) . ( (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/' ) . "config.inc.php", "w")) {
		if(false ===  @fwrite($handle, $data)) {
			return array("1", "4", _("Cannot write into file") . ' config.inc.php');
		}
	} else {
		return array("1", "3", _("Cannot open file") . ' config.inc.php ' . _("for writing."));
	}
	
	return array("0");
}
?>
