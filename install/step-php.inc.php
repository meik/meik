<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

$steptitle    = _("PHP Checks");
$stepfinished = _("PHP Checks were successfully run.");

$good = 0;
$extensions = array("session", "mysql", "zip");
$loaded = get_loaded_extensions();

$code = null;
foreach($extensions as $ext)
{
	if(in_array($ext, $loaded)) {
		$code .= '
			<tr>
				<td>' . $ext . '</td>
				<td><input type="checkbox" checked="checked" /></td>
			</tr>
';
		$good++;
	} else {
		$code .= '
			<tr>
				<td>'  . $ext . '</td>
				<td><input type="checkbox" /></td>
			</tr>
';
	}
}

$steploaded   = '
	<table class="nice">
		<caption>' . _("We are now checking for presence of needed PHP extensions."). '</caption>
		<thead>
			<tr>
				<th>' . _("PHP Extension"). '</th>
				<th>' . _("Present ? ") . '</th>
			</tr>
		</thead>
		<tbody>
			' . $code . '
		</tbody>
	</table>
	<input type="hidden" name="good" value="' . $good . '" />
';

function process_step()
{
	$good = getvar("good");
	
	if($good < 3) {
		return array("1", "1", _("One or more extension is lacking. Please check you have at least Session, MySQL and Zip installed.") );
	} else if($good == 3) {
		return array("0");
	}
}

?>
