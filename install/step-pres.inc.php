<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

$code = htmlentities(file_get_contents("../LICENSE"));
$images = htmlentities(file_get_contents("../images/LICENSE"));

$steptitle  = _("Presentation");
$steploaded = 
	_("Welcome to MEIK !")
	. '<br /><br />
	</p>
	<div class="error">'
	. _("This installation wizard will help you to install and configure the basics for MEIK to work.")
	. '<br />'
	. _("Before anything, thus, you must accept the following license. If you don't, just remove all files,")
	. '<br />'
	. _("close your browser, and have a beer. You have been warned. Animals maybe injuried.")
	. '
	</div>
	<p><br />'
	. '<textarea rows="80" cols="80">

	' . _("About PHP and CSS Code") .' :
	----------------------

' . $code . '

	' . _("About Images") . ' :
	--------------

' . $images . '
	</textarea>';

$stepfinished = _("You accepted the license.");

function process_step()
{
	return array ("0");
}

?>
