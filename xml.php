<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

/**
 * We want to XML data to the script to get descriptions
 * 
 * Errors :
 * --------
 *   * 1 : Execution successfull, no query was done.
 *   * 2 : ---------------------, query response in tag <response></response>
 *   * 3 : Execution unsuccessfull, error description within <err></err>
 * 
 * Array
 * (
 *     [id] => 5
 *     [meik_profiles_data_id] => 0
 *     [meik_exts_vers_id] => 0
 *     [meik_target_id] => 0
 *     [uuid] => 3b56bcc7-54e5-44a2-9b44-66c3ef58c13e
 *     [timestamp] => 1123602244
 *     [xpi] => html_validator_based_on_tidy_-0.6.2-fx+mz-windows.xpi
 *     [name] => Html Validator (based on Tidy)
 *     [version] => 0.6.2
 *     [description] => Adds HTML validation to the View Page Source of the browser. The validation is done by Tidy from W3c.
 * )
 * 
 * Let's have a look at the XML response :
 * ---------------------------------------
 * 
 * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
 *	<error>2</error>
 *	<response>
 *		<name>Html Validator (based on Tidy)</name>
 *		<version>0.6.2</version>
 *		<desc>Adds HTML validation to the View Page Source of the browser. The validation is done by Tidy from W3c.</desc>
 *	</response>
 **/

include_once("libmeik.inc.php");

$meik = new Meik();
$xml = new XML();
$xml->start();

$d = getvar("do");

switch($d) {
	case "getdescr":
		$id = getvar("id");
		$infos = $meik->get_ext_infos($id);
		
		if(is_array($infos)) {
			$xml->addtag("error", "2");
			$xml->addtag("name", $infos["name"]);
			$xml->addtag("version", $infos["version"]);
			$xml->addtag("desc", $infos["description"]);
			$xml->serve();
		} else {
			$xml->addtag("error", "3");
			$xml->addtag("err", _("No extension found with this id."));
			$xml->serve();
		}
		
		break;
	
	default:
		$xml->addtag("error", "1");
		$xml->serve();
		break;
}

class XML {
	public $xml = null;
	
	function start()
	{
		$this->xml = '<?xml version="1.0" encoding="'.$GLOBALS["language"]->charsetselect().'" standalone="yes"?>
';
		$this->starttag("response");
	}
	
	function stop()
	{
		$this->stoptag("response");
	}
	
	function add($content)
	{
		$this->xml .= $content;
	}
	
 	function addtag($tag, $content)
	{
		$this->xml .= '<'.$tag.'>'.$content.'</'.$tag.'>';
	}

	function starttag($tag)
	{
		$this->xml .= '<'.$tag.'>';
	}
	
	function stoptag($tag)
	{
		$this->xml .= '</'.$tag.'>';
	}
	
	function serve()
	{
		$this->stop();
		header ("Content-Type: text/xml");
		echo $this->xml;
	}
}
?>
