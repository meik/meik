<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

class Meik_Auth
{
	public $auth			= "mysql";
	public $auth_plug_name	= "MySQL_Authentication";
	
	private $auth_root	= null; ///< This is the path where all the authentications plugins lives. No trailing slash !
	private $sql		= null;
	private $meik		= null;
	
	/**
	 * We starts the backend. It's simply an object, with one authuser() method.
	 * The object lives in a file, named : auth-$(backendname).inc.php
	 * 
	 * So, for default MySQL backend, we have auth-mysql.inc.php
	 * And, if we want to add a "msad" backend, just do this within auth-msad.inc.php
	 * 
	 * Check the auth-mysql.inc.php to see how should the object looks like.
	 * 
	 * activate_backend() returns a backend object!
	 */
	
	function __construct()
	{
		$this->sql			= $GLOBALS["rsql"];
		$this->meik			= $GLOBALS["meik"];
		$this->auth_root	= 'auth';
	}
	
	/**
	 * This function return the content of $this->auth_root.
	 * 
	 * @return $this->auth_root content.
	 */
	public function get_auth_root()
	{
		return $this->auth_root;		
	}
	
	/**
	 * This function list all the availables authentication backend.
	 * 
	 * @return An array containing all filename corresponding.
	 */
	public function get_all_auth_backend()
	{
		/**
		 * First of all, we list all auth-*.inc.php files.
		 * Then, load it, and we'll read file to find line "Class <Name> Extends Meik_Auth"
		 * to deduce class of each auth-*.inc.php.
		 */
		
		$result			= array();
		
		$d = dir(dirname(__FILE__) . '/' . $this->auth_root . '/');
		while ( false !== ($entry = $d->read()) ) {
			if($entry != '.' && $entry != '..') {
		   		if(preg_match("/auth-\w+\.inc\.php/i", $entry, $matches)) {
		   			$result[] = $entry;
		   		}
			}
		}
		
		$d->close();
		return $result;
	}
	
	public function extract_auth_backends_infos()
	{
		$classes = array();
		foreach($this->get_all_auth_backend() as $backend) {
			$auth_plugin = file_get_contents(dirname(__FILE__) . '/' . $this->auth_root . '/' . $backend);
			if(preg_match("/class (\w+) extends Meik_Auth/im", $auth_plugin, $resultat)
			&& is_array($resultat)
			&& !empty($resultat[1])) {
				if(in_array($resultat[1], get_declared_classes())) {
					continue; ///< We currently see the active backend, so skip it. 
				} else {
					include_once(dirname(__FILE__) . '/' . $this->auth_root . '/' . $backend);
					$class = new $resultat[1]();
				}
				
				if(is_object($class)) {
					$classes[] = $class;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
		
		return $classes;
	}
	
	/**
	 * This functions construct the relative path of the auth plugin.
	 * 
	 * @return The path formed by $this->auth_root and $this->auth
	 */
	private function auth_plugin()
	{
		return dirname(__FILE__) . '/' . $this->auth_root . '/auth-' . $this->auth . '.inc.php';
	}
	
	/**
	 * This function test for file presence, correct loading and all of the backend.
	 * 
	 * @param &$backend Where to stock backend if successfull ?
	 * 
	 * @return true, if all goes well, or false.
	 */
	public function check_backend(&$backend)
	{
		$plugin = $this->auth_plugin();
				
		if(!empty($this->auth) && is_file($plugin)) {
			include_once($plugin);
		} else {
			error(_("No authentication plugin set. Aborting. Was : " . $plugin));
			return false;
		}
		debug("Trying to load " . $this->auth . " authentication backend.");
		
		// $auth_plugin_disc_hooks is defined in each authentication plugin.
		if(class_exists($this->auth_plug_name)) {
			$backend = new $this->auth_plug_name();
		} else {
			error(_("No class found. Wanted : ") . $this->auth_plug_name);
			return false;
		}
		
		if(!is_object($backend) || !method_exists($backend, "authuser")) {
			error(_("Sorry, it looks like your backend object isn't correct."));
			return false;
		}
		
		debug("Backend " . $this->auth . " successfully loaded.");
		return true;
	}
	
	public function activate_backend()
	{
		$backend = null;
		if($this->check_backend($backend)) {
			return $backend;
		} else {
			return false;
		}
	}
	
	/**
	 * We test if account is aleady in database.
	 * 
	 * @param $user The username ...
	 * 
	 * @return The user id. 
	 */
	public function mysql_account_check($user)
	{
		$check = "SELECT `id` FROM `meik_users` WHERE `login` = '$user' LIMIT 1;";
		$result = $this->sql->query($check);
		return ((count($result) == 1) ? $result[0]["id"] : false );
	}
	
	/**
	 * Check if the account is MySQL need to be updated.
	 * 
	 * @param $user The username ...
	 * 
	 */
	public function mysql_need_update($id, $email, $nom, $user)
	{		
		$check = "SELECT `id`, `email`, `nom` FROM `meik_users` WHERE `id` = '$id' LIMIT 1;";
		$result = $this->sql->query($check);
		
		if($result[0]["email"] != $email)
			$this->mysql_update("email", $email);
			
		if($result[0]["nom"] != $nom)
			$this->mysql_update("nom", $nom);
	}
	
	/**
	 * We need to add a user !
	 * 
	 * @param $user The username ...
	 * 
	 * @return it's ID in MySQL.
	 */
	public function mysql_add($mail, $nom, $user)
	{
		// 	function adduser2sql($login, $pass, $nom, $email, $sendmail = true)
		if($this->meik->adduser2sql($user, "LDAP_PASSWORD", $nom, $mail, false, true)) {
			return $this->mysql_account_check(); // we return the id ...
		} else {
			return false;
		}
	}
	
	/**
	 * We want to update some informations.
	 * 
	 * @param $user The username ...
	 * 
	 */
	public function mysql_update($champ, $valeur, $user)
	{
		$req = "UPDATE `meik_users` SET `$champ` = '$valeur' WHERE `login` = '$user';";
		$this->sql->query($req);
	}
	
	/**
	 * This function returns pretty XHTML code for displaying all informations about a backend.
	 * 
	 * @param $backend_object The backend's object to display data !
	 * @param $extra Some extra data to put into <tfoot></tfoot>.
	 * 
	 * @return pretty nice XHTML code, or false if any error.
	 */
	public function format_backend_infos($backend_object, $extra = null)
	{
		if(is_subclass_of($backend_object, "Meik_Auth")) {
			$code = null;
		 	foreach($backend_object->author as $name => $mail) {
		 		$code .= '
								<tr>
									<td>' . $name . '</td>
									<td>' . $mail . '</td>
								</tr>';
		 	}
		 	
			$result = '<table class="auth active">
			<thead>
				<tr>
					<th colspan="2">' . _("backend") . " : " . $backend_object->name . '</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>' . _("Name") . '</td>
					<td>' . $backend_object->name . '</td>
				</tr>
				<tr>
					<td>' . _("Description") . '</td>
					<td>' . $backend_object->desc . '</td>
				</tr>
				<tr>
					<td>' . _("Version") . '</td>
					<td>' . $backend_object->ver . "-". $backend_object->rev . '</td>
				</tr>
				<tr>
					<td>' . _("Author(s)") . '</td>
					<td>
						<table class="author">
							<thead>
								<tr>
									<th>' . _("Name") . '</th>
									<th>' . _("Email") . '</th>
								</tr>
							</thead>
							<tbody>' . $code. '
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2">' . $extra . '</td>
				</tr>
			</tfoot>
		</table><br />
	';
			return $result;
		} else {
			return false;
		}
	}
}
?>
