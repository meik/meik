<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

$todo     = getvar("todo");
$userid   = $session->getsessvar("id");
$username = $session->getsessvar("user");
$rank     = $meik->getuserrank($userid);

$messages = $session->showmessages();

echo    '
<div id="welcome">
	<h2>'._("Welcome").', ' . $session->showusername($username) . $messages . '</h2>
</div>
' . 
	$session->showadmin() .
	$session->showuser().
'
	<div id="content" class="contenu">
<div class="error">' . $rsql->error . '</div>';

switch($todo) {
	case "modifp":
		echo '
<h3>'._("Profiles management").'</h3>
';
		$id    = getvar("profil");
		$modif = getvar("modif");
		$del   = getvar("delete");
		$inst  = getvar("inst");
		
		debug("id == $id");
		debug("modif == $modif");
		debug("delete == $del");
		debug("inst == $inst");
		
		$exts = $meik->get_all_ext();
		$selected = $meik->get_profile_exts($id);
		$nom = $meik->getprofilname($id);
				
		$dispo = null;
		$choisies = null;
		
		if(is_array($exts)) {
			foreach($exts as $member) {
				$dispo .= '					<option value="'.$member["id"].'">'.$member["name"].' ('.$member["version"].')</option>
';
			}
		}
		
		if(is_array($selected)) {
			foreach($selected as $member) {
				$choisies .= '					<option value="'.$member.'">'.$meik->getextname($member).' ('.$meik->getextver($member).')</option>
';
			}
		}
				
		if($modif) {
			debug("Trying to modify profil $id");
			echo '
<form action="?todo=makemodifs" method="post" onsubmit="javascript:recap();">
<p id="modif-profil">
	'._("Profile name").' : <input type="text" name="nomprofil" value="'.$nom.'" /><br />
	'._("Extensions").' :
	<object>
	<table id="modif-prof">
		<thead>
			<tr id="ligne-entete">
				<th>
					<input type="button" value="-" title="' . _("Remove some lines"). '" onclick="remline_dispo();"/>
				</th>
				<th>
					'._("Available").' <span id="count_dispo">(' . count($exts) .')</span>
				</th>
				<th></th>
				<th>
					'._("Choosen").' <span id="count_choisies">(' . count($selected) .')</span>
				</th>
				<th>
					<input type="button" value="-" title="' . _("Remove some lines"). '" onclick="remline_choisies();"/>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr id="ligne-select">
				<td></td>
				<td id="select-dispo">
					<select id="dispo" name="dispo" size="15" multiple="multiple" ondblclick="addext(\'dispo\', \'choisies\');" onchange="makeXML(\'dispo\');" >
	'.$dispo.'
					</select>
				</td>
				<td id="add-remove">
					<input type="button" value="&gt;&gt; '._("Add").' &gt;&gt;" onclick="addext(\'dispo\', \'choisies\');" /><br />
					<input type="button" value="&lt;&lt; '._("Remove").' &lt;&lt;" onclick="rmext(\'choisies\', \'dispo\');" />
				</td>
				<td id="select-choisies">
					<select id="choisies" name="selected" size="15" multiple="multiple" ondblclick="rmext(\'choisies\', \'dispo\');"  onchange="makeXML(\'choisies\');">
	'.$choisies.'
					</select>
				</td>
				<td></td>
			</tr>
			<tr id="ligne-button">
				<td>
					<input type="button" value="+" title="' . _("Add some lines"). '" onclick="addline_dispo();"/>
				</td>
				<td colspan="3"><input type="submit" name="valid" value="'._("Save my profile").'" /></td>
				<td>
					<input type="button" value="+" title="' . _("Add some lines"). '" onclick="addline_choisies();"/>
				</td>
			</tr>
		</tbody>
	</table>
	</object>
	<input type="hidden" id="exts" name="extensions" />
	<input type="hidden" name="idprofil" value="'.$id.'" />
</p>
</form>
<div id="show-desc">
	<!-- <input type="button" name="view" value="'._("Show selected extension's description").'" onclick="javascript:makeXML(\'dispo\');" /> -->
' . $meik->showdesc() . '
</div>
';
		} elseif($del) {
			debug("Going to delete profil $id");
			$del = $meik->delprofil($id);
			// print_r($del);
			if($del) {
				echo _("Profile has been deleted.");
			} else {
				echo _("Cannot delete profile. Profile's ID is : "). $id;
			}
		} elseif($inst) {
			$selected = $meik->get_profile_exts($id);

			$choisies = NULL;
			
			echo '
	<p class="error">
		'._("Warning, in order to work properly, MEIK need JavaScript active. If you can't install any extension, please check JavaScript is active. You also might need to authorize the website to install extensions, this is normal.").'
	</p>
			<div class="info-install">
				'._("Profile name").' : <span id="prof-name">'.$nom.'</span><br />
				'._("Extensions to be installed").' : <br />
			</div>';
			
			$infos = array();
			
			foreach($selected as $member) {
				/*$choisies .= '
				<li>'.$meik->getextname($member["id"]).'</li>
				';*/
				$infos[] = $meik->get_ext_infos($member);
			}
			
			echo '
<form id="xpis" action="javascript:;">
	'.$meik->createHTML($infos).'
<p id="valid-install-exts">
	<input type="button" value="'._("Install checked extensions").'" onclick="javascript:XPIinstall();" />
</p>
</form>';
		}
		
		break;
		
	case "makemodifs":
		echo '
<h3>'._("Profile update").'</h3>
';
		$newnom = getvar("nomprofil");
		$id     = getvar("idprofil");
		$exts   = explode(";", getvar("extensions"));
		array_pop($exts);

		if($meik->updateprofil($id, $newnom)) {
			echo _("Profile successfully updated.")."<br />";
			if($meik->updateprofildata($id, $exts)) {
				echo _("Extensions were successfully added to the profile.")."<br />";
			} else {
				echo _("One or more extension wasn't successfully added to the profile.")."<br />";
			}
		} else {
			echo _("Cannot update profile.")."<br />";
		}

		break;
		
	case "addp":
		echo '
<h3>'._("Add a profile").'</h3>
';
		echo '
<form action="?todo=regp" method="post">
	<p id="add-profil">
		'._("Profile name").' : <input type="text" name="nomprofil" />
		<input type="submit" value="'._("Create").'" />
	</p>
</form>
		';
		break;
		
	case "regp":
		echo '
<h3>'._("Add a profile").'</h3>
';
		$nomp = getvar("nomprofil");
		if($meik->addprofil($nomp, $userid)) {
			echo '<p class="info-add-profil">'._("Profile has been successfully created.").'</p>';
		} else {
			echo '<p class="error">'._("Cannot create profile.").'</p>';
		}
		break;
	
	case "compte":
		echo '
<h3>'._("Account management").'</h3>
';
		$mcompte = getvar("mcompte");
		$classes = array
			(
			"username" => "inactive",
			"password" => "inactive",
			"password-confirm" => "inactive",
			"realname" => "inactive",
			"email" => "inactive",
			"notifs" => "inactive"
			);
		
		if($mcompte == "true") {
			$user   = getvar("user");
			$pass1  = getvar("pass1");
			$pass2  = getvar("pass2");
			$nom    = getvar("nom");
			$email  = getvar("email");
			$notifs = (getvar("notifs") == "on" ? 1 : 0);
					
			if(empty($pass1) && empty($pass2)) {
				debug("Empty passwords !");
				echo _("Cannot continue, because your password is empty.");
				$classes["password"] = "active";
				$classes["password-confirm"] = "active";
 			} else {
				if($pass1 == $pass2) {
					debug("Password are identical.");
					include_once("mail.inc.php");
					$check = new EmailCheck();
					$check->email = $email;
					if($check->checkemail()) {
						debug("email is good, updating ... ");
						$update = "UPDATE `meik_users` SET" .
								" `login` = '$user'," .
								" `pass` = MD5('$pass1')," .
								" `nom` = '$nom'," .
								" `email` = '$email', " .
								" `notifs` = '$notifs'".
								" WHERE `id` = '".$session->getsessvar("id")."' LIMIT 1;";
						$res = $meik->query($update);
						if($res) {
							debug(" ... All is up-to-date. Good. ");
							info(_("Update successfull."));
							break;
						} else {
							debug(" ... There was an error ! ");
							error(_("An error occured while updating account data."));
						}
					} else {
						debug("email address cannot be validated.");
						error(_("It looks like your email is a fake, or the server didn't respond, or something else. Your email has to be valid."));
						$classes["email"] = "active";
					}
				} else {
					debug("Password are NOT identical. Stopping.");
					error(_("Password are not strictly identical. You might have done some typo, or something else. Please back in your browser and change what's needed."));
					$classes["password-confirm"] = "active";
				}
			}
		}

		$infos = $meik->get_user_infos($session->getsessvar("id"));
		
		echo '
<form action="?todo=compte" method="post">
<table id="form-mcompte">
	<tr id="username" class="' . $classes["username"] . '">	
		<td>'._("Username").'</td>
		<td><input type="text" name="user" value="'.$infos["login"].'" /></td>
		<td>'._("This is the name you enter to connect. It's also known as login.").'</td>
	</tr>
	<tr id="password" class="' . $classes["password"] . '">
		<td>'._("Password").'</td>
		<td><input type="password" name="pass1" /></td>
		<td>'._("Type your actual or new password here.").'</td>
	</tr>
	<tr id="password-confirm" class="' . $classes["password-confirm"] . '">
		<td>'._("Password (again)").'</td>
		<td><input type="password" name="pass2" /></td>
		<td>'._("Please enter again the same password as above.").'</td>
	</tr>
	<tr id="realname" class="' . $classes["realname"] . '">
		<td>'._("Real Name").'</td>
		<td><input type="text" name="nom" value="'.$infos["nom"].'" /></td>
		<td>'._("This is the name MEIK will use to welcome you. (optionnal)").'</td>
	</tr>
	<tr id="email" class="' . $classes["email"] . '">
		<td>'._("email address").'</td>
		<td><input type="text" name="email" value="'.$infos["email"].'" /></td>
		<td>'._("Your email has to be up-to-date, otherwise you won't be able to use the whole MEIK's features").'</td>
	</tr>
	<tr id="notifs" class="' . $classes["notifs"] . '">
		<td>'._("Notifications").'</td>
		<td><input type="checkbox" name="notifs" '.($infos["notifs"] ? 'checked="checked"' : '').' /></td>
		<td>'._("If it's checked, then you'll receive an email to the above address, each time one of your extension is updated").'</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" name="mcompte" value="'._("Update my account informations").'" />
		</td>
	</tr>
</table>
<input type="hidden" name="mcompte" value="true" />
</form>';

		break;
		
	case "upform":
		echo '
<h3>'._("Submit one or more extension").'</h3>
';
		$submit = getvar("action");
		
		if($submit) {
			$urls = getvar("urls");
			
			if(!empty($urls)) {
				$urls = explode("\n", $urls);
				foreach($urls as $url) {
					if(!$meik->submit($url)) {
						echo _("An error occurred while submitting").' : "'.$url.'".'.'<br />';
					}
					$files[] = basename($url);
				}
			} else {
				echo '<p class="msg-error">' . 
				_("Skipping extension").' '.$id.' '._("because of empty URL.")
				. '</p>';
			}
			
			if($files) {
				$send = $meik->notice_admin($files, $username);
				if($send) {
					echo '<br />'._("Thanks, your submission has been sent to administrators, and will be processed as soon as posisble.");
				} else {
					echo '<br />'._("Oops, there was a problem trying to send informations to administrators. Please retry later.");
				}
			} else {
				echo '<br />' . _("No file was found.");
			}
			
		} else {
			echo '
<form action="?todo=upform" method="post">
	<p id="form-add" class="form-add">
		'._("Thanks for your help with providing new extension. Submiting them is really easy : you just have to paste the URL (the internet address) of one (or more) XPI package (file which ends by '.xpi') such as those linked on").' <a href="https://addons.mozilla.org">UMO</a> '._("in this area, taking care to put").' <strong>'._("one URL on each line").'</strong>. '._("Then, just press").' '._("Add").' '._("to validate. Site administrator will be advised of your request, and it should processed as faster as possible.").'<br /><br />'._("You can submit as many extension as you want !").'<br />
		<textarea name="urls" rows="20" cols="80"></textarea><br /><br />
		<input type="submit" name="exec" value="'._("Add").'" />
		<input type="hidden" name="action" value="true" />
	</p>
</form>
';
		}
		
		break;
		
	case "notif":
		echo '
<h3>'._("Notify an extension's update").'</h3>
';
		$submit = getvar("action");

		if($submit == "validate") {
			$exts = getvar("ext");
			$urls = null;
			foreach($exts as $id => $file) {
				if(!empty($file)) {
					if(!$meik->isnotified($id, $file)) {
						echo _("It looks like there's already an update notification for this extension.").'<br />';
					} else {
						$urls[$id] = $file;
					}
				} else {
					echo '<p class="msg-error">' . 
					_("Skipping extension").' '.$id.' '._("because of empty URL.")
					. '</p>';
				}
			}
			
			if(is_array($urls)) {
				foreach($urls as $id => $name) {
					$listes[] = $meik->getextname($id);
				}
				$send = $meik->notice_admin($listes, $username, "not");
				if($send) {
					echo '<br />'._("Thanks, your notification update has been sent to administrators, and will be processed as soon as possible.");
				} else {
					echo '<br />'._("Oops, there was a problem trying to send informations to administrators. Please retry later.");
				}
				
			} else {
				echo '<br />'._("An error occurred while notifying new packages. Please check above.");
			}
			
		} else if($submit == "proceed") {
			$exts = explode(";", getvar("extensions"));
			array_pop($exts);
			
			echo '
<div id="notice-notify">
	'._("Here is the list of all the extensions you want to notify an update. So, now, just paste (Right click, paste or CTRL+V), in the text box under each extension, the URL address of the updated XPI package.").'
</div>
';
			
			$html = null;
			foreach($exts as $member) {
				$name = $meik->getextname($member);
				$vers = $meik->getextver($member);
				$html .= '
			<li>
				'.$name.' ('.$vers.')<br />
				<input class="notify-url" type="text" name="ext['.$member.']" size="72" />
			</li><br />';
			}
			
			echo '
<form action="?todo=notif" method="post">
	<p id="form-notif" class="form-notif">
		<ul>
		'.$html.'
		</ul>
		<input type="hidden" name="action" value="validate" />
		<input type="submit" value="'._("Validate notification").'" />
	</p>
</form>
';
			
		} else {
			$sql = "SELECT `id`, `name`, `version` FROM `meik_exts` WHERE `notif` = '0' ORDER BY `name` ASC;";
			$exts = $meik->query($sql);
					
			$dispo = null;
			
			if(is_array($exts)) {
				foreach($exts as $member) {
					$dispo .= '					<option value="'.$member["id"].'">'.$member["name"].' ('.$member["version"].')</option>
		';
				}
			}
			echo '
		<form action="?todo=notif" method="post" onsubmit="javascript:recap_notif();">
			<p id="form-add" class="form-add">
				'._("Please select extension you want to notify updates. You can select multiple element by keeping the CTRL key pressed while selecting with the mouse. Only extension which haven't been notified about an update are listed here.").'<br />
				<select id="choisies" name="choisies" size="30" multiple="multiple">
'.$dispo.'
				</select><br /><br />
				<input type="hidden" name="action" value="proceed" />
				<input type="hidden" id="exts" name="extensions" />
				<input type="submit" value="'._("Notify update for selected extension").'"/>
			</p>
		</form>
';
		}

		break;
			
	default:
		$profils = $meik->get_user_profiles($userid);
		echo '
<h3>'._("Profiles management").'</h3>
';

		if(is_array($profils)) {
			echo '
			<form action="?todo=modifp" method="post">
			<p id="content-profils">'._("Your profiles").' : 
				<select id="select-profils" name="profil">
			';
			foreach($profils as $entry) {
				echo '		<option value="'.$entry["id"].'" >'.$entry["name"].'</option>';
			}
			echo '
				</select>
			</p>
				<div id="menu-profils">
					<dl class="menu-profils">	
						<dd><input type="submit" name="modif" value="'._("Modify this profile").'" /></dd>
						<dd><input type="submit" name="delete" value="'._("Delete this profile").'" /></dd>
						<dd><input type="submit" name="inst" value="'._("Procede to install").'" /></dd>
					</dl>
				</div>
			</form>
';
		} else {
			echo '<br />'._("You have no profile registered.").'<br />'._("You have to first create at least one extensions profile.").'<br /><a href="?todo=addp">'._("Create my first profile.").'</a>';
		}
		
		break;
}

echo '</div>';
		
?>