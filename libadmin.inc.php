<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
 	/*************************************************************
	 *************************************************************
	 **                                                         **
	 **  Now entering within the Administration-oriented code.  **
	 **                                                         **
	 *************************************************************
	 *************************************************************/

// $admin = new Admin();

class Admin extends MEIK {

	/**
	 * Lighter version of $this->retrievefiles(). Same goal : copy URLs to "tmp/" directory.
	 * 
	 * @param $urls The URL list wether an array or a string with linebreak.
	 * 
	 * @return true or false, wether at least one file successfully downloaded.
	 */
	function admin_retrievefiles($urls) // $urls : suites d'url s�par�es par \n
	{
		if(!is_array($urls)) {
			$urls = explode("\n", $urls);
		}
		debug("Got " . count($urls) . " URLs.<br />");
		if(!is_dir("tmp/")) {
			mkdir("tmp/");
		}
		foreach($urls as $url) {
			$url = trim($url);
			$content = @file_get_contents($url);
			if(false !== $content) {
				$name = basename($url);
				if($name) {
					$file = fopen("tmp/".$name, "w");
					if(fwrite($file, $content)) {
						echo _("Successfully downloaded")." <b>$url</b> "._("to")." <u>tmp/$name</u>.<br />";
						$files[] = $name;
						fclose($file);
					}
				}
			} else {
				echo _("There was a problem trying to download").' : "'.$url.'". '._("Please check this address and try again.").'<br />';
			}
		}
		
		return (@count($files) > 0);
	}
	
	/**
	 * This is some kind of meta-function, used to explode huge pages within many little. So we need some navigation links.
	 * @param $table     The MySQL table to work with.
	 * @param $next      The next page's number.
	 * @param $condition Set some condition to filter the SELECT request.
	 * 
	 * @return The HTML code for navigation.
	 */
	function admin_createlinks($table, $next, $condition = "")
	{
		if(!empty($condition)) {
			$cond = "WHERE $condition";
			/** @todo May need some improvements to prevent from SQL Injection. */
		} else {
			$cond = "";
		}
		$count = "SELECT COUNT(*) FROM `$table` $cond;"; // simply count how many they are.
		$count = $this->query($count);
		$count = $count[0]["COUNT(*)"]; // Get the result.
		$pages = floor($count/$this->nombre)+1; // which page will be the last?
		
		$html = _("Navigation :");
		
		for($i = 0; $i < $pages; $i++) { // Loop for each page.
			if( ($i*$this->nombre) == $next) { // Are we on a page number ?
				$html .= '
		<span class="current">' . $i . '</span> '; // Yes, and it's our current page !
			} else {
				$html .= '
		<a href="'.$_SERVER["PHP_SELF"] . '?manager=' .$GLOBALS["man"].'&amp;next=' . ($i * $this->nombre) . '">' . $i . '</a> '; // Yes, but it's not the current, so create a link.
			}
		}
		
		$html = '
	<p id="nav">' . $html . '
	</p>';
		
		return $html; // Send HTML.
	}
	
	/**
	 * Just list the users.
	 * 
	 * @return An array, with all the users.
	 */
	function admin_listusers($by_id, $by_login)
	{
		if($by_id !== false) {
			if ($by_id == 0    || $by_id == 1)
				$order = ($by_id == 0)    ? "ORDER BY `id` ASC"    : "ORDER BY `id` DESC";
			
			$_SESSION["order_users_id"] = $by_id;
			$_SESSION["order_users_login"] = NULL;
		}
		
		if ($by_login !== false) {
			if ($by_login == 0 || $by_login == 1)
				$order = ($by_login == 0) ? "ORDER BY `login` ASC" : "ORDER BY `login` DESC";
		
			$_SESSION["order_users_id"] = NULL;
			$_SESSION["order_users_login"] = $by_login;
		}
		
		$get = "SELECT * FROM `meik_users` " . $order . " LIMIT $this->start, $this->nombre";
		// Notice that it's ready for page explosion :) No huge stufffff :)
		$got = $this->query($get);
		return $got;
	}
	
	/**
	 * We have to make you go.
	 * 
	 * @param $id The user's ID.
	 * 
	 * @return true or false wether the user was correctly deleted or not !
	 */
	function admin_deleteuser($id)
	{
		debug("Trying to delete user $id.");
		$profiles = $this->get_user_profiles($id); // List all user's profile.
		
		if(!is_array($profiles)) {
			debug("No profile !");
		} else {
			foreach($profiles as $profile) { // Clean the database, his profile won't be useful anymore.
				$pid = $profile["id"];
				$del = "DELETE FROM `meik_profiles_data` WHERE `meik_profile_id` = '$pid';";
				if(!$this->query($del)) {
					debug("There was an error while deleting profile $pid data.");
					return false;
				}
			}
		}
		
		// Delete profiles list.
		$del = "DELETE FROM `meik_profiles` WHERE `user_id` = '$id';";
		if(!$this->query($del)) {
			debug("There was an error while deleting profiles of user $id.");
			return false;
		}
		
		// And then, launch the charter !
		$final = "DELETE FROM `meik_users` WHERE `id` = '$id';";
		return $this->query($final);
	}
	
	/**
	 * Change a user status.
	 * 
	 * @param $id     The user's ID.
	 * @param $statut The new status :)
	 * 
	 * @return true or false, wether change was successfull or not.
	 */
	function admin_modif_statut($id, $statut = MEIK_STATUT_USER)
	{
		$update = "UPDATE `meik_users` SET `statut` = '$statut' WHERE `id` = '$id' LIMIT 1;";
		debug("Modifying status of $id .... Setting $statut.");
		return $this->query($update);
	}
	
	/**
	 * Change a user from normal to administrator.
	 * 
	 * @param $id The user's ID.
	 * 
	 * @return true or false, wether he has been successfully promoted or not.
	 */
	function admin_promote($id)
	{
		return $this->admin_modif_statut($id, MEIK_STATUT_ADMIN);
	}
	
	/**
	 * Change a user from administrator to normal role.
	 * 
	 * @param $id The user's ID.
	 * 
	 * @return true or false, wether he has been successfully unpromoted or not.
	 */
	function admin_unpromote($id)
	{
		return $this->admin_modif_statut($id, MEIK_STATUT_USER);
	}
	
	/**
	 * List all available extensions. Might be huge, so it's done step by step :)
	 * 
	 * @return An array containing the whole MySQL results.
	 */
	function admin_listexts($by_id, $by_name)
	{
		if($by_id !== false) {
			if ($by_id == 0   || $by_id == 1)
				$order = ($by_id == 0)    ? "ORDER BY `id` ASC"  : "ORDER BY `id` DESC";
			
			
			$_SESSION["order_exts_id"] = $by_id;
			$_SESSION["order_exts_name"] = NULL;
		}
		
		if ($by_name !== false) {
			if ($by_name == 0 || $by_name == 1)
				$order = ($by_name == 0) ? "ORDER BY `name` ASC" : "ORDER BY `name` DESC";
			
			$_SESSION["order_exts_name"] = $by_name;
			$_SESSION["order_exts_id"] = NULL;
		}
		
		$get = "SELECT * FROM `meik_exts` " . $order . " LIMIT $this->start, $this->nombre";	
		$got = $this->query($get);
		return $got;
	}
	
	/**
	 * Delete an extension from database & FS.
	 * 
	 * @param $id The extension's ID.
	 * 
	 * @return true or false, wether it worked or not.
	 */
	function admin_deleteext($id)
	{
		debug("Trying to delete extension $id.");
		$uuid = $this->getextuuidfromid($id);
		$xpi  = $this->admin_get_extxpi_name($id);
		$xpi  = $xpi[0]["xpi"];
		
		// Delete from profiles.
		$del = "DELETE FROM `meik_profiles_data` WHERE `meik_ext_id` = '$id';";
		if(!$this->query($del)) {
			debug("There was an error while deleting extension $id from profiles.");
			return false;
		}
		
		// Delete versions informations.
		$del = "DELETE FROM `meik_exts_vers` WHERE `uuid_ext` = '$uuid';";
		if(!$this->query($del)) {
			debug("There was an error while deleting extension $id versions.");
			return false;
		}
		
		// Launch this pretty charter :)
		$final = "DELETE FROM `meik_exts` WHERE `id` = '$id';";
		return ($this->query($final) && $this->admin_removeold($xpi));
	}
	
	/**
	 * Lis of all the target applications.
	 * 
	 * @return The array which contain all our informations.
	 */
	function admin_listapplis()
	{
		/**
		 * @todo Might be useful to use some LIMIT in MySQL request
		 * for those who'll many targets.
		 */
		$get = "SELECT * FROM `meik_target` ORDER BY `id` ASC";
		$got = $this->query($get);
		return $got;
	}
	
	/**
	 * Just add a new application as target.
	 * 
	 * @param $uuid It's UUID.
	 * @param $nom  The new application name.
	 * 
	 * @return true or false, wether it has been successfully added. 
	 */
	function admin_addappli($uuid, $nom)
	{
		$get = "INSERT INTO `meik_target` (`uuid`, `nom`) VALUES ('$uuid', '$nom')";
		$got = $this->query($get);
		return $got;
	}
	
	/**
	 * Just delete ...
	 * 
	 * @param $id The application ID.
	 * 
	 * @return true or false, wether it has been successfully added.
	 */
	function admin_delappli($id)
	{
		/**
		 * @todo Also delete references everywhere in the db.
		 */
		$get = "DELETE FROM `meik_target` WHERE `id` = '$id' LIMIT 1;";
		$got = $this->query($get);
		return $got;
	}
	
	/**
	 * Update the application data. We can update UUID, and it's name.
	 * 
	 * @param $id   The application's ID.
	 * @param $uuid The application's UUID.
	 * @param $nom  The application's name.
	 * 
	 * @return true or false, wether it has been successfully added.
	 */
	function admin_updateappli($id, $uuid, $nom)
	{
		/**
		 * @todo URGENT: also update extension's data !
		 */
		$get = "UPDATE `meik_target` SET `id` = '$id'," .
				" `uuid` = '$uuid'," .
				" `nom` = '$nom'" .
				" WHERE `id` = '$id' LIMIT 1;";
		$got = $this->query($get);
		return $got;
	}
	
	/**
	 * Get the list of updateURL and UUID to check for automatic update processus
	 * 
	 * @return array An array where each node contains updateURL and UUID of extension.
	 */
	function admin_listupdateurl()
	{
		$get = "SELECT `updateurl`, `uuid` FROM `meik_exts` WHERE `updateurl` <> '';";
		$got = $this->query($get);
		
		return $got;
	}
	
	/**
	 * Get the list of updateURL and UUID to check for automatic update processus, but limit search to
	 * passed arguments.
	 * 
	 * @param Array $extensions An array containing all the extension's ID to retrieve Update URL from.
	 * 
	 * @return array An array where each node contains updateURL and UUID of extension.
	 */
	function admin_listupdateurl_restrict($extensions)
	{
		$stuffz = '`id` = ' . array_shift($extensions) . ' ';
		foreach($extensions as $id)
		{
			$stuffz .= ' OR `id` = ' . mysql_real_escape_string($id);
		}
		$get = "SELECT `updateurl`, `uuid` FROM `meik_exts` WHERE `updateurl` <> '' AND (" . $stuffz . ");";
		$got = $this->query($get);
		
		return $got;
		
	}
	
	/**
	 * Start the installation of a new extension. Make install ;)
	 * 
	 * @return true.
	 */
	function admin_makeinstall()
	{
		$liste = $this->listPackages($this->base."/tmp/"); // List all the XPI.
		foreach($liste as $ext) {
			$uuid  = $ext["uuid"]; // Retrieve UUID
			$fname = $this->extfile($uuid); // Find if there's already stuff for this uuid.
			$xpi   = $ext["entry"]; // Tell us the filename.
			if($fname) { // It's an UPDATE !
				debug("There is a file for uuid $uuid : $fname. Will be erased for $xpi.");
				echo $fname . ' '._("already exists for extension").' ' . $uuid . '. '._("Will be overwrited by").' ' . $xpi .'. <br /> ';
				$this->admin_replace($fname, $xpi); // We replace.
			} else { // It's a NEW INSTALLATION.
				debug("No previous file found for $uuid !");
				echo _("No previous installation of").' ' . $uuid . '. '._("Going to use this file").' ' . $xpi . '<br />';
				$this->admin_copynew($xpi); // We install.
			}
		}
		
		return true; // Such a liar ...
	}
	
	/**
	 * Simply used to update an XPI.
	 * 
	 * @param $old The old XPI file ...
	 * @param $new The new XPI file ...
	 * 
	 * @return true or false, wether it has been successfully replaced.
	 */
	function admin_replace($old, $new) // replaces $old XPI with $new XPI in $this->xpidir
	{		
		if(is_file($this->xpireldir.$old) && is_file("tmp/".$new)) {
			return ($this->admin_removeold($old)
				 && $this->admin_copynew($new));
		} else {
			debug("One or more isn't a file ! Check for $old and $new !");
			echo _("One or more of this isn't a correct file") . ' : ' .
				is_file($this->xpireldir.$old) . ' ('._("old").') &amp;&amp; ' .is_file("tmp/".$new) . ' ('._("new").').<br />';
			return false;
		}
	}
	
	/**
	 * Remove an extension package.
	 * 
	 * @param $old The XPI filename.
	 * 
	 * @return true or false, wether it has been successfully deleted.
	 */
	function admin_removeold($old)
	{
		if(@unlink($this->xpireldir.$old)) {
			debug("Ok, ".$this->xpireldir.$old." deleted. Continuing.");
			echo $this->xpireldir.$old . ' '._("deleted, continuing.").'<br />';
			return true;
		} else {
			debug("Error while deleting $old. Stopping.");
			echo _("Error while deleting").' ' . $old . '. '._("Stopping.").'<br />';
			return false;
		}
	}
	
	/**
	 * Copy a new package from a dir to the XPI's directory.
	 * 
	 * @param $new The XPI filename.
	 * @param $src The source directory, default is 'tmp/'.
	 * 
	 * @return true or false, wether it has been successfully copied.
	 */
	function admin_copynew($new, $src = "tmp/")
	{
		if(@copy($src.$new, $this->xpireldir.$new)) { // Copy !
			debug("Ok, $new copied to $this->xpireldir. Continuing.");
			echo $new . ' '._("copied into").' '.$this->xpireldir.', '._("continuing.").'<br />';
			if (@unlink($src . $new)) { // Delete the source.
				debug("Ok, ".$src.$new." deleted. Ending.");
				echo $src . $new . ' '._("deleted, end.").'<br />';
				return true;
			} else { // Error while deleting.
				debug("Error while copying $src$new. Stopping.");
				echo _("Error while copying").' ' . $src . $new . '. '._("Stopping.").'<br />';
				return false;
			}
		} else { // Cannot copy :'(
			debug("An error occured while copying $new to $this->xpireldir. Stopping.");
			echo _("Error while copying").' ' . $new . ' '._("to").' ' . $this->xpireldir . '. '._("Stopping.").'<br />';
			return false;
		}
	}
	
	/**
	 * Make the list of all extensions that were submitted. Used by $this->admin_showsubmit().
	 * 
	 * @return false if nothing was found, an array of the urls if something is present :)
	 */
	function admin_listsubmit()
	{
		$select = "SELECT `url` FROM `meik_submit` LIMIT $this->start, $this->nombre;";
		$select = $this->query($select);
		
		if(is_array($select)) { // Did we found something ?
			foreach($select as $el) {
				$urls[] = $el["url"];
			}
			return $urls; // Pretty cool !
		} else {
			return false; // Go back to work !
		}
	}
	
	/**
	 * So, we want to delete a submission.
	 * 
	 * @param $filename The XPI filename. Will match part of the URL (in fact, the filename in the url).
	 * 
	 * @return true or false wether it has been successfully deleted.
	 */
	function admin_deletesubmit($filename)
	{
		$del = "DELETE FROM `meik_submit` WHERE `url` LIKE '%$filename' LIMIT 1;";
		return $this->query($del);
	}
	
	/**
	 * We want to have the full list of XPI that are inside the DB.
	 * 
	 * @return An array, containing this data.
	 */
	function admin_list_xpi_db()
	{
		$select = "SELECT `xpi` FROM `meik_exts`";
		$select = $this->query($select);

		$result = null;

		if(is_array($select)) {
			foreach($select as $node) {
				$result[] = $node["xpi"];
			}
		}

		return $result;
	}
	
		
	/**
	 * We want to have the XPI's file name of an extension.
	 * 
	 * @param $id The Extension's ID in the DB.
	 * 
	 * @return The correspondant XPI, if found, otherwise, false.
	 */
	function admin_get_extxpi_name($id)
	{
		$get = "SELECT `xpi` FROM `meik_exts` WHERE `id` = '$id'";
		$got = $this->query($get);
		return $got;
	}
	
	/**
	 * Just list all xpi within $this->xpireldir
	 * 
	 * @return An array, containing the data.
	 */
	function admin_list_xpi_fs()
	{
		$result = array(); // initialize the array.
		$dir = $this->xpidir;
		$k = 0;
		$d = @dir($dir);
		if($d) {
			while (false !== ($entry = $d->read())) { // List all the files.
				if(strstr($entry, ".xpi")) { // It's an XPI ! Might be better to check for MIME type ?
					debug("Found one XPI : $entry");
					$result[] = $entry;
				}
			}
			$d->close();
		} else {
			error(_("Please check that directory named 'xpis' exists within directory named : ") . ' `' . $dir . "'");
		}
		
		return $result;
	}
	
	
	/**
	 * Simply make a diff between XPI stored in the DB, and those presents in $this->xpireldir ...
	 * 
	 * @return An array, with XPI that are not in the DB.
	 */
	function admin_list_xpi_nonadded()
	{
		$db = $this->admin_list_xpi_db();
		$fs = $this->admin_list_xpi_fs();
		
		$diff = array();
		
		foreach ($fs as $file) {
			if(!in_array($file, $db)) {
				$diff[] = $file;
			}
		}
		
		return $diff;
	}
	
	/**
	 * Check if update are available at URL.
	 * 
	 * @param $url The extension URL to check
	 * @param $newver The new version string, filled by the function.
	 * 
	 * @return array containing XPI url. Looks like this :
	 * 
	 * Array
(
    [c45c406e-ab73-11d8-be73-000a95be3b12] => Array
        (
            [ec8030f7-c20a-464f-9b0e-13a3a9e97384] => Array
                (
                    [minVersion] => 1.0
                    [maxVersion] => 1.5.0.*
                    [updateLink] => http://chrispederick.com/work/webdeveloper/webdeveloper.xpi
                )

            [a463f10c-3994-11da-9945-000d60ca027b] => Array
                (
                    [minVersion] => 1.0
                    [maxVersion] => 1.0+
                    [updateLink] => http://chrispederick.com/work/webdeveloper/webdeveloper.xpi
                )

            [86c18b42-e466-45a9-ae7a-9b95ba6f5640] => Array
                (
                    [minVersion] => 1.0
                    [maxVersion] => 1.7.12
                    [updateLink] => http://chrispederick.com/work/webdeveloper/webdeveloper_mozilla.xpi
                )

        )

)
	 */
	function admin_check_update($url, &$newver, $verb = true)
	{
		/** @todo Add substitution code for %ITEM_I% and others ... */
		
		if(false !== ($rdf = @file_get_contents($url))) {
			info(_("Retrieving data from") . ' ' . $url);
			$xml = new MofoRDF($rdf);
			$xml->rdf_set_type("MOFO_UPDATE_RDF");
			$res = $xml->get_array();
			// $res = parse_update_manifest($rdf);
			
			/**
			 * res (example) : 
			 * Array
			(
    [extensions] => Array
        (    | ------ extension UUID -----------|
            [c45c406e-ab73-11d8-be73-000a95be3b12] => Array
                (    |---|-> extension version
                    [1.0.2] => Array
                        (    | --------- target UUID -----------|
                            [ec8030f7-c20a-464f-9b0e-13a3a9e97384] => Array
                                (
                                    [minVersion] => 1.0
                                    [maxVersion] => 1.5.0.*
                                    [updateLink] => http://chrispederick.com/work/webdeveloper/webdeveloper.xpi
                                )

                            [a463f10c-3994-11da-9945-000d60ca027b] => Array
                                (
                                    [minVersion] => 1.0
                                    [maxVersion] => 1.0+
                                    [updateLink] => http://chrispederick.com/work/webdeveloper/webdeveloper.xpi
                                )

                            [86c18b42-e466-45a9-ae7a-9b95ba6f5640] => Array
                                (
                                    [minVersion] => 1.0
                                    [maxVersion] => 1.7.12
                                    [updateLink] => http://chrispederick.com/work/webdeveloper/webdeveloper_mozilla.xpi
                                )

                        )
                        .
                        .
                        .
			)
			 */
			
			$link = array();
						
			if(is_array($res) && count($res["extensions"]) > 0) {
				foreach($res as $liste) {
					if(is_array($liste)) {
						foreach($liste as $uuid => $rev) {
							$ver = $this->extver($uuid);
							$max = 0;
							foreach($rev as $new => $data) {
								// echo "\$this->comp_ver($new, $ver) => " .($this->comp_ver($new, $ver) ? "VRAI" : "FAUX");
								if($this->comp_ver($new, $ver) && $this->comp_ver($new, $max)){
									$max = $new;
									$link[$uuid] = $data;
								}
							}
						}
					} else {
						if($verb) error(_("Unable to compute RDF versions tree."));
						return false;
					}
				}

				$newver = $max;
				return $link;
			} else {
				if($verb) error(_("Unable to compute RDF tree."));
				return false;
			}
		} else {
			if($verb) error(_("Cannot retrieve content at") . ' ' . $url);
			return false;
		}
	}
	
	/**
	 * Print out the users list ! Sexy XHTML Code ;)
	 * 
	 * @return The HTML code to print.
	 */
	function admin_show_userlist()
	{
		$sess = $GLOBALS["session"];
		$html = '<h3>'._("Users management") . '</h3>'; // Some headline.
		
		// order by ?
		/*
		$by_id    = getvar("by_id");
		$by_login  = getvar("by_login");
		*/
		$by_id    = (getvar("by_id") 	== NULL)	? $sess->getsessvar("order_users_id")		: getvar("by_id");
		$by_login = (getvar("by_login") == NULL)	? $sess->getsessvar("order_users_login")	: getvar("by_login");
		
		$txt = array
			(
				"id-asc"		=> _("Sort by") . ' ' . _("ID") . ' ' . _("ascendant"),
				"id-desc"		=> _("Sort by") . ' ' . _("ID") . ' ' . _("descendant"),
				"login-asc"		=> _("Sort by") . ' ' . _("Login") . ' ' . _("ascendant"),
				"login-desc"	=> _("Sort by") . ' ' . _("Login") . ' ' . _("descendant"),
			);
		$id = '<a href="admin.php?manager=users&amp;by_id=0"><img src="images/asc.png" alt="' . $txt["id-asc"] . '" title="' . $txt["id-asc"] . '"/></a>' . _("ID") . '<a href="admin.php?manager=users&amp;by_id=1"><img src="images/desc.png" alt="' . $txt["id-desc"] . '" title="' . $txt["id-desc"] . '" /></a>';
		$login = '<a href="admin.php?manager=users&amp;by_login=0"><img src="images/asc.png" alt="' . $txt["login-asc"] . '" title="' . $txt["login-asc"] . '" /></a>' . _("Login") . '<a href="admin.php?manager=users&amp;by_login=1"><img src="images/desc.png" alt="' . $txt["login-desc"] . '" title="' . $txt["login-desc"] . '" /></a>';
		
		$users = $this->admin_listusers($by_id, $by_login); // Get the users list.
		
		$html .= '
<div class="liste-users">
	<table class="users">
		<thead>
			<tr>
				<th class="head-user-id">'.$id.'</th>
				<th class="head-user-login">'.$login.'</th>
				<th class="head-user-del">'._("Delete").'</th>
				<th class="head-user-promote">'._("Promote").'</th>
				<th class="head-user-special">'. _("Native account") .'</th>
			</tr>
		</thead>
		<tbody>
			';

		if(is_array($users)) { // Is it a list?
			foreach($users as $user) {
				$id      = $user["id"]; // User's unique ID.
				$login   = $user["login"]; // Login
				$admin   = $user["statut"]; // Status
				$nnative = $user["nnative"]; // Native account or not.
				
				$html .= '
				<tr>
					<td class="user-id">'.$id.'</td>
					<td class="user-login">'.$login.'</td>
					<td class="user-del">
						<form action="admin.php?manager=muser" method="post">
							<div id="del-user-'.$id.'" class="user">
								<input type="hidden" name="id" value="'.$id.'" />
								<input type="hidden" name="login" value="'.$login.'" />
								<input type="hidden" name="next" value="'.$this->start.'" />
								<input type="submit" name="delete" value="'._("Delete").'" />
							</div>
						</form>
					</td>
					<td class="user-prom">
						<form action="admin.php?manager=muser" method="post">
							<div id="prom-user-'.$id.'" class="user">
								<input type="hidden" name="id" value="'.$id.'" />
								<input type="hidden" name="login" value="'.$login.'" />
								<input type="hidden" name="next" value="'.$this->start.'" />
								'.(!$admin ? '<input type="submit" name="promote" value="'._("Make administrator").'" />' :
								'<input type="submit" name="unpromote" value="'._("Unmake administrator").'" />' ).'
							</div>
						</form>
					</td>
					<td class="user-nnative">
						'. ($nnative ? _("No") : _("Yes")) .'
					</td>
				</tr>
	';
			}
		} else { // So bad. ...
			$html .= '
<p class="error">'._("No user has been found.").'</p>
';
		}
		
		$html .= '
				</tbody>
	</table>
</div>';
		
		return $html;
	}
	
	/**
	 * Now, it's time for extension.
	 * 
	 * @return Some pretty XHTML code.
	 */
	function admin_show_extslist()
	{
		$sess = $GLOBALS["session"];
		$html =  '<h3>'._("Extensions management").'</h3>';
		
		// order by ?
		/*
		$by_id    = getvar("by_id");
		$by_name  = getvar("by_name");
		*/
		$by_id    = (getvar("by_id") 	== NULL)	? $sess->getsessvar("order_exts_id")		: getvar("by_id");
		$by_name  = (getvar("by_name")	== NULL)	? $sess->getsessvar("order_exts_name")		: getvar("by_name");
		
		$txt = array
			(
				"id-asc"	=> _("Sort by") . ' ' . _("ID") . ' ' . _("ascendant"),
				"id-desc"	=> _("Sort by") . ' ' . _("ID") . ' ' . _("descendant"),
				"name-asc"	=> _("Sort by") . ' ' . _("Name") . ' ' . _("ascendant"),
				"name-desc"	=> _("Sort by") . ' ' . _("Name") . ' ' . _("descendant"),
			);
		
		$id = '<a href="admin.php?manager=exts&amp;by_id=0"><img src="images/asc.png"  alt="' . $txt["id-asc"] . '" title="' . $txt["id-asc"] . '"/></a>' . _("ID") . '<a href="admin.php?manager=exts&amp;by_id=1"><img src="images/desc.png"  alt="' . $txt["id-desc"] . '" title="' . $txt["id-desc"] . '"/></a>';
		$name = '<a href="admin.php?manager=exts&amp;by_name=0"><img src="images/asc.png"  alt="' . $txt["name-asc"] . '" title="' . $txt["name-asc"] . '"/></a>' . _("Name") . '<a href="admin.php?manager=exts&amp;by_name=1"><img src="images/desc.png"  alt="' . $txt["name-desc"] . '" title="' . $txt["name-desc"] . '"/></a>';
		
		$exts = $this->admin_listexts($by_id, $by_name); // List the whole extensions we want to print.
		
		$html .= '
<div class="liste-exts">
	<table class="exts">
		<thead>
			<tr>
				<th class="head-ext-id">'. $id .'</th>
				<th class="head-ext-nom">'. $name .'</th>
				<th class="head-ext-ver">'._("Version").'</th>
				<th class="head-ext-del">'._("Delete").'</th>
				<th class="head-ext-update">'._("Update").'</th>
				<!-- <th class="head-ext-update2">'._("Update URL").'</th> -->
			</tr>
		</thead>
		<tbody>
		';
		
		if(is_array($exts)) {	
			foreach($exts as $ext) {
				$id       = $ext["id"];
				$name     = $ext["name"];
				$version  = $ext["version"];
				$update   = ((count(parse_url($ext["updateurl"])) > 1) ?
							'<div id="update-ext-'.$id.'" class="exts-opts">
								<input type="button" name="add-to-update" value="'._("Add to update list").'" onclick="add_to_update_list(' . $id . ', \'' . $name . '\');" />
							</div>' ///< In this case, there's a custom updateURL
							: _("None.")); ///< Bad !
				
				$hasupdate = (count(parse_url($ext["updateurl"])) > 1);
				
				$html .= '
				<tr>
					<td>' .$id. '</td>
					<td>' .$name. '</td>
					<td>' .$version. '</td>
					<td>
						<form action="admin.php?manager=mext" method="post" class="ext-form">
							<div id="ext-'.$id.'" class="exts-opts">
								<input type="submit" name="delete" value="'._("Delete").'" />
								<input type="hidden" name="id" value="'.$id.'" />
								<input type="hidden" name="name" value="'.$name.'" />
								<input type="hidden" name="next" value="'.$this->start.'" />
								<input type="hidden" name="hasupdate" value="'.$hasupdate.'" />
							</div>
						</form>
					</td>
					<td>
						' . $update . '
					</td>
					<!-- <td>
						' . htmlentities($ext["updateurl"]) . '
					</td> -->
				</tr>
	';
			}
		} else {
			$html .= '
<p class="error">'._("No extension has been found.").'</p>
';
		}
		
		$html .= '
		</tbody>
	</table>
	<br />

	<form action="admin.php?manager=mass-update" method="post" class="ext-form" onsubmit="recap_update();">
		<fieldset id="fiel-mass-update">
			<legend>' . _("Update list") . '</legend>
			<label>'  . _("This list contains the whole extensions that will be requested for update once validated.<br />Just click on one extension once it's in the list to view informations about the selected extension.<br />Click 'Remove' to not select this extension for update retrieval.") . '</label><br />
			' . $this->showdesc() . '
			<select id="mass-update" name="mass-update" size="15" multiple="multiple" onchange="makeXML(\'mass-update\');">
			</select><br />
			<input type="button" name="addall"  value="'  . _("Add all extensions with Update URL") . '" onclick="add_all_to_update_list();" /> | 
			<input type="button" name="remove"  value="'  . _("Remove this extension") . '" onclick="remove_from_update_list();" /> | 
			<input type="submit" name="execute" value="' . _("Retrieve as many as update available") . '" />
			<input type="hidden" name="liste"   value="" id="exts" />
		</fieldset>
	</form>

</div>';
		
		return $html;
	}
	
	/**
	 * It's the same, for applications.
	 * 
	 * @return XHTML Code, once again.
	 */
	function admin_show_applis()
	{
		$html = '<h3>'._("Target management").'</h3>';
		$applis = $this->admin_listapplis(); // Applications lists, once again.

		foreach($applis as $appli) {
			$id      = $appli["id"];
			$nom     = $appli["nom"];
			$uuid    = $appli["uuid"];
			
			$html .= '
<form action="admin.php?manager=mappli" method="post">
	<p id="appli-'.$id.'" class="liste-appli">
		<input type="submit" name="modif" value="'._("Modify").'" />
		<input type="text" name="appli_nom" value="'.$nom.'" size="24"/> => 
		<input type="text" name="appli_uuid" value="'.$uuid.'" size="47" maxlength="36" /> 
		<input type="submit" name="delete" value="'._("Delete").'" />
		<input type="hidden" name="id" value="'.$id.'" />
		<input type="hidden" name="nom" value="'.$nom.'" />
		<input type="hidden" name="next" value="'.$this->start.'" />
	</p>
</form>
';
		}
		
		$html .= '
<hr /><h4>'._("Adding an application:").'</h4>
	<form action="admin.php?manager=mappli" method="post">
		<p id="add-appli" class="add-appli">
		   '._("Application name").' : <input type="text" name="nom" /><br />
		   '._("Application UUID").' : <input type="text" name="uuid" /><br />
			<input type="submit" name="ajout" value="'._("Add this application").'" />
		</p>
	</form>
';
		return $html;
	}
	
	/**
	 * Show the notification queue !
	 */
	function admin_shownotifs()
	{
		$req = "SELECT `ext_id`, `url` FROM `meik_notifs` LIMIT $this->start, $this->nombre;";
		$req = $this->query($req);
		
		if(is_array($req)) { // There are some notifications !
			$html = '
	<form action="admin.php?manager=mvalid-not" method="post">
		<table class="users">
			<thead>
				<tr>
					<th>'._("Reject").'</th>
					<th>'._("Accept").'</th>
					<th>'._("Extension").'</th>
					<th>'._("URL").'</th>
				</tr>
			<thead>
			<tbody>';
			
			$i = 0;
			foreach($req as $ext) {
				$extension = $this->getextname($ext["ext_id"]);
				$url       = $ext["url"];
				
				$html .= '
				<tr>
					<td><input type="radio" name="ext['.$i.'][act]" value="del" /></td>
					<td><input type="radio" name="ext['.$i.'][act]" value="val" checked="checked" /></td>
					<td>'.$extension.'</td>
					<td>'.$url.'<input type="hidden" name="ext['.$i.'][url]" value="'.$url.'" /></td>
				</tr>
	';
			$i++;
			}
			
			$html .= '
			</tbody>
		</table>
		<input type="submit" value="'._("Process").'" />
	</form>
	';
		} else { // Ok, go back to work !
			$html = _("Sorry, no extension is in the queue list of notification update.");
		}
		
		return $html;
	}
	
	/**
	 * We want to delete a notification from the queue.
	 * 
	 * @param $url The URL of notification to delete.
	 * 
	 * @return true or false, wether it has been successfully deleted.
	 */
	function admin_delnotif($url)
	{
		$del = "SELECT `ext_id` FROM `meik_notifs` WHERE `url` = '$url' LIMIT 1;";
		$del = $this->query($del);
		$id  = $del[0]["ext_id"];
		// We suppose the URL exists.
		
		$del = "DELETE FROM `meik_notifs` WHERE `ext_id` = '$id' LIMIT 1;";
		if($this->query($del)) { // Delete .
			$upd = "UPDATE `meik_exts` SET `notif` = '0' WHERE `id` = '$id' LIMIT 1;";
			if($this->query($upd)) { // Update notification to 'none'.
				return true; // YES.
			} else {
				return false; // fucking terrorists.
			}
		} else {
			return false; // So bad.
		}
	}
	
	/**
	 * Show the new extension which were submitted.
	 * 
	 * @return The XHTML code, ready to be used !
	 */
	function admin_showsubmit()
	{
		$urls = $this->admin_listsubmit();
		$files = null;
		$this->retrievefiles($urls, "tmp-submit/", $files, true); // Download all submitted packages.
		$news = $this->listPackages(dirname($_SERVER["SCRIPT_FILENAME"]) . "/tmp-submit/"); // List all the downloaded packages.

		if(@count($news) > 0) {
			$html = '
	<form action="admin.php?manager=mvalid-sub" method="post">
		<table class="users">
			<thead>
				<tr>
					<th>'._("Reject").'</th>
					<th>'._("Accept").'</th>
					<th>'._("Version").'</th>
					<th>'._("Extension").'</th>
					<th>'._("Description").'</th>
					<th>'._("Filename").'</th>
				</tr>
			</thead>
			<tbody>
			';
			
			$i = 0;
			foreach($news as $ext) {
				$version     = $ext["version"];
				$extension   = $ext["extension"];
				$description = $ext["description"];
				$file        = $ext["entry"];
				
				// We might get '-1', as those fields are optional.
				if($version != "-1" && $extension != "-1" && $description != "-1") { // It's correct !
					$html .= '
				<tr>
					<td><input type="radio" name="ext['.$i.'][act]" value="del" /></td>
					<td><input type="radio" name="ext['.$i.'][act]" value="val" checked="checked" /></td>
					<td>'.$version.'</td>
					<td>'.$extension.'</td>
					<td>'.$description.'</td>
					<td>'.$file.'<input type="hidden" name="ext['.$i.'][file]" value="'.$file.'" /></td>
				</tr>
	';
				} else {
					$html .= '
				<tr>
					<td><input type="radio" name="ext['.$i.'][act]" value="del" checked="checked" /></td>
					<td><input type="radio" name="ext['.$i.'][act]" value="val" disabled="true" /></td>
					<td colspan="3"><span class="invalid">'._("Invalid extension").'</span></td>
					<td>'.$file.'<input type="hidden" name="ext['.$i.'][file]" value="'.$file.'" /></td>
				</tr>
	';
				}
			$i++;
			}
			
			$html .= '
			</tbody>
		</table>
		<input type="submit" value="'._("Process").'" />
	</form>
	';
		} else { // Some good time for you.
			$html = _("Sorry, no extension is in the queue list of submission.");
		}
		
		return $html;
	}
	
	/**
	 * This will download & analyze each Update RDF :)
	 * 
	 * @param $_urls The array containing the whole URLs to check.
	 * 
	 * @return Nothing, it will dump some good XHTML code :)
	 */
	function admin_showmassupdate_result($_urls)
	{
		$newver = null;
		$code = '
	<table class="users">
		<form action="admin.php?manager=madd" method="post">
		<caption>' . _("Here are the extensions that are you requested for update."). '</caption>
		<thead>
			<tr>
				<th>' . _("Extension") . '</th>
				<th>' . _("Target") . '</th>
				<th>' . _("Update") . '</th>
			</tr>
		</thead>
		<tbody>
';
		
		foreach($_urls as $url) {
			$urls = $this->admin_check_update($url["updateurl"], $newver);
			if(is_array($urls)) {
				foreach($urls as $uuid => $appli) {
					foreach($appli as $appid => $data) {
						$extname = (!$this->resolveuuid($uuid,  "ext")) ? $uuid : $this->resolveuuid($uuid,  "ext");
						$appname = (!$this->resolveuuid($appid, "app")) ? $uuid : $this->resolveuuid($appid, "app");
						// link : $data["updateLink"]
						/* echo '<p class="msg-info">' . _("We have been able to find this new version for the extension") . ' ``' . $extname . "'' ($uuid), for application ``" . $appname . "'' ($appid) : " . $newver . '. DOWNLOAD ===&gt; '. $data["updateLink"] . ' &lt;===</p>'; */
						$code .= '
				<tr>
					<td>' . $extname . '</td>
					<td>' . $appname . '</td>
					<td><input type="checkbox" name="urls[]" checked="checked" value="' . $data["updateLink"] . '" /></td>
				</tr>
	';
					}
				}
			} else {
				error(_("Invalid content, cannot parse."));
				$err = '<tr><td><p class="msg-debug">' . str_replace(array("<", ">", "\n", "  "), array("&lt;", "&gt;", "<br />", "&nbsp;&nbsp;"), @file_get_contents($url)) . '</p></td></tr>';
			}
		}
		
		$code .= '
		</tbody>
		<input type="submit" value="' . _("Start upgrade") . '" />
		</form>
	</table>
' . $err;
		if(is_array($_urls)) echo $code;
	}
	
	/**
	*/
	function admin_show_auth_infos()
	{
		require_once("setauth.inc.php");
		$meikauth = new Meik_Auth();
		$meikauth->auth	= $auth; ///< Defined in setauth.inc.php
		$meikauth->auth_plug_name = $auth_plug_name; ///< Defined in setauth.inc.php
		$backend = $meikauth->activate_backend(); // Select the autentication backend !
	 	$avail = $meikauth->extract_auth_backends_infos();
	 	
	 	$codaz = "<h3>" . _("Current authentication backend") . "</h3>";
	 		$codaz .= $meikauth->format_backend_infos($backend);
	 		
		$codaz .= "<h3>" . _("Other available authentication backend") . "</h3>";
		foreach($avail as $class) {
				$codaz .= $meikauth->format_backend_infos($class, _("To use this authentication backend") . ' : <a href="admin.php?manager=auth-set&amp;cls=' . urlencode(get_class($class)) . '&amp;val=' . urlencode($class->name) . '" title="' . _("Click to use this authentication backend") . '" accesskey="A">' . _("Activate !") . '</a>');
		}
		
		return $codaz;
	}
	
	function admin_show_auth_set()
	{
		$val	= urldecode(getvar("val"));
		$class	= urldecode(getvar("cls"));
		$meikauth = new Meik_Auth();
		$meikauth->auth				= $val;
		$meikauth->auth_plug_name	= $class;
		
		if($meikauth->check_backend($backend)) {
		 	$codaz = '<p class="msg-warning big">
' . _("Here is the backend you selected for election as new authentication backend. Please confirm your choice NOW, by clicking \"") . _("Yes, I know what I'm doing.") . '"
			</p>';
			$codaz .= "<h3>" . _("Selected authentication backend") . " : " . _("Informations") . "." . "</h3>";
			$codaz .= $meikauth->format_backend_infos($backend);
			$codaz .= '<br />

	<form action="admin.php?manager=auth-set-valid" method="post" class="ext-form" >
		<fieldset class="field-set-auth">
			<legend class="msg-error big">' . _("Your approval is needed. Please read carefully, or your installation might be corrupted, or your pets dead."). '</legend>
			<label class="msg-error big">
				' . _("By validating this choice, MEIK is going to rewrite part of its config. You are strongly advised to backup now the following files : ") . '
				<ol>
					<li>setauth.inc.php</li>
				</ol>
			</label>
			<input type="submit" name="valid"  value="' . _("Yes, I know what I'm doing."). '" />
			<input type="hidden" name="module" value="' . urlencode($val) . '" />
			<input type="hidden" name="class"  value="' . urlencode($class) . '" />
		</fieldset>
	</form>
	';
			return $codaz;	
		} else {
			error(_("Hmm, please check your authentication plugin, doesn't seems to be valid."));
		}
	}
	
	function admin_show_set_valid()
	{
		$auth_plugin	= urldecode(getvar("module"));
		$class_plugin	= urldecode(getvar("class"));
		$auth_valid		= getvar("valid_new_backend");
		
		$meikauth = new Meik_Auth();
		$meikauth->auth				= $auth_plugin;
		$meikauth->auth_plug_name	= $class_plugin;
		
		if($auth_valid !== "true") {
			if($meikauth->check_backend($backend)) {
				$backend_obj	= $meikauth->activate_backend();
				$config			= $backend_obj->get_conf_keys();
				$conf_desc		= $backend_obj->get_conf_keys_desc();
				
				$codaz = '
	<form action="admin.php?manager=auth-set-valid" method="post" class="ext-form" >
		<fieldset class="field-set-auth">
			<legend class="msg-info">' . _("New backend configuration") . '</legend>';
				
				if(count($config) > 0) {
					$codaz .= '
				<label>' . _("You now have the ability to set critical configuration for you newly choosen authentication backend. Please pay attention to what you're doing, as there is no verification done.<br />Refer to the authentication backend manual for explantion of each configuration key."). '</label>
				<table class="auth active">
					<thead>
						<tr>
							<th>' . _("Configuration key") . '</th>
							<th>' . _("Key value") . '</th>
							<th>' . _("Key description") . '</th>
						</tr>
					</thead>
					<tbody>
';
						
						foreach($config as $key) {
							$codaz .= '
						<tr>
							<td>' . $key . '</td>
							<td><input type="input" name="keys[' . $key. ']" size="64" /></td>
							<td>' . $conf_desc[$key] . '</td>
						</tr>';
						}
						
					$codaz .= '
					</tbody>
				</table>
';
					
				} else {
					$codaz .= '
				<label>' . _("How lucky you are. There's no configuration needed for this authentication backend. Just confirm this form and maybe it'll work."). '</label>
';
				}
				
				$codaz .= '<br />
			<input type="submit" name="valid" value="' . _("Generate new configuration.") . '" />
			<input type="hidden" name="valid_new_backend" value="true" />
			<input type="hidden" name="module" value="' . urlencode($auth_plugin) . '" />
			<input type="hidden" name="class"  value="' . urlencode($class_plugin) . '" />
		</fieldset>
	</form>';
				return $codaz;
			} else {
				error(_("Not a valid authentication backend."));
			}
		} else {
			$auth_plugin	= urldecode(getvar("module"));
			$class_plugin	= urldecode(getvar("class"));
			$keys			= getvar("keys");
						
			if($meikauth->check_backend($backend)) {
				$backend_obj	= $meikauth->activate_backend();
				$config			= $backend_obj->get_conf_keys();
				$config_count	= count($config);
				
				if($config_count > 0) {
					if(count($keys) != $config_count) {
						error(_("Configuration keys size mismatch."));
					} else {
						foreach($config as $key) {
							if(!array_key_exists($key, $keys)) {
								error(_("Needed key is lacking. Was searching for : ") . $key);
								break;
							}
						}
						$conf = serialize($keys);
						$dumpconf = '$serialized_conf = "' . addslashes($conf) . '";
';
					}
				} else {
					$dumpconf = '';
				}
														
				$newfile = '<?php
/* $Id$ */
/**
* Copyright (c) <2006> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
* associated documentation files (the "Software"), to deal in the Software without restriction, including 
* without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
* copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
* following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
* LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
* NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

$auth			= "' . $auth_plugin . '";
$auth_plug_name	= "' . $class_plugin . '";
' . $dumpconf. '
?>';
				if(file_put_contents('setauth.inc.php', $newfile)) {
					echo '<p class="msg-error big">' . _("Perfect, you new configration has been written. Click the link above to disconnect, and then you will be able to reconnect with : ")  . $class_plugin . '<br /><br /><a href="meik.index.php?nxt=unauth">' . _("Disconnect") . '</a></p>';
				} else {
					error(_("Error while writing new configuration to setauth.inc.php."));
				}
			} else {
				error(_("Not a valid authentication backend."));
			}
		}
	}
}
?>