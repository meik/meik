<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

// Includes all what's needed.
$list = array(
	"librdf.inc.php", "functions.inc.php", "defines.inc.php", "libsession.inc.php", "libsql.inc.php",
	"libadmin.inc.php", "libauth.inc.php"
	);

foreach($list as $inc)
{
	if(is_readable(dirname(__FILE__) . '/' . $inc))
		include_once($inc);
	else
		die("Cannot load $inc.");
}

unset($list);
unset($inc);

// don't make XML fails !
ini_set("short_open_tag", "Off");

// Create the SQL Object
$rsql    = new SQL();
if(is_object($rsql)
&& method_exists($rsql, "connect")    && method_exists($rsql, "query") && method_exists($rsql, "mysqlerr") && method_exists($rsql, "checkMySQL")
&& method_exists($rsql, "totalcount") && method_exists($rsql, "disconnect")) {
	$inc = "config.inc.php";
	if(is_readable(dirname(__FILE__) . '/' . $inc))
		include_once($inc);
	else
		die("Cannot load $inc.");
}
else
	die("Error while creating MySQL object.");

// Play with languages !
$inc = "liblang.inc.php";
if(is_readable(dirname(__FILE__) . '/' . $inc))
	include_once($inc);
else
	die("Cannot load $inc.");
$language = new Lang();

// Start session handler.
$session = new Session();

/**
 * This is the MEIK Class. It's the main MEIK class.
 * All MEIK's relative features are within this class, from
 * MySQL Queries to Admin backend.
 * 
 * Here are the public params for the class :
 * @param $start      From which should start the SQL request ? Used with LIMIT ...
 * @param $xpireldir  The directory where XPIs lives, under MEIK directory.
 * @param $dir        It's the public copy of $this->xpidir
 * @param $debug      Do we activate or not the debug mode ?
 * @param $auth       Authentication Backend used.
 * @param $lang       The selected language.
 * @param $backend    The object refering to the selected authentication backend.
 * @param $base       The base directory where MEIK lives.
 * @param $xpidir     The full path of directory where XPIs are living.
 * @param $rdf        The install.rdf content, from an XPI.
 * @param $nombre     Limit of per page admin contents to show. Default is 30.
 * @param $liste      An array of Users ID mapped to the UUID of extensions that were updated.
 * @param $usermails  An array of Users ID mapped to their mail address.
 * 
 * @author Lissyx
 * @copyright Copyright &copy; 2005, Lissyx
 * 
 */

class Meik {
	public $start     = 0;       // starts of SQL limits
	public $xpireldir = "xpis/"; // relative dir of XPIs...
	public $dir       = null;    // public copy of $this->xpidir;
	public $debug     = false;   // active ou non le mode debug
	public $lang      = null;    // selected language
	public $auth      = null;    // Authentication Backend.
	public $backend   = null;    // object of the authentication backend.
	public $nombre    = 30;      // number per page (admin)
	public $xpidir    = null;    // the full path of XPIs dir. made with __construct();
	public $base      = null;    // base directory of the application
	public $rdf;                 // The install.rdf !
	public $liste     = array(); // array of users/UUID that where updated. one user maps to several uuid
	public $usermails = array(); // array of uid mapped to mail
	public $language  = null;    // Just access the language object
	public $meikauth  = null;    // Meik Authentication Backend !
	
	/**
	 * Initiate the MEIK MEIK !
	 * We'll fill $this->base, $this->xpidir, $this->dir
	 * 
	 * We also take time to select language. So error message are
	 * localizable.
	 * */
	function __construct()
	{
		/* It's some special stuff ;) */
		$this->language = $GLOBALS["language"];
		header("X-Special-Tribute: Roxane, a.k.a. Alexandra.");
		$this->base   = dirname($_SERVER["SCRIPT_FILENAME"]);
		$this->xpidir = $this->base.'/'.$this->xpireldir;
		$this->dir    = $this->xpidir;
		$this->language->intl(); // Launch language select !
	}
	
	/**
	 * This is DEPRECATED.
	 * Only a wrapper for SQL::query
	 * 
	 * @param $sql The MySQL request to executes.
	 * 
	 * @return An associating array if we got something interesting.
	 */
	// Executes a MySQL query 
	function query($sql)
	{
		return $GLOBALS["rsql"]->query($sql);
	}
	
	/**
	 * We'll open the XPI package (it's a PKZip archive), and retrieve install.rdf
	 * 
	 * @param $name The XPI filename
	 * @param $dir  The directory where the XPI lives. Optionnal. Default is empty, meaning we try within $this->xpidir;
	 * 
	 * @return The install.rdf content.
	 */
	 
	function extractXPI($name, $dir = "")
	{
		if(empty($dir)) { $dir = $this->xpidir; } // Ok, defaulting ...
		$zipname = $dir.$name;
		// debug("opening $zipname");
		$zip = zip_open($zipname); // Opening ...
		
		if($zip) { // Ok, let's go.
			while( ($z = zip_read($zip)) !== FALSE ) { // We loop on all files
				if(zip_entry_name($z) == "install.rdf") { // SPOTTED !
					if(zip_entry_open($zip, $z, "r")) {
						$data = zip_entry_read($z, zip_entry_filesize($z)); // get data.
					}
				}
			}
			
			zip_close($zip);
			
			$this->rdf[$name] = $data; // Set the $this->rdf variable. Notice we can store more than one rdf.
		} else {
			$data = false;
		}

		return $data; // bye !
	}
	
	/**
	 * Ugly.
	 * 
	 * Simply return the content of $this->rdf
	 */
	function get_install_rdf() // to run AFTER extractXPI
	{
		return $this->rdf;
	}
	
	/**
	 * Lists all XPI which are in $dir (default is $this->xpidir)
	 * 
	 * @param $dir The directory to parse. Default (empty) mean $this->xpidir.
	 * 
	 * @return An array, with some informations extracted from XPI.
	 * 
	 * See example below :
	 <pre>
	[1] => Array
        (
            [version] => 0.2.1.028
            [extension] => SessionSaver .2
            [description] => Magically restores your last browsing session.
            [uuid] => 909409b9-2e3b-4682-a5d1-71ca80a76456
            [compat] => Array
                (
                    [ec8030f7-c20a-464f-9b0e-13a3a9e97384] => 0.9:1.0.4
                    [86c18b42-e466-45a9-ae7a-9b95ba6f5640] => 1.0:1.8
                )

            [time] => 1123602244
            [updateURL] => http://adblock.mozdev.org/update.rdf
            [entry] => sessionsaver_.2-0.2.1.028-fx+mz.xpi
        )
     </pre>
     Note about the array :
      - "version"     : This is the version of the extension.
      - "extension"   : The name of the extension.
      - "description" : Optionnal short description of the extension.
      - "uuid"        : The ID of the extension, used to identify it.
      - "compat"      : An array, in which each target application has a key named with its UUID, associated to a string : "a:b" where a and b are version number defining compatibility.
      - "time"        : The timestamp of XPI.
      - "updateURL"   : The URL where we can find a RDF file to be aware of updates.
      - "entry"       : The XPI filename.
	 */
	function listPackages($dir = "")
	{
		$packList = array(); // initialize the array.
		if(empty($dir)) { $dir = $this->xpidir; }
		$k = 0;
		if(false !== ($d = @dir($dir))) {
			while (false !== ($entry = $d->read())) { // List all the files.
				if(strstr($entry, ".xpi")) { // It's an XPI ! Might be better to check for MIME type ?
					debug("Found one XPI : $entry");
					$datas = $this->extractXPI($entry, $dir); // Get install.rdf datas.
					if(strlen($datas) > 0) {
						$infos = $this->getPackageInfos($datas, $entry, $dir); // We parse the install.rdf !
					} else {
						$infos["version"] = -1;
						$infos["extension"] = -1;
						$infos["description"] = null;
						$infos["uuid"] = -1;
						$infos["compat"] = -1;
						$infos["time"] = filemtime($dir.$entry);
						// it's default values, if no install.rdf was found.
						// here for backward compatibility with extension that only provides install.js
					}
					$infos["entry"] = $entry;
					$packList[] = $infos;
				}
			}
			$d->close();
		} else {
			error(_("Unable to open directory") . ' : ' . $dir);
		}
		
		return $packList; // Give the datas !
	}
	
	/**
	 * We want to parse the install.rdf to retrieve informations.
	 * 
	 * @param $data The install.rdf itself !
	 * @param $entry The filename of the XPI.
	 * 
	 * @return An array, with some of the datas that are returned by $this->listPackages();
	 */
	function getPackageInfos($data, $entry, $dir)
	{
		$xml = new MofoRDF($data);
		$xml->rdf_set_type("MOFO_INSTALL_RDF");
		$res = $xml->get_array();
		
		//$res = parse_install_manifest($data); // Parse the install.rdf. Thanks to UMO.
		// print_r($res);
		if(empty($dir)) { $dir = $this->xpidir; }
		//print_r($res);
		
		$infos = array();
		$infos["version"] = $res["version"];
		$infos["extension"] = $res["name"];
		$infos["description"] = $res["description"];
		$infos["uuid"] = $this->extractfrombrackets($res["id"]); // Get uid from {uid}
		$infos["compat"] = $this->getcompat($res["targetApplication"]);
		$infos["time"] = filemtime($dir.$entry);
		$infos["updateURL"] = $res["updateURL"];
		
		return $infos;
	}
	
	/**
	 * We'll create some HTML used for the final installation step.
	 * 
	 * @param $packList An array, with all data :)
	 * 
	 * @return Some HTML code.
	 * 
	 */
	function createHTML($packList)
	{
		/**
		 The array is using this kind of pattern :
		 <pre>
	Array
	(
		[id] => 5
		[meik_profiles_data_id] => 0
		[meik_exts_vers_id] => 0
		[meik_target_id] => 0
		[uuid] => 73a6fe31-595d-460b-a920-fcc0f8843232
		[timestamp] => 1118957144
		[xpi] => noscript-1.0.9-fx+mz.xpi
		[name] => NoScript
		[version] => 1.0.9
	)
		 </pre>
		*/
		$id = 0;
		if(is_array($packList)) { // Check we have an array.
			$html = '	<ul class="install-extensions">
';
			foreach($packList as $file => $package) {
				$file = $this->xpireldir.$package["xpi"]; // create URL for XPI.
				$deps = $this->getdependances($package["uuid"]); // search for all applications
				$dependances = $this->formatdependances($deps); // just display the target applications found.
				$html .= '		<li class="extension"><input type="checkbox" id="ext' . $id . '" name="' . $package["name"] . '" value="' . $file . '" checked="checked" /> '.$package["name"].' v'.$package["version"].' : '.$dependances.'</li>
	'; // Main code. This is a list element.
				$id++;
			}
			$html .= '	</ul>';
		} else {
			$html = '
<p class="error">'._("No extension found for this profile.").'</p>'; // ':( So bad ...
		}
		return $html;
	}
	
	/**
	 * We compute some HTML to display target application on which an extension can be used.
	 * 
	 * @param $deps An array with all target application related data
	 * 
	 * $deps is an array, prepared by this->getdependances(). It looks like :
	<pre>
	Array
	(
		[0] => Array
			(
				[target] => Mozilla Firefox
				[min] => 1.0
				[max] => 1.0+
			)
	
		[1] => Array
			(
				[target] => Mozilla Suite
				[min] => 1.7
				[max] => 1.8
			)
	
	)
	</pre>
	 * 
	 * @return Some HTML code.
	 **/
	function formatdependances($deps)
	{
		$result = null;
		
		if(is_array($deps)) {
			foreach($deps as $depend) { // We loop on each application
				$nom = $depend["target"];
				$min = $depend["min"];
				$max = $depend["max"];
				
				$result .= '<span class="nom-appli">'.$nom.'</span>, v<span class="min">'.$min.'</span>-<span class="max">'.$max.'</span>; '; // and simply format it.
			}
		}
		
		return $result;
	}
	
	/**
	 * Simply get data which is between brackets. Pretty easy.
	 * 
	 * @param $data Some string like "{404c1575-b9fa-4a48-ac43-6e479955459f}"
	 * 
	 * @return A string like "404c1575-b9fa-4a48-ac43-6e479955459f"
	 */
	function extractfrombrackets($data)
	{
		$res = null;
		preg_match("/\{([^\"]+)\}/i", $data, $res); // regex are so powerful ;)
		return strtolower($res[1]); // UUID is not case sensitive afair.
	}
	
	/**
	 * Prepare a compatibility Array, used to notify the min and max version of
	 * each target application, within the final installation step.
	 * 
	 * @param $datarray An array, looking like :
	 <pre>
	 Array
	 	(
 		[{ec8030f7-c20a-464f-9b0e-13a3a9e97384}] => Array
 			(
 				[minVersion] => 0.9
 				[maxVersion] => 1.0.4
 			)
 		[{86c18b42-e466-45a9-ae7a-9b95ba6f5640}] => Array
 			(
 				[minVersion] => 1.0
 				[maxVersion] => 1.8
 			)
	 	)
	 </pre>
	 * 
	 * @return another array ;)
	 <pre>
	 Array
        (
            [ec8030f7-c20a-464f-9b0e-13a3a9e97384] => 0.9:1.0.4
            [86c18b42-e466-45a9-ae7a-9b95ba6f5640] => 1.0:1.8
        )
	 </pre>
	 * 
	 */
	function getcompat($datarray)
	{
		$newarray = array();
		$i = 0;
				
		if(is_array($datarray)) {
			foreach($datarray as $node => $leaf) { // Loop on each application.
				$min = $leaf["minVersion"];
				$max = $leaf["maxVersion"];
				$leaf = $min.":".$max;
				$newarray[$this->extractfrombrackets($node)] = $leaf; // prepare the new format
				$i++;
			}
		}
		return $newarray; // and return it.
	}
	
	/**
	 * Send inscription confirmation email.
	 * 
	 * @param $dest The user's email ;)
	 * @param $login The user's login
	 * @param $pass The user's password
	 * @param $nom The user's real name.
	 */
	function sendemail($dest, $login, $pass, $nom)
	{
		$subj = _("MEIK subscribtion");
		$message = sprintf(_("\nThanks for subscribing to MEIK. Here is a summary of your informations : \n\n----------\nLogin: '%s'\nPassword: '%s'\nName: '%s'\n----------\n\nKeep preciously those informations, as we can't give you them back."), $login, $pass, $nom);
		mail($dest, $subj, $message, "X-Special-Tribute: Roxane, a.k.a. Alexandra.");
                                     // It's only because she deserves it.
	}
	
	/**
	 * We want to get all the profiles of one user.
	 * 
	 * @param $id The user's ID.
	 * 
	 * @return A basic array containing all the profile's ID.
	 */
	function get_user_profiles($id)
	{
		// Get all the profiles associated to $id
		debug("Trying to get profiles for user id $id");
		$sql = "SELECT `id`, `name` FROM `meik_profiles` WHERE `user_id` = '$id'"; // Get the data.
		$result = $this->query($sql);
		debug("Profiles : ");
		if($this->debug){
			echo "<pre>";
			print_r($result);
			echo "</pre>";
		}
		return $result; // return it. easy.
	}
	
	/**
	 * One again, we just want to get extension which are in a profile.
	 * 
	 * @param $id The profile's ID.
	 * 
	 * @return A basic array with all the extension's ID.
	 */
	function get_profile_exts($id)
	{
		// Get the content of the profile $id
		debug("Trying to get profile $id content.");
		$sql = "SELECT `meik_ext_id` FROM `meik_profiles_data` WHERE `meik_profile_id` = '$id'";
		$result = $this->query($sql);
		$res = array();
		if(is_array($result)) {
			foreach($result as $element) {
				$res[] = $element["meik_ext_id"]; // Just reformat it.
				/** 
				 * @todo Is this really needed ? Check for, and check also for performances impact.
				 * */
			}
		} /* else {
			echo "No extension found.";
		} */
		debug("Extensions ids for this profile : ");
		if($this->debug){
			echo "<pre>";
			print_r($res);
			echo "</pre>";
		}
		return $res;
	}
	
	/**
	 * Retrieve extension's informations.
	 * 
	 * @param $id The extension's ID.
	 * 
	 * @return The first array of the whole MySQL result.
	 */
	function get_ext_infos($id)
	{
		// Retrieve all informations about extension $id
		debug("Trying to find extension id $id");
		$sql = "SELECT * FROM `meik_exts` WHERE `id` = '$id' LIMIT 1;";
		$result = $this->query($sql);
		debug("Extension informations : ");
		if($this->debug){
			echo "<pre>";
			print_r($result[0]);
			echo "</pre>";
		}
		return $result[0];
	}
	
	/**
	 * Retrieve user's informations
	 * 
	 * @param $id The user's ID.
	 * 
	 * @return The first array of the whole MySQL result.
	 */
	function get_user_infos($id)
	{
		// Retrieve all informations about user $id
		debug("Trying to find user id $id");
		$sql = "SELECT * FROM `meik_users` WHERE `id` = '$id' LIMIT 1;";
		$result = $this->query($sql);
		debug("User informations : ");
		if($this->debug){
			echo "<pre>";
			print_r($result);
			echo "</pre>";
		}
		return $result[0];
	}
	
	/**
	 * Get all the extensions, ordered ascending by name.
	 * 
	 * @return a basical array, which each node containing :
	 * - id, the extension ID.
	 * - name, the extension name.
	 * - version, the extension version.
	 */
	function get_all_ext()
	{
		$sql = "SELECT `id`, `name`, `version` FROM `meik_exts` ORDER BY `name` ASC;";
		$result = $this->query($sql);
		if($this->debug) {
			echo "<pre>";
			print_r($result);
			echo "</pre>";
		}
		return $result;
	}
	
	/**
	 * We want to add an extension into the database. It manages itself wether it's uptodate or not.
	 * 
	 * @param $infos An array whith all the informations needed.
	 * @see $this->getPackageInfos() for the array format.
	 */
	function add_ext($infos)
	{
		$version     = $infos["version"];
		$name        = $infos["extension"];
		$xpi         = $infos["entry"];
		$timestamp   = $infos["time"];
		$uuid        = $infos["uuid"];
		$compat      = $infos["compat"];
		$updateurl   = $infos["updateURL"];
		$description = mysql_real_escape_string($infos["description"], $GLOBALS["rsql"]->sqlconn); // Protect ..
		
		if(	 empty($uuid) 
		  || empty($version)
		  || empty($compat)
		  || empty($name)
		  ) { // all seems not to be filled.
		  // "Un vous manque et tout est depeuple ..."
			warning(_("This extension's install.rdf file is missing one or more of the required property reference. Please check for this property at ") . "http://developer.mozilla.org/en/docs/install.rdf#Required_Property_Reference <br />" . _("Extension file is : ") . $xpi);
			return false;
		}
		
		/* First we and Extension's information. */
		
		/**
		 * @todo Might be more coherent to switch uptodate check and presence check.
		 * Presence before up to date ...
		 */
		if(!$this->isinfoup2date($uuid, $timestamp, $updateurl)) { // Is this up to date data ?
			if($this->isExtInfosPresent($uuid)) { // if it's not up to date, maybe it's not present ...
				$sql_upd_ext = "UPDATE `meik_exts` SET `timestamp` = '$timestamp', `xpi` = '$xpi', `name` = '$name', `version` = '$version', `description` = '$description', `updateurl` = '$updateurl' WHERE `uuid` = '$uuid' LIMIT 1;";
				if($this->query($sql_upd_ext)) {
					info("Update of " . $this->resolveuuid($uuid, "ext") ." was successfull."); // Ok, it's updated.
					$this->prepare_mail_updates($uuid); // Notice those who want to be
				} else {
					info("Error while updating " . $this->resolveuuid($uuid, "ext") .".");
				}
			} else {
				$sql_add_ext = "INSERT INTO `meik_exts` (`id`, `meik_profiles_data_id`, `meik_exts_vers_id`, `meik_target_id`, `uuid`, `timestamp`, `xpi`, `name`, `version`, `description`, `updateurl`) VALUES ('', '0', '0', '0', '$uuid', '$timestamp', '$xpi', '$name', '$version', '$description', '$updateurl');";
				// we add ...
				if($this->query($sql_add_ext) ) {
					info("Extensions informations were added : '$uuid', '$timestamp', '$xpi', '$name', '$version' ('uuid', 'timestamp', 'xpi', 'name', 'version')");
				} else {
					info("Error while adding extension informations, aborting ... Maybe due to an already existing record.");
				}
			}
		} else {
			info("Extension's informations (" . $this->resolveuuid($uuid, "ext") .") are already up to date, no modification was done on extension informations.");
			// No need to update !
		}
		
		/* Now, prepare for link with applications */
		
		/**
		 * @todo Might be more coherent to switch uptodate check and presence check.
		 * Presence before up to date ...
		 */
		
		if(is_array($compat)) {
			foreach($compat as $uuid_cible => $vers) {
				$vers = explode(":", $vers);
					$min = $vers[0];
					$max = $vers[1];
				if(!$this->isdataup2date($uuid, $min, $max)) { // check if data is up-to-date
					if($this->isExtDatasPresent($uuid, $uuid_cible)) { // is it present ?
						$sql_upd_vers = "UPDATE `meik_exts_vers` SET `minver` = '$min', `maxver` = '$max' WHERE `uuid_ext` = '$uuid' LIMIT 1;";
						if($this->query($sql_upd_vers)) {
							info("Update of " . $this->resolveuuid($uuid, "ext") ."'s data for " . $this->resolveuuid($uuid_cible, "app") ." was successfull.");
						} else {
							info("Cannot update datas for " . $this->resolveuuid($uuid, "ext") .".");
						}
					} else {
						$sql_add_vers = "INSERT INTO `meik_exts_vers` (`id`, `uuid_ext`, `uuid_appli`, `minver`, `maxver`) VALUES ('', '$uuid', '$uuid_cible', '$min', '$max');";
						if($this->query($sql_add_vers)) {
							info("Informations added : (`id`, `uuid_ext`, `uuid_appli`, `minver`, `maxver`) VALUES ('', '$uuid', '$uuid_cible', '$min', '$max');");
						} else {
							info("Error while adding extension versions of target " . $this->resolveuuid($uuid_cible, "app"));
						}
					}
				} else {
					info("Extension's datas (" . $this->resolveuuid($uuid, "ext") .") are already up to date, no modification was done on extension data.");
				}
			}
		} else {
			error("Some strange behavior : Compat is not an array.");
		}
	}
	
	/**
	 * Check if extension's informations are present.
	 * 
	 * @param $uuid The extension's UUID.
	 * 
	 * @return true or false, wether it is or not present.
	 */
	function isExtInfosPresent($uuid)
	{
		// check if an extension is already added !
		$test = "SELECT `id` FROM `meik_exts` WHERE `uuid` = '$uuid' LIMIT 1;";
		return is_array($this->query($test));
	}

	/**
	 * Check if extension's data are up-to-date, regarding to a target application.
	 * 
	 * @param $uuid The extension's UUID.
	 * @param $uuid_cible The target application UUID.
	 * 
	 * @return true or false, wether it is or not up-to-date.
	 */
	function isExtDatasPresent($uuid, $uuid_cible)
	{
		// check if extension's data are already presents !
		$test = "SELECT `id` FROM `meik_exts_vers` WHERE `uuid_ext` = '$uuid' AND `uuid_appli` = '$uuid_cible' LIMIT 1;";
		return is_array($this->query($test));
	}
	
	/**
	 * Check if extension's informations are up-to-date, (check for timestamp, and updateURL).
	 * 
	 * @param $uuid The extension's UUID.
	 * @param $timestamp The timestamp.
	 * @param $updateurl The updateURL provided by the install.rdf
	 * 
	 * @return true or false, wether it is or not up-to-date.
	 */
	function isinfoup2date($uuid, $timestamp, $updateurl)
	{
		// we only compare with timestamp files !
		$test = "SELECT `id` FROM `meik_exts` WHERE `uuid` = '$uuid' AND `timestamp` = '$timestamp' AND `updateurl` = '$updateurl' LIMIT 1;";
		return is_array($this->query($test));
	}
	
	/**
	 * Check if extension's data are up-to-date (meaning, check for target version, target
	 * application, etc.)
	 * 
	 * @param $uuid The extension uuid
	 * @param $min The minimal version of target.
	 * @param $max The maximal version of target.
	 * 
	 * @return true or false, wether something has been found or not.
	 */
	function isdataup2date($uuid, $min, $max)
	{
		$test = "SELECT `id` FROM `meik_exts_vers` WHERE `uuid_ext` = '$uuid' AND `minver` = '$min' AND `maxver` = '$max' LIMIT 1;";
		return is_array($this->query($test));
	}
	
	/**
	 * This is used to get the list of users which have a specific extension in their profile.
	 * 
	 * @param $uuid The extension's UUID we want to find.
	 * 
	 * @return An array with all the profile's identifier.
	 */
	function get_users_exts($uuid)
	{
		// we want to have the list of users that have the extension with uuid $uuid.
		$extid = $this->getextidfromuuid($uuid);
		if($extid) {
			debug("Got extension ID for UUID $uuid : $extid");
			$req = "SELECT `meik_profile_id` FROM `meik_profiles_data` WHERE `meik_ext_id` = '$extid'";
			$res = $this->query($req);
			if($res) {
				// $meik->debug = true;
				return $res;
			}
		}
	}
	
	/**
	 * We want to create the list of users who wants to be notifed, and whose extension
	 * have been updated.
	 * 
	 * @param $uuid The updated extension's UUID.
	 * 
	 * This returns nothing but it fills the $this->liste array, where each node
	 * is an array containing each uuid of extension which were updated.
	 */
	function make_users_exts_liste($uuid)
	{
		$res = $this->get_users_exts($uuid);
		if($res) {
			foreach($res as $el) {
				$id = $this->get_userid_from_profile($el["meik_profile_id"]);
				$notify = $this->user_want_notify($id);
				if($id && $notify) {
					$this->liste[$id][] = $uuid;
				}
			}
		}
	}
	
	/**
	 * We want to resolve user's ID associated to a profile ID.
	 * 
	 * @param $pid The profile ID we want to resolve.
	 * 
	 * @return An integer, which is the user's id.
	 */
	function get_userid_from_profile($pid)
	{
		// we want user id of profile id $pid
		// debug("Getting user_id for profile $pid");
		$req = "SELECT `user_id` FROM `meik_profiles` WHERE `id` = '$pid' LIMIT 1;";
		$res = $this->query($req);
		$res = $res[0]["user_id"];
		// debug("Got id : " . $res);
		return $res;
	}
	
	/**
	 * We know which users to send mail to, so go for their mail !
	 * 
	 * @param $uuid The extension we want to notify update.
	 * 
	 * This functions returns nothing, but it fills the $this->usermails array,
	 * where each node is a key with user's ID, and has the user's email as value.
	 */
	function prepare_mail_updates($uuid)
	{
		$this->make_users_exts_liste($uuid);
		$this->liste = $this->clean_duplicates($this->liste);
		if(is_array($this->liste)) {
			foreach($this->liste as $uid => $exts) {
				$this->usermails[$uid] = $this->get_user_email($uid);
			}
		} else {
			warning(_("Cannot prepare mail for extension UUID ") . $uuid );
		}
	}
	
	/**
	 * Do this user whants to be notified of future updates of it's extensions ?
	 * 
	 * @param $uid The user's ID.
	 * 
	 * @return true or false, wether we should or not notify him. 
	 */
	function user_want_notify($uid)
	{
		$wants = "SELECT `notifs` FROM `meik_users` WHERE `id` = '$uid' LIMIT 1;";
		$wants = $this->query($wants);
		$wants = ($wants[0]["notifs"] == "1") ? true: false;
		return $wants;
	}
	
	/**
	 * Just retrieve user's email.
	 * 
	 * @param $uid The user's ID.
	 * 
	 * @return The user's email.
	 */
	function get_user_email($uid)
	{
		$mail = "SELECT `email` FROM `meik_users` WHERE `id` = '$uid' LIMIT 1;";
		$mail = $this->query($mail);
		return $mail[0]["email"];
	}
	
	/**
	 * So, here we go. It's the main emailing update function, which will
	 * send each user a mail with all extension that were just updated.
	 */
	function mail_updates()
	{
		$msg = null;
		if(is_array($this->liste)) { 
			foreach($this->liste as $uid => $extensions) {
				$email = $this->usermails[$uid];
				$msg = sprintf("\n" . _("Here are extension you subscribed to, and which have been updated in MEIK : ") . "\n\n\n");
				foreach($extensions as $extension) {
					$nom = $this->resolveuuid($extension, "ext");
					$new = $this->extver($extension);
					$msg .= sprintf("---\n" . _("Extension : %s (%s)\nUpdated to %s\n---\n\n"), $nom, $extension, $new);
				}
			
			$msg .= sprintf("\n----------------------------\n%s\n", _("You requested to be aware of each update. You can change this within your MEIK's account."));
			$send .= mail($email, _("[MEIK] - Extensions update notification"), $msg, "X-Special-Tribute: Roxane, a.k.a. Alexandra.\nFrom: meik@localhost");
				if($send) {
					echo '<p class="info-email">'._("Sending notification mail to").' '.$email.'.</p>';
				}
			}
		} else {
			warning(_("No extension list."));
		}
		
	}
	
	/**
	 * We just want to have an array were all elements (regarding of subnodes)
	 * are uniques. Otherwise, as it's used to clean array for updates, user's might
	 * have many times the same extension notified in the same email.
	 * 
	 * @param $array The array to clean.
	 * @return The cleaned array.
	 */
	function clean_duplicates($array)
	{
		if(is_array($array)) {
			foreach($array as $key => $content) {
				$narray[$key] = array_unique($content);
			}
		} else {
			warning(_("Not an array."));
		}
		return $narray;
	}
	
	/**
	 * We want to know the filename of an extension.
	 * 
	 * @param $uuid The extension's UUID we want the filename.
	 * 
	 * @return The filename. Woo, such a strange world.
	 */
	function extfile($uuid)
	{
		$test = "SELECT `xpi` FROM `meik_exts` WHERE `uuid` = '$uuid' LIMIT 1;";
		$res  = $this->query($test);
		return $res[0]["xpi"];
	}
	
	/**
	 * Extension version, that is what we want to know.
	 * 
	 * @param $uuid The extension's UUID we want the filename.
	 * 
	 * @return Maybe the version stored in database ?
	 */
	function extver($uuid)
	{
		$ver = "SELECT `version` FROM `meik_exts` WHERE `uuid` = '$uuid' LIMIT 1;";
		$res  = $this->query($ver);
		return $res[0]["version"];
	}
	
	/**
	 * Try to authenticate the user. Main idea is to let play the authentication backend,
	 * and to verify what we got ;)
	 * 
	 * @param $user The user's login
	 * @param $pass The password user's submitted.
	 * 
	 * @return true or false, wether it looks good or not.
	 */
	function authuser($user, $pass)
	{
		include_once("setauth.inc.php");
		
		$this->auth = $auth; ///< defined in setauth.inc.php;
		$meikauth = new Meik_Auth();
		$meikauth->auth = $this->auth;
		$meikauth->auth_plug_name = $auth_plug_name; ///< defined in setauth.inc.php
		$this->meikauth = $meikauth;
		$backend = $meikauth->activate_backend(); ///< Select the autentication backend !
				
		// Restore configuration.
		if(!empty($serialized_conf)) {
			$conf_backend = unserialize($serialized_conf);
			$backend->set_conf($conf_backend);
		}
		
		$result  = $backend->authuser($user, $pass); ///< Let the backend play ...
				
		if(	is_array($result) ///< Did we receive an array ?
			&& array_key_exists("login", $result) ///< And is it valid ?
			&& array_key_exists("id", $result) ) { ///< ...
				$l10n  = $this->language->chooselang();
				$GLOBALS["session"]->createsession(	$result["login"],
													$result["id"],
													$result["nom"],
													$l10n); ///< YEAH it's good. Oasis (is) good.
				return true;
		} else {
			$GLOBALS["session"]->deletesession($user); ///< Sorry, cya in hell.
			return false;
		}
	}
	
	/**
	 * We want to add the user in MySQL database. Should also be used by authentication backend.
	 * Less code mean less bug (or should. if not try to stop coke.)
	 * 
	 * @param $login The user's name to log in with.
	 * @param $pass  The user's password.
	 * @param $nom   User's real name.
	 * @param $email User's email address.
	 * 
	 * @param $sendmail Should we send the confirmation email ? Default is true. For external authentication backend, it's a good idea to make this "false".
	 * @param $nnative  Is this a NON-native account ? Default is false, HAVE TO BE true for use with external authentication backend.
	 * 
	 * @return true if email have to be sent, and it has been. Nothing otherwise.
	 */
	function adduser2sql($login, $pass, $nom, $email, $sendmail = true, $nnative = false)
	{
		$test = "SELECT * FROM `meik_users` WHERE `login` = '$login' LIMIT 1;";
		if(is_array($this->query($test))) {
			echo _("Cannot add user")." $login, "._("already present.");
		} else {
			$insert = "INSERT INTO `meik_users` (`id`, `meik_profiles_data_id`, `login`, `pass`, `nom`, `email`, `statut`, `nnative`) VALUES ('', '0', '$login', MD5('$pass'), '$nom', '$email', ".MEIK_STATUT_USER.", '$nnative');";
			if($this->query($insert) && $sendmail) {
				$GLOBALS["meik"]->sendemail($email, $login, $pass, $nom);
				echo _("User successfully added. Sending email.").'<br />'._("You are now able to").' <a href="meik.index.php">'._("login").'</a> '._("using the login page.");
			} else if(!$sendmail) {
				return true;
			}
		}
	}
	
	/**
	 * Let the user come with us :)
	 * 
	 * @param $login The user's login. This is what's used for the user to authenticate.
	 * @param $pass  The user's password, also used for authentication.
	 * @param $nom   The user's real name. Optionnal.
	 * @param $email The user's email address. Be careful, this address is tested with some hardcore code.
	 */
	function registeruser($login, $pass, $nom, $email)
	{
		if(!empty($login) && !empty($pass) && !empty($email)) {
			include_once("mail.inc.php");
			$check = new EmailCheck();
			$check->email = $email;
			if($check->checkemail()) {
				$this->adduser2sql($login, $pass, $nom, $email);
			} else {
				echo _("Email seems not to be valid. Cannot continue.");
			}
		} else {
			echo _("User, password or email is empty. Please go back and check.");
		}
	}
	
	/**
	 * Just add a profile for a user.
	 * 
	 * @param $nomp The profile's name we want to add.
	 * @param $user The user ID of user who'll get this profile.
	 * 
	 * @return true or false, wether the profile has been correctly added (or not !).
	 */
	function addprofil($nomp, $user)
	{
		$insert = "INSERT INTO `meik_profiles` (`id`, `meik_users_id`, `meik_users_meik_profiles_data_id`, `meik_profiles_data_id`, `meik_exts_id`, `user_id`, `name`) VALUES ('', '0', '0', '0', '0', '$user', '$nomp')";
		if($this->query($insert)) {
			debug("Profil $nomp added for user $user");
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * We added the profile ? Just delete !
	 * 
	 * @param $ipd The profile ID !
	 * 
	 * @return true or false, wether the profile has been correctly deleted or not.
	 */
	function delprofil($idp)
	{
		$del = "DELETE FROM `meik_profiles_data` WHERE `meik_profile_id` = '$idp';";
		if(!$this->query($del)) {
			debug("There was an error while deleting profile data of profile $idp");
			return false;
		}
		
		$del = "DELETE FROM `meik_profiles` WHERE `id` = '$idp' LIMIT 1;";
		return $this->query($del);
	}
	
	/**
	 * What's the name of this profile ?
	 * 
	 * @param $id The profile ID we want to know the name.
	 * 
	 * @return The name, if it has been found, or false.
	 */
	function getprofilname($id)
	{
		$req = "SELECT `name` FROM `meik_profiles` WHERE `id` = '$id' LIMIT 1;";
		$res = $this->query($req);
		
		if(is_array($res)) {
			return $res[0]["name"];
		} else {
			return false;
		}
	}
	
	/**
	 * Resolve the name of an extension id.
	 * 
	 * @param $id The ID (in mysql database, not UUID) of the extension.
	 * 
	 * @return The name, if found, false otherwise.
	 */
	function getextname($id)
	{
		$req = "SELECT `name` FROM `meik_exts` WHERE `id` = '$id' LIMIT 1;";
		$res = $this->query($req);
		
		if(is_array($res)) {
			return $res[0]["name"];
		} else {
			return false;
		}
	}
	
	/**
	 * Which version do we have ?
	 * 
	 * @param $id The extension ID in MySQL.
	 * 
	 * @return The version string, if found, false otherwise.
	 */
	function getextver($id)
	{
		$req = "SELECT `version` FROM `meik_exts` WHERE `id` = '$id' LIMIT 1;";
		$res = $this->query($req);
		
		if(is_array($res)) {
			return $res[0]["version"];
		} else {
			return false;
		}
	}
	
	/**
	 * Just update the name of a profile.
	 * 
	 * @param $id  The profile ID.
	 * @param $nom The new profile name.
	 * 
	 * @return true or false, wether the request was successfully executed or not.
	 */
	function updateprofil($id, $nom)
	{
		$update = "UPDATE `meik_profiles` SET `name` = '$nom' WHERE `id` = '$id' LIMIT 1;";
		return $this->query($update);
	}
	
	/**
	 * Update the profile's extension list.
	 * 
	 * @param $id    The profile ID to update.
	 * @param $liste An array of extensions ID, which are member of this profile.
	 * 
	 * @return true or false, wether the update was good.
	 */
	function updateprofildata($id, $liste)
	{
		// $id => profil
		// $liste => extensions
		$delete = "DELETE FROM `meik_profiles_data` WHERE `meik_profile_id` = '$id';";
		if($this->query($delete)) {
			debug("All previous extensions from profile $id were deleted. Continuing with addition !");
			$good = 0; // Initiate counter to ensure all was correct.
			foreach($liste as $ext) {
				$insert = "INSERT INTO `meik_profiles_data` (`id`, `meik_users_id`, `meik_profile_id`, `meik_ext_id`) VALUES ('', NULL, '$id', '$ext');";
				if($this->query($insert)) {
					debug("Extention $ext was addedd successfully.");
					$good++; // one more !!
				} else {
					debug("Extension $ext can't be added.");
				}
			}
			if($good == count($liste)) {
				debug("All extensions were added.");
				return true;
			} else {
				debug("Cannot add all the extensions.");
				return false;
			}
		} else {
			debug("Cannot delete all the extensions associated to profile $id. Stopping.");
		}
	}
	
	/**
	 * We want to know all target applications an extension is for.
	 * 
	 * @param $uuid The extension UUID we want to know for which target application it's designed.
	 * 
	 * @return An array, were each node is like :
	 <pre>
	 Array
	 (
	 	[target] => "Nom",
	 	[min] => "min",
	 	[max] => "max",
	 )
	 </pre>
	 Nom is the name of the application, min is it's minimal version and max is it's maximal ...
	 */
	function getdependances($uuid)
	{
		$get = "SELECT * FROM `meik_exts_vers` WHERE `uuid_ext` = '$uuid';";
		$res = $this->query($get);
		if($res) {
			$result = array();
			foreach($res as $dep) {
				$nom = $this->resolveuuid($dep["uuid_appli"], "app");
				$min = $dep["minver"];
				$max = $dep["maxver"];
				$result[] = array("target" => $nom, "min" => $min, "max" => $max);
			}
			return $result;
		} else {
			debug("Cannot find any dependency");
		}
	}
	
	/**
	 * Resolve an unknown UUID. You have to specify if you want to resolve using Extension table, or Target Application table.
	 * 
	 * @param $uuid The UUID we want to resolve.
	 * @param $type This is the value to define, to choose between application table or extension table. Set it to "app" if you search for an application, or "ext" if your search for an extension.
	 */
	function resolveuuid($uuid, $type)
	{
		// $type = ("app" | "ext");
		switch($type) {
			case "app":
				$req = "SELECT `nom` FROM `meik_target` WHERE `uuid` = '$uuid'";
				$do = $this->query($req);
				return $do[0]["nom"];
			
			case "ext":
				$req = "SELECT `name` FROM `meik_exts` WHERE `uuid` = '$uuid'";
				$do = $this->query($req);
				return $do[0]["name"];
		}
	}
	
	/**
	 * We want the UUID of an extension, we have it's MySQL id.
	 * 
	 * @param $id The MySQL id of the extension.
	 * 
	 * @return The UUID.
	 */
	function getextuuidfromid($id)
	{
		$req = "SELECT `uuid` FROM `meik_exts` WHERE `id` = '$id'";
		$do = $this->query($req);
		return $do[0]["uuid"];
	}
	
	/**
	 * Reverse resolve, we want the MySQL of an extension, knowing it's UUID.
	 * 
	 * @param $uuid The extension UUID, it's the only thing we know.
	 * 
	 * @return The MySQL ID.
	 */
	function getextidfromuuid($uuid)
	{
		$req = "SELECT `id` FROM `meik_exts` WHERE `uuid` = '$uuid'";
		$do = $this->query($req);
		return $do[0]["id"];
	}
	
	/**
	 * We want to be aware of the User's Rank (normal, or admin ?)
	 * 
	 * @param $id The user's ID.
	 * 
	 * @return The Status, an integer.
	 */
	function getuserrank($id)
	{
		$get = "SELECT `statut` FROM `meik_users` WHERE `id` = '$id'";
		$get = $this->query($get);
		return $get[0]["statut"]; // Pretty easy.
	}
		
	/**
	 * Show description. Used with some AJAX :)
	 * 
	 * @return Some uggly HTML code :)
	 */
	function showdesc()
	{
		$html = '
	<div id="description">
		<div id="desc-error"></div>
		<table id="desc-success">
			<tr>
				<th id="titre-desc" colspan="2">'._("Extension's information").'</th>
			</tr>
			<tr>
				<td id="titre-desc-name">'._("Name").' : </td>
				<td id="desc-name"></td>
			</tr>
			<tr>
				<td id="titre-desc-vers">'._("Version").' : </td>
				<td id="desc-vers"></td>
			</tr>
			<tr>
				<td id="titre-desc-desc">'._("Short description").' : </td>
				<td id="desc-desc"></td>
			</tr>
		</table>
	</div>
';
		return $html;
	}
	
	/**
	 * Send notification to the administrator, about submission or extension's update.
	 * 
	 * @param $filelist The extnesion list, as an array were each node contain the extension name.
	 * @param $user     The user name which made the action.
	 * @param $type     The action type. 'sub' stands for submission, and 'not' for .. notification. Default is 'sub'.
	 * @return True or false, wether an error hasn't occured or not.
	 */
	function notice_admin($filelist, $user, $type = "sub") // notice the admins of a submission and/or an update notif
	{
		$err = false;
		if(is_array($filelist)) { // Check it's valid.
			$admins = "SELECT `email`, `nom` FROM `meik_users` WHERE `statut` = '".MEIK_STATUT_ADMIN."'";
			$admins = $this->query($admins);
			
			if($type == "sub") { // Submission !
				$msg = sprintf("\n" . _("User %s has just submit the following extensions :") . "\n-----", $user);
			} elseif ($type == "not") { // Notification ...
				$msg = sprintf("\n" . _("User %s has just submit update of the following extensions :") . "\n-----", $user);
			}
			
			foreach($filelist as $ext) {
				$msg .= sprintf("\n\t+ %s", $ext);
			}
			
			$msg .= sprintf("\n-----\n" . _("You can validate or delete them using the administration interface.") . "\n");
			
			foreach($admins as $admin) { // Make a loop on the admins' emails.
				$send = mail($admin["nom"] . " <".$admin["email"].">", _("[MEIK] Extension submit"), $msg, "X-Special-Tribute: Roxane, a.k.a. Alexandra.\nFrom: meik@localhost");
				if($send) {
					echo _("Submission email was sent.");
				} else {
					$err = true;
				}
			}
		}
		
		return !$err; // If no error, return true.
	}
	
	/**
	 * We want to download some files.
	 * 
	 * @param $urls An URL list, wether an array or simply a string where each url is seperated using a linebreak '\n'.
	 * @param $dest The destination directory.
	 * @param $liste The array where will be placed all filenames that were successfully copied. IT'S PASSED AS REFRENCE. (check http://fr2.php.net/manual/en/language.references.php).
	 * @param $silent Boolean : do we signal problems ? Default is false.
	 * 
	 * @return true or false, wether at least one file successfully copied. 
	 */
	function retrievefiles($urls, $dest, &$liste, $silent = false) // $urls : suites d'url s�par�es par \n, ou tableau
	{
		if(!is_array($urls)) { // Is it an array ? No ? Make it an array.
			$urls = explode("\n", $urls);
		}
		
		debug("Got " . count($urls) . " URLs.<br />");
		
		if(!is_dir($dest)) {
			if(!@mkdir($dest)) { // Create the destination directory if it doesn't exists.
				error(_("Unable to create directory") . ' : ' . $dest . ' . '. _("Please check permissions."));
			}
		}
		
		$files = array(); // initializing variable is cleaner :)
		if(is_array($urls)) {
			foreach($urls as $key => $url) { // Loop on each URL.
				$url = trim($url);
				$content = @file_get_contents($url);
				if(false !== $content) { // Did we downlaod it ?
					$name = basename($url); // Take the filename.
					if($name) {
						$file = fopen($dest.$name, "w"); // do copy.
						if(fwrite($file, $content) && !$silent) {
							echo _("Successfully downloaded")." <b>$url</b> "._("to")." <u>'$dest$name'</u>.<br />";
							$files[$key] = $name; // Add to the successfully copied list.
							fclose($file);
						}
					}
				} else { // An error happened.
					if(!$silent) { // And we're allowed to tell so.
						echo _("There was a problem trying to download").' : "'.$url.'". '._("Please check this address and try again.").'<br />';
					}
				}
			}
			
			$liste = $files; // Set the copied files list.
		}

		return (@count($files) > 0); // Did at least one file has been correctly copied ?
	}
	
	/**
	 * Did we already sent a notification ?
	 * 
	 * @param $id  The extension's ID.
	 * @param $url The URL of update.
	 * 
	 * @return true or false, wether a notification has been added or is already present.
	 */
	function isnotified($id, $url)
	{
		$test = "SELECT `ext_id`, `url` FROM `meik_notifs` WHERE `ext_id` = '$id' LIMIT 1;";
		$test = $this->query($test);
		if(!is_array($test)) { // No notification already present !
			$req = "INSERT INTO `meik_notifs` (`ext_id`, `url`) VALUES ('$id', '$url');";
			$req = $this->query($req); // Add a notification for this URL !
			
			$req = "UPDATE `meik_exts` SET `notif` = '1' WHERE `id` = '$id' LIMIT 1;";
			$req = $this->query($req); // Set notification !
			
			return $req;
		} else {
			return false; // Too bad.
		}
	}
	
	/**
	 * Extension proposal.
	 * 
	 * @param $url The XPI package URL of new extension proposed.
	 * 
	 * @return true or false wether the URL has been correclty added.
	 */
	function submit($url)
	{
		$req = "INSERT INTO `meik_submit` (`url`) VALUES ('$url');";
		$req = $this->query($req);
		return $req;
	}
	
	/**
	 * Compare two versions number, and syat which one is the newest.
	 * 
	 * e.g. 1.0.2 and 0.9.3, we should have the first as newest.
	 * 
	 * @param $ver1 The first version to compare
	 * @param $ver2 The second version to compare
	 * 
	 * @return true, if ver1 > ver2, false otherwise.
	 */
	function comp_ver($ver1, $ver2)
	{
		$a1 = explode(".", $ver1);
		$a2 = explode(".", $ver2);
		
		$digit1 = array_shift($a1);
		$digit2 = array_shift($a2);
		/*
		echo "We got : $ver1, $ver2\n => ";
		echo "Compares $digit1 with $digit2 ...\n";
		*/
		$ver1 = join(".", $a1);
		$ver2 = join(".", $a2);

		if($digit1 > $digit2) {
			// echo "That's ok, $digit1 > $digit2\n";
			return true; // ok it's good.
		} else {
			// echo "a1 :: " . count($a1) . " && a2 :: " . count($a2) . " \n";
			if( (count($a1) >= 1)
			 && (count($a2) >= 1) ) { // we have enough digits to continue.
				// echo "Playing for next round with $ver1, $ver2 ....\n";
				return $this->comp_ver($ver1, $ver2);
			} else {
				// echo "What's about a2 ? " . count($a2) . " \n";
				if(count($a2) == 0) { // ver2 will be empty next round ! ver1 has won !
					return true;
				} else {
					return false;
				}
			}
		}
	}
}

?>