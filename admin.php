<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

$time_start = microtime(true);
include_once("libmeik.inc.php");

$admin = new Admin();
$meik = new Meik();
$meik->debug = false;

$disco = getvar("disco");
if($disco) { header("Location: meik.index.php"); }

$userid = $session->getsessvar("id");
$rank = $meik->getuserrank($userid);

if($rank != "1") {
	header("HTTP/1.1 404 Not Found");
	exit();
}

header('Content-Type: ' . contenttype(). '; charset='.$language->charsetselect());
echo '<?xml version="1.0" encoding="'.$language->charsetselect().'" ?>';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Administration MEIK : <?php echo $session->getsessvar("user"); ?>@meik - <?php echo MEIK_VERSION; ?></title>
	<script type="text/javascript" src="meik.js">
	</script>
	<link rel="stylesheet" type="text/css" media="screen" title="MEIK Default Admin Style" href="meik.admin.css" />
</head>

<body onload="admin_toggle_load();">
<div id="head">
	<?php echo $language->showlangselect(); ?>
	<h1>MEIK : Multi Extension Installer Kit <span class="version">(v<?php echo MEIK_VERSION; ?>)</span></h1><p class="logo-mini-admin"><img src="images/meik-logo-mini-admin.png" alt="Mini MEIK's Logo" /></p>
</div>
<h2><?php echo $session->getsessvar("user"); ?>@meik:/administration# show ||
<?php echo _("Hidden messages :: "); ?>
	<span class="messages-msg-info"    id="messages-msg-info"    ></span>
	<span class="messages-msg-warning" id="messages-msg-warning" ></span>
	<span class="messages-msg-err"     id="messages-msg-err"     ></span>
	<span class="messages-msg-debug"   id="messages-msg-debug"   ></span>

</h2>
<?php	 
echo $session->showadmin() . 
'
<dl class="back">
	<dt><a href="meik.index.php">'._("Back to MEIK").'</a></dt>
</dl>
<div id="content" class="contenu">

';

$man = getvar("manager");
$next = getvar("next");
if($next) {
	$admin->start = $next;
}

switch($man) {
	case "users":
		echo $admin->admin_show_userlist();		
		echo $admin->admin_createlinks("meik_users", $next);
		break;
	
	case "muser":		
		$id        = getvar("id");
		$name      = getvar("login");
		$delete    = getvar("delete");
		$promote   = getvar("promote");
		$unpromote = getvar("unpromote");
		
		if($delete) {
			if($admin->admin_deleteuser($id)) {
				echo '<p class="info-admin">' . $name . ' ' . _("successfully deleted.").'</p>';
			} else {
				echo '<p class="err-admin">'._("Error while deleting user.").'</p>';
			}
		} elseif($promote) {
			if($admin->admin_promote($id)) {
				echo '<p class="info-admin">'. $name . ' ' . _("is now administrator.") .'</p>';
			} else {
				echo '<p class="err-admin">' . _("Error while promoting."). '</p>';
			}
		} elseif($unpromote) {
			if($admin->admin_unpromote($id)) {
				echo '<p class="info-admin">' . $name . ' ' . _("is no more administrator.") .'</p>';
			} else {
				echo '<p class="err-admin">' . _("Error while unpromoting, looks like you have to meet Lucifer."). '</p>';
			}
		} else {
			echo '<p class="error">'._("You should not smoke weed.").'</p>';
		}
		
		echo $admin->admin_show_userlist();
		echo $admin->admin_createlinks("meik_users", $next);
		
		break;
		
	case "exts":
		echo $admin->admin_show_extslist();
		echo $admin->admin_createlinks("meik_exts", $next);
		break;
		
	case "mext":
		$delete    = getvar("delete");
		$id        = getvar("id");
		$name      = getvar("name");
		
		if($delete) {
			if($admin->admin_deleteext($id)) {
				echo '<p class="info-admin">'._("Extension").' ' . $name . ' ' . _("successfully deleted.").'.</p>';
			} else {
				echo '<p class="err-admin">'._("Error while deleting extension.").'.</p>';
			}
		} else {
			echo '<p class="error">'._("You should not smoke weed.").'</p>';
		}
		
		echo $admin->admin_show_extslist();	
		echo $admin->admin_createlinks("meik_users", $next);
		
		break;
	
	case "aims":
		echo $admin->admin_show_applis();
		echo $admin->admin_createlinks("meik_target", $next);
		break;
		
	case "mappli":
		echo '<h3>'._("Target management").'</h3>';
		$ajout  = getvar("ajout");
		$delete = getvar("delete");
		$modif  = getvar("modif");
	 	
		if($ajout) {
			$nom  = getvar("nom");
			$uuid = getvar("uuid");
			if($admin->admin_addappli($uuid, $nom)) {
				debug("The target application $nom ($uuid) has been added.");
				echo '<p class="info-admin">' . _("Target application") . ' ' . $nom . ' ' . _("has been correctly added.").'</p>';
			} else {
				debug("Error while adding target application $nom ($uuid) !");
				echo '<p class="err-admin">' . _("An error occured while adding") . ' ' .$nom.'.</p>';
			}
		} elseif($delete) {
			$nom  = getvar("nom");
			$id   = getvar("id");
			if($admin->admin_delappli($id)) {
				debug("The target application $nom ($id) has been deleted.");
				echo '<p class="info-admin">' . _("Target application") . ' ' . $nom . ' ' . _("has been correctly deleted.").'</p>';
			} else {
				debug("Error while deleting target application $nom ($id) !");
				echo '<p class="err-admin">' . _("An error occured while deleting") . ' ' .$nom.'</p>';
			}
		} elseif($modif) {	
			$nom  = getvar("appli_nom");
			$uuid = getvar("appli_uuid");
			$id   = getvar("id");
			if($admin->admin_updateappli($id, $uuid, $nom)) {
				debug("The target application $nom ($id) has been updated.");
				echo '<p class="info-admin">' . _("Target application") . ' ' . $nom . ' ' . _("has been correctly updated.").'</p>';
			} else {
				debug("Error while updating target application $nom ($id) !");
				echo '<p class="err-admin">' . _("An error occured while updating") . ' ' .$nom.'</p>';
			}
		} else {
			echo '<p class="error">'._("You should not smoke weed.").'</p>';
		}
		
		echo $admin->admin_show_applis();
		break;
	
	case "update":
		echo '<h3>'._("Reindex available extensions").'</h3>';
		include_once("make-xpi-update.inc.php");
		break;
		
	case "add":
		echo '<h3>'._("Add or update one or more extension").'</h3>';
		echo '
<form action="admin.php?manager=madd" method="post">
	<p id="form-update" class="form-update">
		'._("Add url of each extension to add/update, on each line.").'<br />
		<textarea name="urls" rows="20" cols="80"></textarea><br /><br />
		<input type="submit" name="exec" value="'._("Add/Update").'" />
	</p>
</form>
';
		break;
		
	case "madd":
		echo '<h3>'._("Add or update one or more extension").'</h3>';
		$files = null;
		$urls = getvar("urls");
		
		echo '
<p class="good">
	'._("If all goes well, then you have to launch a reindexation in order to finish the update. If you don't do so, then your users will still have the old extensions !").'
</p>
';
		
		$ret = $admin->admin_retrievefiles($urls);
		
		if($ret) {
			$admin->admin_makeinstall($ret);
		}
		
		break;
	
	case "valid-sub":
		echo $admin->admin_showsubmit();
		echo $admin->admin_createlinks("meik_notifs", $next);
		break;
	
	case "mvalid-sub":
		$exts = getvar("ext");
		echo '
<p class="good">
	'._("If all goes well, then you have to launch a reindexation in order to finish the update. If you don't do so, then your users will still have the old extensions !").'
</p>
';

		foreach($exts as $ext) {
			$act = $ext["act"];
			$file = $ext["file"];
			if($act == "del") {
				if(is_file("tmp-submit/".$file)) {
					if(unlink("tmp-submit/".$file)) {
						echo _("The file").' '.$file.' '._("has been successfully deleted from submission directory.").'<br />';
						if($admin->admin_deletesubmit($file)) {
							echo _("The file").' '.$file.' '._("has been successfully deleted from submission list.").'<br />';
						} else {
							echo _("Cannot delete").' '.$file.' '._("from submission list.").'<br />';
						}
					} else {
						echo _("Cannot delete").' '.$file.' '._("from submission directory.").'<br />';
					}
				} else {
					if($admin->admin_deletesubmit($file)) {
						echo _("The file").' '.$file.' '._("has been successfully deleted from submission list.").'<br />';
					} else {
						echo _("Cannot delete").' '.$file.' '._("from submission list.").'<br />';
					}
				}
			} else {
				if($admin->admin_copynew($file, "tmp-submit/")) {
					echo _("Successfully copied").' '.$file.'.';
					if($admin->admin_deletesubmit($file)) {
						echo _("The file").' '.$file.' '._("has been successfully deleted from submission list.").'<br />';
					} else {
						echo _("Cannot delete").' '.$file.' '._("from submission list.").'<br />';
					}
				} else {
					echo _("Cannot add the new file") . " : $file";
				}
			}
		}
		
		break;
	
	case "valid-not":
		echo $admin->admin_shownotifs();
		echo $admin->admin_createlinks("meik_notifs", $next);
		
		break;
	
	case "mvalid-not":
		$exts = getvar("ext");
		$urls = array();

		foreach($exts as $ext) {
			$act = $ext["act"];
			$url = $ext["url"];
			
			if($act == "del") {
				if($admin->admin_delnotif($url)) {
					echo _("The extension which update URL was")." \"$url\" "._("has just been rejected successfully.").'<br />';
				} else {
					echo _("An error occured while deleting the notification for extension which update URL was")." \"$url\" <br />";
				}
			} else {
				$urls[] = $url;
			}
		}
		
		if(is_array($urls)) {
			$ret = $admin->admin_retrievefiles($urls);
			if($ret) {
				$admin->admin_makeinstall($ret);
				echo '
<p class="good">
	'._("If all goes well, then you have to launch a reindexation in order to finish the update. If you don't do so, then your users will still have the old extensions !").'<br />'._("Going to clean the garbage of extensions !").'
</p>
';
				foreach($exts as $ext) {
					if($admin->admin_delnotif($ext["url"])) {
						echo _("Extension's URL (").$ext["url"]._(") : Done.").'<br />';
					}
				}
			}
		} else {
			echo _("Nothing to do.");
		}

		break;
	
	case "auth-infos":
		echo $admin->admin_show_auth_infos();
		break;
		
	case "auth-set":
		echo $admin->admin_show_auth_set();
		break;
	
	case "auth-set-valid":
		echo $admin->admin_show_auth_set_valid();
		break;
		
	case "xpi-manage":
		$liste = $admin->admin_list_xpi_nonadded();
		
	 	$code = null;
		if(@count($liste) > 0) {
			foreach($liste as $file) {
				$code .= '
			<tr>
				<td>' . $file . '</td>
				<td>
					<form action="admin.php?manager=mxpi-manage" method="post">
						<div class="del-file">
							<input type="hidden" name="file" value="' . base64_encode($file) . '" />
							<input type="submit" name="delete" value="' . _("Delete") . '" />
						</div>
					</form>
				</td>
			</tr>';
			}
		} else {
			$code .= '
			<tr>
				<td colspan="2"><p class="msg-error">' . _("We're sorry, but no file has been found.") . '</p></td>
			</tr>';
		}
			
		echo "<h3> "._("List of all the XPI packages presents in the filesystem, but not in the database")." </h3>";
		echo '<p class="msg-info">' . _("Those files are oftently mal-formed package which cannot be installed in MEIK. So, here, you can delete them from trying to add them everytime you make a reindexation.") . "</p>";
		
		echo '
	<table class="xpi-manage">
		<thead>
			<tr>
				<th>' . _("Package filename") . '</th>
				<th>' . _("Action") . '</th>
			</tr>
		</thead>
		<tbody>
			' . $code . '
		</tbody>
	</table>';
		
		break;
	
	case "mxpi-manage":
		$fichier = base64_decode(getvar("file"));
		
		echo "<h3> "._("List of all the XPI packages presents in the filesystem, but not in the database")." </h3>";
		echo '<p class="msg-info">' . _("Those files are oftently mal-formed package which cannot be installed in MEIK. So, here, you can delete them from trying to add them everytime you make a reindexation.") . "</p>";
		
		echo '<p class="msg-info">' . _("Now going to delete the file file") . ' ``' . $fichier . '`` ' . _("from directory") . ' : ' . $admin->xpireldir . '</p>';
		
		$delete = unlink($admin->xpireldir . $fichier);
		
		echo '<p class="msg-info">' . ($delete ? _("The file has been deleted successfully.") : _("An error occurred while deleting.")) . '</p>';
		
		break;
	
	default:
		echo '
<p class="good">
	'._("Welcome to my world.").'
</p>';
		break;
		
	case "auto-update":
		$exts = $admin->admin_listupdateurl();
		
		$code = '
	<table class="users">
		<form action="admin.php?manager=madd" method="post">
		<caption>' . _("Here are the extensions that are ready for auto-update, and which have a valid RDF update file available."). '</caption>
		<thead>
			<tr>
				<th>' . _("Extension") . '</th>
				<th>' . _("Target") . '</th>
				<th>' . _("Update") . '</th>
			</tr>
		</thead>
		<tbody>
';
		
		if(is_array($exts)) {
			foreach($exts as $ext) {
				/**
				 * Array
				(
				    [updateurl] => http://www.mozilla-enigmail.org/php/checkupdate.php
				    [uuid] => 847b3a00-7ab1-11d4-8f02-006008948af5
				)
				*/
				if(preg_match("/^http/", $ext["updateurl"]) && !preg_match("/%ITEM_/", $ext["updateurl"])) {
					// echo "Checking for " . htmlentities($ext["updateurl"]) . "<br />\n";
					$urls = $admin->admin_check_update($ext["updateurl"], $new, false);
					if($urls !== false) {
						$done = array();
						foreach($urls as $uuid => $infos)
						{
							foreach($infos as $tuuid => $tinfos)
							{
								$link = $tinfos["updateLink"];
								if(!in_array($link, $done)) {
									$done[]  = $link;
									$extname = $meik->resolveuuid($uuid, "ext") ? $meik->resolveuuid($uuid, "ext") : _("Unknown");
									$appname = $meik->resolveuuid($tuuid, "app") ? $meik->resolveuuid($tuuid, "app") : _("Unknown");
								
									$code .= '
			<tr>
				<td>' . $extname . '</td>
				<td>' . $appname . '</td>
				<td><input type="checkbox" name="urls[]" checked="checked" value="' . $link . '" /></td>
			</tr>
';
								} // end if
							} // end foreach $infos
						} // end foreach $urls
					} // end if
				} // end if
			} // end foreach $exts
		} // end if
		
		$code .= '
		</tbody>
		<input type="submit" value="' . _("Start upgrade") . '" />
		</form>
	</table>
';
		
		if(is_array($exts)) echo $code;
		
		break;
		
	case "mupdate":
		
		break;
	
	case "mass-update":
			$liste = getvar("liste"); ///< liste is a string, 'n;m;p; ... ;'
			$liste = explode(";", $liste); ///< transform the received string into an array.
			array_pop($liste); ///< and remove it's last empty element.
			
			$exts = $admin->admin_listupdateurl_restrict($liste);
			$admin->admin_showmassupdate_result($exts);
			
		break;
}

echo '
</div>
';
$time_end = microtime(true);
$time = round($time_end - $time_start, 4);

$rsql->disconnect();
echo '
<p id="credits" class="credits">
		'._("This page might be ") . ' <a href="http://validator.w3.org/check?uri=referer">XHTML 1.1 Strict</a> ' . _("valid."). ' - '._("CSS also should be ").' <a href="http://jigsaw.w3.org/css-validator/check/referer">'._("valid.").'</a>
	</p>
</body>
</html>
';
?>