<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
function debug($message)
{
	$meik = $GLOBALS["meik"];
	if($meik->debug) {
		echo '
<div class="msg-debug">
	<span class="tag-debug">[' . _("DEBUG"). ']</span> 
	'.htmlentities($message).'
</div>';
	}
}

function error($message)
{
	echo '
<div class="msg-error">
	<span class="tag-error">[' . _("ERROR"). ']</span> 
	'.htmlentities($message).'
</div>';
}

function warning($message)
{
	echo '
<div class="msg-warning">
	<span class="tag-warning">[' . _("WARNING"). ']</span> 
	'.htmlentities($message).'
</div>';
}

function info($message)
{
	echo '
<div class="msg-info">
	<span class="tag-info">[' . _("INFO"). ']</span> 
	'.htmlentities($message).'
</div>';
}

function getvar($varname)
{
	if(isset($_POST[$varname])) {
		$data = $_POST[$varname];
	} elseif(isset($_GET[$varname])){
		$data = $_GET[$varname];
	} else {
		$data = false;
	}
	
	return $data;
}

/**
 * Return correct Content-Type HTTP header value wether the client support
 * or not application/xhtml+xml ..
 */
function contenttype()
{
	if(isset($_SERVER["HTTP_ACCEPT"]) && stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
		return "application/xhtml+xml";
	} else {
		return "text/html";
	}
}

/**
 * Create high-end HTML-based menu from a simple array.
 */
function makemenu($array)
{
	/**
	  *
	$array = array (
	  "id"    => "admin", 
	  "title" => _("Administrator menu"),
	  "data"  => array (
	  		[0] => array (
	  			"admin.php?manager=users",
	  			_("Users management")
	  			)
	  		[1] => array (
	  			"admin.php?manager=exts",
	  			_("Extensions management"),
	  			"true", // optionnal : is this the same page as we're on ?
	  			"true" // optionnal  : Did you run through this step ?
	  			)
	  		)
		);
	gives => 
	<div class="menu">
		<dl id="menu-admin">
		<dt id="hd-menu-admin">'._("Administrator menu").'</dt>
			<dd><a href="admin.php?manager=users">'._("Users management").'</a></dd>
			<dd class="current"><a href="admin.php?manager=exts">'._("Extensions management").'</a></dd>
			<dd><a href="admin.php?manager=aims">'._("Target management").'</a></dd>
			<dd><a href="admin.php?manager=update">'._("Reindex available extensions").'</a></dd>
			<dd><a href="admin.php?manager=add">'._("Add or update one or more extension").'</a></dd>
			<dd><a href="admin.php?manager=valid-sub">'._("Validate new extensions").'</a></dd>
			<dd><a href="admin.php?manager=valid-not">'._("Validate update notification").'</a></dd>
			<dd><a href="admin.php?manager=auth-infos">'._("Authentication backend").'</a></dd>
			<dd><a href="admin.php?manager=xpi-manage">'._("List non-added XPI packages").'</a></dd>
		</dl>
	</div>
	 */
	 
	if(is_array($array) && is_array($array["data"])) {
		$id    = $array["id"];
		$title = $array["title"];
		$html  = null;
		
		foreach ($array["data"] as $entry)
		{
			$url  = $entry[0];
			$name = $entry[1];
			if(array_key_exists(2, $entry) && $entry[2] === true) {
				if($url && $name)
					$html .= '
					<dd class="current">' . $name . '</dd>'; // add a new entry !
			} else if (array_key_exists(3, $entry) && $entry[3] === true) {
				if($url && $name)
					$html .= '
					<dd class="done">' . $name . '</dd>'; // add a new entry !
			} else {
				if($url && $name)
					$html .= '
					<dd><a href="' . $url . '">' . $name . '</a></dd>'; // add a new entry !
			}
		}
		
		$code = '
	<div class="menu">
		<dl id="menu-' . $id . '">
			<dt id="hd-menu-' . $id . '"> ' . $title . '</dt>
' . $html . '
		</dl>
	</div>
';
		
	}
	
	return $code;
}

?>
