<?php
/* $Id$ */
/**
 * Copyright (c) <2006> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

 
/**
 * This is the authentication backend for MEIK, using Microsoft Active Directory.
 * 
 */
class ActiveDirectory_Authentication extends Meik_Auth {
	public $desc   = null; // Description
	public $rev    = null; // Revision
	public $ver    = null; // Version
	public $author = null; // array of authors
	public $name   = null; // backend name.
	
	
	/**
	 * Here this is the config for ActiveDirectory_Authentication.
	 */
	public $msad_host = null; // What's the hostname of ActiveDirectory ?
	public $msad_port = null; // What's the port to connect to ?
	public $msad_base = null; // The base DN in which to search, e.g. ou=eleves,dc=di-epu,dc=univ-tours,dc=local
	public $msad_filt = null; // The AD filter to use for account. Default is "uid"
	public $msad_realname = null; // Name of the object which stores the user's real name. Default is "cn"
	public $msad_samaccount = null; // Name of the object which stores the user's login name. Default is "sAMAccounName"
	public $msad_mail = null; // Where we'll found the mail account.
	
	private $meik     = null; // The MEIK object.
	private $meikauth = null; // The Meik_Auth Object, with mysql_* functions
	
	function __construct()
	{
		$this->desc   = "This is MEIK's Microsoft Active Directory authentication backend.";
		$this->rev    = "release";
		$this->ver    = "1.0";
		$this->author = array ("Lissyx" => "lissyx@evilkittens.org");
		$this->name   = "msad";
		$this->meik     = $GLOBALS["meik"];
		$this->meikauth = $GLOBALS["meik"]->meikauth;
	}
	
	/**
	 * This function receives an array similar to the one returned by get_conf_keys() and
	 * set its values.
	 * 
	 * Array looks like :
	 * 
	 * array
			(
			"msad_host" => "localhost",
			"msad_port" => "389",
			"msad_base" => "dc=localhost",
			"msad_realname" => "cn",
			"msad_filt" => "uid"
			);
	 * 
	 * @return true if all goes well, false either.
	 */
	public function set_conf($array)
	{
		if(is_array($array)) {
			foreach($array as $key => $value) {
				if(property_exists($this, $key)) {
					$this->$key = $value;
				} else {
					error(_("This property doesn't exists for this class. There is no ") . __CLASS__ . "::" . $key);
					return false;
				}
			}
		} else {
			return false;
		}
		
		return true;
	}
	
	/**
	 * This function returns us all configurations keys for the object ActiveDirectory_Authentication.
	 * 
	 * @return An array, with all keys (such as 'msad_host', 'msad_port' ...)
	 */
	public function get_conf_keys()
	{
		return array
				(
			"msad_host",
			"msad_port",
			"msad_base",
			"msad_realname",
			"msad_samaccount",
			"msad_filt",
			"msad_mail"
				);
	}
	
	/**
	 * This function returns us all configurations keys descriptions for the object ActiveDirectory_Authentication.
	 * 
	 * @return An array, with all keys and their description (such as 'msad_host', 'msad_port' ...)
	 */
	public function get_conf_keys_desc()
	{
		return array
				(
			"msad_host" => _("Active Directory server hostname"),
			"msad_port" => _("Active Directory server port"),
			"msad_base" => _("Base DN for Active Directory binding (such as cn=Users,dc=host,dc=domain,dc=tld)"),
			"msad_realname" => _("Which attribute defines the real name ?"),
			"msad_samaccount" => _("Which attribute defines the user name used for login ?"),
			"msad_filt" => _("Which attribute is used to filter entities within msad_base ?"),
			"msad_mail" => _("Which attribute defines the user's email ?")
				);
	}
	
	/**
	 * This is the method called by MEIK to check.
	 * 
	 * @param $user The username to check.
	 * @param $pass The user's password to check.
	 * 
	 * @return An array, composed :
	 <pre>
	 Array
	 (
		[login] => "username",
		[id]    => "userid",
		[nom]   => "user real name" (optionnal)
	 )
	 </pre>
	 * 
	 * Sending an array with all this mean the user was correctly authenticated.
	 * 
	 * We'll add the user into the MySQL database, once it's authenticated, and ID
	 * will be the one in the MySQL database.
	 * 
	 */
	public function authuser($user, $pass)
	{
		/* 
		 * As we know that MySQL connection is already established, we don't need
		 * to create another.
		 * 
		 * Better, we'll re-use $meik->query !
		 */
		 $this->user = $user;
		 $this->pass = $pass;
		 		 
		 if($this->msad_attack()) {
		 	if($this->msad_authuser()) {
		 		$realname = $this->msad_get($this->msad_realname);
		 		$email    = $this->msad_get($this->msad_mail);
								
		 		$id       = @($this->meikauth->mysql_account_check($user));
		 		if($id === false)
		 			$id = @($this->meikauth->mysql_add($email, $realname, $user));
		 			
		 		@($this->meikauth->mysql_need_update($id, $email, $realname, $user));
		 		// echo "$this->user:$this->pass => $realname, $email, $id";
		 		// echo "Ok, now $user is authenticated, and we know it's name is $realname.";
		 		return array ("login" => $user, "id" => $id, "nom" => $realname);
		 	} else {
		 		return false;
		 	}
		 } else {
		 	return false;
		 }
	}
	
	/**
	 * This function will check for all the parameters to be here.
	 * 
	 * @return true or false, wether it's good or not.
	 */
	private function msad_check()
	{
		if(empty($this->msad_host))
			$this->msad_host = "localhost";
			
		if(empty($this->msad_port))
			$this->msad_port = "389";
					
		if(empty($this->msad_filt))
			$this->msad_filt = "uid";
			
		if(empty($this->msad_realname))
			$this->msad_realname = "cn";
		
		if(empty($this->msad_samaccount))
			$this->msad_samaccount = "sAMAccounName";
		
		if(empty($this->msad_mail))
			$this->msad_mail = "mail";
		
		return !empty($this->msad_base);
	}
	
	/**
	 * This will try to bind the msad server, anonymously or using a username, depending
	 * on the configuration.
	 * 
	 * If it success, then we'll put the connection resource in $this->msad .
	 */
	private function msad_attack()
	{
		if($this->msad_check()) {
			if(false !== ($this->msad = ldap_connect($this->msad_host, $this->msad_port))) {
				ldap_set_option($this->msad, LDAP_OPT_PROTOCOL_VERSION, 3); // If none, bind won't work.
				return true;
			} else {
				error(_("Cannot contact msad server."));
				return false;
			}
		} else {
			error(_("msad configuration seems broken. Please check."));
			return false;
		}
	}
	
	/**
	 * This function will try to bind the msad server.
	 *  
	 * @return true or false, wether the couple user and password is found.
	 */
	private function msad_authuser()
	{
		$user = $this->user;
		$pass = $this->pass;
		
		if($this->msad_is_connected()) {
			$dn = $this->msad_filt . "=" . $user . "," . $this->msad_base;
			// echo("Binding as " . $dn . "; with password $pass");
			$msadbind = @ldap_bind($this->msad, $dn, $pass);
			if(false !== $msadbind) {
				return true;
			} else {
				return false;
			}
		} else {
			error(_("msad isn't available for binding."));
			return false;
		}
	}
	
	/**
	 * Search for and real name !
	 * 
	 * @param $what What to get ?
	 * 
	 * @return The realname !
	 */
	private function msad_get($what)
	{
		$user = $this->user;
		
		$search = ldap_search($this->msad, $this->msad_base, $this->msad_filt . "=" . $user);
		$entry  = ldap_first_entry($this->msad, $search);

		if(false !== $entry) {
			$id = @ldap_get_values($this->msad, $entry, $what);
			if(count($id) > 0)
				return $id[0];
			else
				return false;
		} else {
			return false;
		}
	}
	
	/**
	 * Check wether the msad is connected or not.
	 */
	private function msad_is_connected()
	{
		return (is_resource($this->msad));
	}
}
?>
