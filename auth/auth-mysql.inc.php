<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <webmaster@lissyx-overclocking.fr.st>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
/**
 * This is the default authentication backend for MEIK.
 * 
 * It's goal is to provide a pattern of what's needed within an
 * authentication backend.
 * 
 */
class MySQL_Authentication extends Meik_Auth {
	public $desc   = null; // Description
	public $rev    = null; // Revision
	public $ver    = null; // Version
	public $author = null; // array of authors
	public $name   = null; // backend name.
	
	function __construct()
	{
		$this->desc   = "This is MEIK's MySQL native and historical authentication backend. Therefore, it uses the MEIK mysql database to authenticate users, so there is no need to add ourselves the users to the database.";
		$this->rev    = "release";
		$this->ver    = "1.4";
		$this->author = array ("Lissyx" => "lissyx@evilkittens.org");
		$this->name   = "mysql";
	}
	
	/**
	 * This function receives an array similar to the one returned by get_conf_keys() and
	 * set its values.
	 * 
	 * @return true everytime, as there's no config.
	 */
	public function set_conf($array)
	{
		return true;
	}
	
	/**
	 * This function returns us all configurations keys for the object MySQL_Authentication.
	 * 
	 * @return An array, with all keys (such as 'ldap_host', 'ldap_port' ...)
	 */
	public function get_conf_keys()
	{
		return array();
	}
	
	/**
	 * This is the method called by MEIK to check.
	 * 
	 * @param $user The username to check.
	 * @param $pass The user's password to check.
	 * 
	 * @return An array, composed :
	 <pre>
	 Array
	 (
		[login] => "username",
		[id]    => "userid",
		[nom]   => "user real name" (optionnal)
	 )
	 </pre>
	 * 
	 * Sending an array with all this mean the user was correctly authenticated.
	 * 
	 */
	public function authuser($user, $pass)
	{
		/* 
		 * As we know that MySQL connection is already established, we don't need
		 * to create another.
		 * 
		 * Better, we'll re-use $meik->query !
		 */
		
		$meik = $GLOBALS["meik"];
		$auth = "SELECT `login`, `id`, `nom` FROM `meik_users` WHERE `login` = '$user' AND `pass` = MD5('$pass') LIMIT 1;";
		$result = $meik->query($auth);
		return $result[0];
	}
}
?>