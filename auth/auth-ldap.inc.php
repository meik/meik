<?php
/* $Id$ */
/**
 * Copyright (c) <2005> LISSY Alexandre <lissyx@infos-du-net.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

/**
 * This is the default authentication backend for MEIK using LDAP.
 * 
 * It's goal is to provide an exmaple of authentication backend,
 * which is different from MySQL.
 * 
 */
class LDAP_Authentication extends Meik_Auth
{
	/**
	 * Here this is needed public variables !
	 */
	public $desc   = null; // Description
	public $rev    = null; // Revision
	public $ver    = null; // Version
	public $author = null; // array of authors
	public $name   = null; // backend name.
	
	/**
	 * Here this is the config for LDAP_Authentication.
	 */
	public $ldap_host = null; // What's the hostname of LDAP ?
	public $ldap_port = null; // What's the port to connect to ?
	public $ldap_base = null; // The base DN in which to search, e.g. ou=eleves,dc=di-epu,dc=univ-tours,dc=local
	public $ldap_filt = null; // The LDAP filter to use for account. Default is "uid"
	public $ldap_realname = null; // Name of the object which stores the user's real name. Default is "cn"
	public $ldap_mail = null; // Where we'll found the mail account.
	
	/**
	 * Here this is neede private variables !
	 */
	private $ldap     = null; // The LDAP ressource.
	private $meik     = null; // The MEIK object.
	private $meikauth = null; // The Meik_Auth Object, with mysql_* functions
	private $user     = null; // The user to test !
	private $pass     = null; // his password.
	
	function __construct()
	{
		$this->desc     = "This is LDAP exmaple backend, from MEIK. It should be used as an example of how to make an authentication backend, which doesn't authenticate on the mysql's MEIK database.";
		$this->rev      = "release";
		$this->ver      = "1.4";
		$this->author   = array ("Lissyx" => "lissyx@evilkittens.org");
		$this->name     = "ldap";
		$this->meik     = $GLOBALS["meik"];
		$this->meikauth = $GLOBALS["meik"]->meikauth;
	}
	
	function __destruct()
	{
		if($this->ldap_is_connected()) {
			@ldap_unbind($this->ldap);
		}
	}
	
	/**
	 * This function receives an array similar to the one returned by get_conf_keys() and
	 * set its values.
	 * 
	 * Array looks like :
	 * 
	 * array
			(
			"ldap_host" => "localhost",
			"ldap_port" => "389",
			"ldap_base" => "dc=localhost",
			"ldap_realname" => "cn",
			"ldap_filt" => "uid"
			);
	 * 
	 * @return true if all goes well, false either.
	 */
	public function set_conf($array)
	{
		if(is_array($array)) {
			foreach($array as $key => $value) {
				if(property_exists($this, $key)) {
					$this->$key = $value;
				} else {
					error(_("This property doesn't exists for this class. There is no ") . __CLASS__ . "::" . $key);
					return false;
				}
			}
		} else {
			return false;
		}
		
		return true;
	}
	
	/**
	 * This function returns us all configurations keys for the object LDAP_Authentication.
	 * 
	 * @return An array, with all keys (such as 'ldap_host', 'ldap_port' ...)
	 */
	public function get_conf_keys()
	{
		return array
				(
			"ldap_host", "ldap_port", "ldap_base", "ldap_realname", "ldap_filt"
				);
	}
	
	/**
	 * This function returns us all configurations keys for the object LDAP_Authentication.
	 * 
	 * @return An array, with all keys (such as 'ldap_host', 'ldap_port' ...)
	 */
	public function get_conf_keys_desc()
	{
		return array
				(
			"ldap_host" => _("LDAP server hostname"),
			"ldap_port" => _("LDAP server port"),
			"ldap_base" => _("Base DN for LDAP binding (such as cn=Users,dc=host,dc=domain,dc=tld)"),
			"ldap_realname" => _("Which attribute defines the real name ?"),
			"ldap_filt" => _("Which attribute is used to filter entities within ldap_base ?")
				);
	}
	
	/**
	 * This is the method called by MEIK to check.
	 * 
	 * @param $user The username to check.
	 * @param $pass The user's password to check.
	 * 
	 * @return An array, composed :
	 <pre>
	 Array
	 (
		[login] => "username",
		[id]    => "userid",
		[nom]   => "user real name" (optionnal)
	 )
	 </pre>
	 * 
	 * Sending an array with all this mean the user was correctly authenticated.
	 * 
	 * We'll add the user into the MySQL database, once it's authenticated, and ID
	 * will be the one in the MySQL database.
	 * 
	 */
	public function authuser($user, $pass)
	{
		/* 
		 * As we know that MySQL connection is already established, we don't need
		 * to create another.
		 * 
		 * Better, we'll re-use $meik->query !
		 */
		 $this->user = $user;
		 $this->pass = $pass;
		 		 
		 if($this->ldap_attack()) {
		 	if($this->ldap_authuser()) {
		 		$realname = $this->ldap_get($this->ldap_realname);
		 		$email    = $this->ldap_get($this->ldap_mail);
		 		$id       = $this->meikauth->mysql_account_check($user);
		 		if($id === false)
		 			$id = $this->meikauth->mysql_add($email, $realname, $user);
		 			
		 		$this->meikauth->mysql_need_update($id, $email, $realname, $user);
		 		// echo "$this->user:$this->pass => $realname, $email, $id";
		 		// echo "Ok, now $user is authenticated, and we know it's name is $realname.";
		 		return array ("login" => $user, "id" => $id, "nom" => $realname);
		 	} else {
		 		return false;
		 	}
		 } else {
		 	return false;
		 }
	}
	
	/**
	 * This function will check for all the parameters to be here.
	 * 
	 * @return true or false, wether it's good or not.
	 */
	private function ldap_check()
	{
		if(empty($this->ldap_host))
			$this->ldap_host = "localhost";
			
		if(empty($this->ldap_port))
			$this->ldap_port = "389";
					
		if(empty($this->ldap_filt))
			$this->ldap_filt = "uid";
			
		if(empty($this->ldap_realname))
			$this->ldap_realname = "cn";
			
		if(empty($this->ldap_mail))
			$this->ldap_mail = "mail";
		
		return !empty($this->ldap_base);
	}
	
	/**
	 * This will try to bind the LDAP server, anonymously or using a username, depending
	 * on the configuration.
	 * 
	 * If it success, then we'll put the connection resource in $this->ldap .
	 */
	private function ldap_attack()
	{
		if($this->ldap_check()) {
			if(false !== ($this->ldap = ldap_connect($this->ldap_host, $this->ldap_port))) {
				ldap_set_option($this->ldap, LDAP_OPT_PROTOCOL_VERSION, 3); // If none, bind won't work.
				return true;
			} else {
				error(_("Cannot contact LDAP server."));
				return false;
			}
		} else {
			error(_("LDAP configuration seems broken. Please check."));
			return false;
		}
	}
	
	/**
	 * This function will try to bind the LDAP server.
	 *  
	 * @return true or false, wether the couple user and password is found.
	 */
	private function ldap_authuser()
	{
		$user = $this->user;
		$pass = $this->pass;
		
		if($this->ldap_is_connected()) {
			$dn = $this->ldap_filt . "=" . $user . "," . $this->ldap_base;
			// echo("Binding as " . $dn . "; with password $pass");
			$ldapbind = @ldap_bind($this->ldap, $dn, $pass);
			if(false !== $ldapbind) {
				return true;
			} else {
				return false;
			}
		} else {
			error(_("LDAP isn't available for binding."));
			return false;
		}
	}
	
	/**
	 * Search for and real name !
	 * 
	 * @param $what What to get ?
	 * 
	 * @return The realname !
	 */
	private function ldap_get($what)
	{
		$user = $this->user;
		
		$search = ldap_search($this->ldap, $this->ldap_base, $this->ldap_filt . "=" . $user);
		$entry  = ldap_first_entry($this->ldap, $search);
		if(false !== $entry) {
			$id = ldap_get_values($this->ldap, $entry, $what);
			return $id[0];
		} else {
			return false;
		}
	}
	
	/**
	 * Check wether the LDAP is connected or not.
	 */
	private function ldap_is_connected()
	{
		return (is_resource($this->ldap));
	}
}
?>